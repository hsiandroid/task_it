package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.BookingHistoryModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class BookingHistoryRecyclerViewAdapter extends BaseRecylerViewAdapter<BookingHistoryRecyclerViewAdapter.ViewHolder, BookingHistoryModel>{

    private ClickListener clickListener;

    public BookingHistoryRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_booking_history));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setTag(holder.getItem());
        holder.getBaseView().setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().name);
        holder.awayTXT.setText(holder.getItem().away);
        holder.priceRangeTXT.setText(holder.getItem().priceRange);

        Picasso.with(getContext())
                .load(holder.getItem().full_path)
                .error(R.drawable.placeholder_avatar)
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.imageCIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)                  TextView nameTXT;
        @BindView(R.id.imageCIV)                 ImageView imageCIV;
        @BindView(R.id.awayTXT)                  TextView awayTXT;
        @BindView(R.id.priceRangeTXT)            TextView priceRangeTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public BookingHistoryModel getItem() {
            return (BookingHistoryModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {

        if(clickListener != null){
            clickListener.onItemClick((BookingHistoryModel) v.getTag());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener{
        void onItemClick(BookingHistoryModel bookingHistoryModel);
    }
} 
