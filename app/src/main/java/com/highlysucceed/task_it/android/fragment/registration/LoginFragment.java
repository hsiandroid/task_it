package com.highlysucceed.task_it.android.fragment.registration;

import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.data.model.db.UserItem;
import com.highlysucceed.task_it.data.preference.UserData;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class LoginFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = LoginFragment.class.getName().toString();
    public RegistrationActivity registrationActivity;

    @BindView(R.id.emailACTV)           AutoCompleteTextView emailACTV;
    @BindView(R.id.passwordET)          EditText passwordET;
    @BindView(R.id.forgotPasswordBTN)   TextView forgotPasswordBTN;
    @BindView(R.id.loginBTN)            TextView loginBTN;
    @BindView(R.id.signUBTN)            TextView signUBTN;
    @BindView(R.id.imageIV)             ImageView imageIV;
//    @BindView(R.id.requestEmailTV)      TextView requestEmailTV;

    public static LoginFragment newInstance(){
        LoginFragment loginFragment = new LoginFragment();
        return loginFragment;
    }

    @Override
    public int onLayoutSet()     {
        return R.layout.fragment_login;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        forgotPasswordBTN.setOnClickListener(this);
        signUBTN.setOnClickListener(this);
        loginBTN.setOnClickListener(this);
        imageIV.setOnClickListener(this);
//        requestEmailTV.setOnClickListener(this);

        if (UserData.getString(UserData.EMAIL).equals("")){
            emailACTV.setText("");
        }else {
            emailACTV.setText(UserData.getString(UserData.EMAIL));
        }

        if (UserData.getString(UserData.PASSWORD).equals("")){
            emailACTV.setText("");
        }else {
            passwordET.setText(UserData.getString(UserData.PASSWORD));
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.forgotPasswordBTN:
                registrationActivity.openForgotPasswordFragment();
                break;
            case R.id.signUBTN:
                registrationActivity.openSignUpFragmentB();
                break;
            case R.id.loginBTN:
                saveAccount();
                break;

        }
    }

    private void saveAccount(){
        UserItem userItem = new UserItem();
        userItem.id = 1;
        userItem.fname = "John";
        userItem.lname = "Doe";
        userItem.email = "john.doe@gmail.com";
        userItem.image = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABC1BMVEX///8AAAD3z6v0wJX53cQrKytoV0g9PT0pKSkNDQ391K/+1bBiUkT3zqotLS33wpccHBwTExPivpwkJCQhISFdTkAQEBD+yJuzlnzZtpb/48m5m4AYGBj39/dsW0vxyqfRr5CHcV7x8fHU1NR4ZFM0NDSUfGfjybOioqJUVFTe3t6ysrLGponcrYYwKCGcg2xISEh8fHz41rjLy8ubgmuQkJA9Myr1x5+HalKEhIRmZmbl5eV0dHS+vr5URjrQpH+gjn6tra23opBbW1u8lHN6YErt0rqefGBGOjAjHRg1KiDAl3U3MSytiGpVTEPLtKBeU0qGdmnBq5h7bWFsXVBSQTKrl4ZFPTZ9b2LSpGW1AAAYwElEQVR4nNWdCV/aytrADUowCzthJxBERUVoXaitVuvpovSc1vbcevp+/0/yzprMJJMQYAL2ub97ryUs+fM882yzsLW1QbnubPLT1yF7ytHl8aZvIlk5VhRl/+0fDbl3Mrw5yhWrQMqZm+H7a9/19wqU/ZON3NyK0j8e3pSVoLy75yhfk4eHf9aQ7O9dfhTAUXk19CA79MHK/QZveFG5fheBh+XoPX3ypftYZm+TN72g9C8j6Hi7fM88dLPh215I+vP1iBnfgj/abfJI4XzT972IvI+CI0D3e0P8x26RPPR207e9iBxr6OYr7TBARtq5sob/yvQ3fd8LyDW4YS2dy+UKMRDTafqsP8lSLxFiOp2rxEIsV/Gfl5u+7/iyh8w0BxBjaLGQ3qVq9PvUznvh278AQTGjkoNaLEbBYakARJIFveIznGvl9YYIfNLpH59cvn33cT/36uPby/P+veISptPafMQyQNzFlrrLpXbgm3q3KShPjofvMmLVIMKcKEf1ibYLJI1db5EpOM7hAxtGvL7MRWgGSRw7raYhIhmMLuIJ/vcmEc9fRdx1ERtpOr07nxA4VIhI1H3f6XSOz4fu6zaWDOwF+DQNlIOaRtRCJY4SobfxED3Bg3MzHrXzmr0TrV0ogzuEUR4ordJuV9IeYYygiIci8De8Xypi5OIm0p09xvbaZUiWY5i4f8UyU6WYDiK2qVI3EDNOvC+/wuMIJBcjYBA7BUJtWisCV0XVv/aOjlsFQr4QLCoxByIIhVjS5QqUMtCn617XrkQXsJ0W8wGucqXQBv+pVHZzuThFBsreCCMybfRXlV5cL+CQfmw5hC9d4eyyWFXmi0atlBXPt6617qAVblWswFw6RrYt4BMApj3zXmfZ0dcooFiB5Vh+hRdQhgT5uPB4tEbCmzmAi/NBHRbK7uBzRyP7VvvrA9wjt+R3LbvlFQDxd1aAWYMrFc4DV9aAdo4TC9JK45xMLlcuKtgDLg8YLesgPC7CjjUJ9W0OEA69ctx6dznhrDSpDjIIEgXqJncZPlS7IquNlYEuJ1wJtZtUQeXdf8FTIaFCSo2Xni0lQ+Y+zhNrkruDjAUk+QokXMXNzBO2uQFdQSI1IxmCBZCHBQAx4RKhPqZkmPs4Ro/IrxmPj/BnaWwm42WciDA5I2WTNlJ4S5537Lv1boUF9MwSeZrEAFkcmhXLtdMTqh2twuWijNJ8OYhk8eKDN+8jU4muAgtcss2Nu0IuSUdTpGY69B6TtwagTztObX+1y4273SQJQci/v+70T9jO7EdZgNf7+A21QDXoIyonShiUnCQz7RBFFQPVYCA4JOdKxeJfwbKkEA22g9VuLk7pnqTIaU+9xW8mAEwyOMQTKYQ4f/A69KwkVinFFSmEJE4wpUS6TGdckiskYooMQrJwqcAlauRfS2ahRXkeV4anIQkEq8K0VlyJUGnvS0rQ2zJmM7Cf0dhRCFQQEiziSiETPue4iLySAEhCBetIIRc20+Vrpf1MRkagkdLpx2/FDUN4b6vpUNEymYwENyWjW0McDZeuwQfQSFzBlwIlZlZ2ODkJgFt9/F5crECPIK0uf48FQJhZNZxKmcm4DhBSxRVgmbH83WVWR5RTAAd16A6+anl3d/lMex8hrmKokmZqBITxpgLnSRoRZpb1VfvytjLgN2Q6F5Ka2hVMmFnOCvYl9i/wOxakE7YJ4f5Srx7Ov/HYgpsGbemERUK4XFiUOamPS4uioAMsiVC8Im6eSNyHco/esMoQysmaNZdQFDLmj055WzRw7461Ujm1T9UlFDqb+Yiy5tc6+BvmiicphK6V7u+KBramzWOU1WfDUxVcm1uOqyGE+yEg2nxEOREfLwwq8D0aKWbajo4VWgxEGeUvXpNQ9TWhpPQQC15KM/4aQjgHUYYScTN/N+0jlNGA2qVOZtbrpiZLEUpYB4YWWYsapRIIc4gQWEPLSKWMuphwDuLqU6Q4GPtVKGcedJ9aaTcFxPBrUYuDuPIqKdwLFrSCY3nT23HkZTdlK6WQTMWE0Yir+hrcZqssNV1h9vTuIOK65hZPmFD32akWC3FVM8VGGuCL0cx/7Bl6Vi9FPKPsI0ylQgAjCVdMwLGRagIVzsm9b0eGDu+5G/G8XIYbh2AkWr5nxEHMzKeIknv0JoUA4ZxgYd5hvmhCkrIVlSkh1C8Cz4mBuFpXH9dNgWEYnXl/Ps2/IWYXRUizUkVRjZRwIDKIER+3WrctJNxHAM4etmv57WwMwl23wD8gCtdxvJg9Nf9Tfn/4wRJGIK6U1pC5bX/GFg74/Clf2waScgnZW5u1eo73L6807FHCA3ThqbnTfGo2m0/uUyMJV1rchrvdPkeTC3Oj04dToD4orpFyicqgq+uGSf9VIIRVZdalz76FF37vEGl+iIW40tSMyJWGAM4eTvN5zLftGanjXr+td3XPEBWa0MAJKMeghCjG/tukiF9iEa60ZggnpXxdIegAF5+/Ae1RPJZwVHcc56DeKhnEuRo2eQ3TSWyQJ+s9eOF/LiAg/Pn09H9zEVchfC8g9OUyM+vhbpvDY4chMEvApuveA+RGaYsGBEPbVSEchq6NQsJ/wWhs/juPcJW+8EmAkOtBPV99Ow3QscMwIHqLvJTYKPRZNKFJdUEW+/0LQ7jT9IZjBOIq4WIY1CF5V+3hExh3AjrOSINCkmsvnVHqVMGQ3t4RyOEcwlUy04AOXRXehcBFExo4HJAJC1SAOZ4FT5ULQ0S4M1MimzarEOJxyPhSOvU+roXiscPQD1hnTBRntjZjwLOekToTADb/L1qJq7T3z4lNBdLR5yjCsGFIAGEg3CebhtxAAQlTeiorJJzja1YhJGuhPBXSHOzz4oRGFweK/X13z9dsZDAGjcxVRLjzRYs001WslMzgu5m327p4XJBQN7oX/vubHXT14DNFhM0fyRHib9utntx0Rgt3MwJHo3dHA38GbtW7RpAvhDA6XqzU28cHWlFXw6Tcf0cgBgh7t/wtjR2Qwgn0F0r4FEoIqsvVOjVkI6zf0QBXE0EoGIN6rzUZ2ECcSauXoimcQLLCcNEUxos2OjOsc7/S5AVZ0VYJrmH7Fo4ounMd5W++FE5EKHQ1woF4JGV7PlloQlfpMYPp6yI6jC8hhB+ChGmp806kymd7pM/JEIrDhSgiypo7HLJmyhAWTxez0vgiHIjB1FTKkkQo3PpnhvBbREBMgLAZIJQ3iU8WEfjGYWTEXwuhvFNA7vEboqDvEd4tFPElELZ9hCv2gVkhiRuqoNxoEZl4J0L4D1aiSyhz9yjZtc2tJY3KaKJKfHmEMjflnXvelGZt4yg+RLiUGtGLhNFip4mcKbP+ROaSr07FG4gk846qnfL52hsDTumi/CUmG0540IvOmk0h4l8KJzIJ2TVfpHoKNdJ8bfvu09VnVX18VE3bqfdgFzFan1kAV+rVHds0wYtU++e/T19EkM0fyRFiM0X1BZ4W1cTRHuB9s2b8V61NnZYeBWnoI2fqb1B+/y8IyfT4pRNeM4TImX4VGWk+f6cqYrF7KaG5ZnW9ETZH/OPJz4idDRW5239R0MfhArkaU0BYO30OuVco07qAUddHtxGv+f7EIzKzGIrsswZQ9k32j8A/HwJGms8/RNwrYmz5K3qj9zjnNT95JR6y16SlpUhQRCzjA61gZvrJT5jfjlIgEZVHJDNpkfL7C6vGJndN6j58pMNcutyutssVAWF+eya6P59oPiVO579EKR6yiNzXKPUwBbi0TiNlBQwXgaSUudnZ8+fJqIek7pgsRZsjzKbYMThWHfKq0WRgMmsEZsw0RpPzSjLPNbn2f7M+wvxneuH54Q7EDJ2KYXRLdTqbpjzyOnSn2RS1Xuqi7gZ91ZfDD9/pxe+eEs94u5a0wRnKUPEJHw7zn4h2r/AUKZ+VgrSmhbXV4r0pnixUHuspf+YD8rZm88sHkuV/aIYQSlRiYJm5jxCt7Bp/y5MZ7mC31GgMptbI70v1nvVo9wRdN5yZNpt//UZfnGun/qV90mrg4MnjHGH+G3zoYZs+JiwsDFH/UBc+6uXezeZ/VVaJOr+yb1/a2W3BFXocYQ14k+dTLwdYrXRC4vmWL78U5Z+dAOGr+5NzeTW+4HxZljB/ChRYYx5g7nTZSpiNgaCkOAzq8Eiim+mfQznhWrGsLwVGesclccAZdksNKKVUeGvbJ7qRKqEXlUogT+dytcMiMdMzLkeQ/KsmfXdP8ng88xFe8aNSbxzQRaXVR7slnn3xiVGqq1PyHc4eB60dLpM5/MkQjmfJIJLV9M9/I4pvfDzkBuWdP9c8mK+/kul7jfZhR1AgAsLbbjbVcMizZJ7WhqdnpndkuIVP4OevgoN23J0D2Aq+xpetYUG2DDVOVuWW5QHiDZbWdmT3CWlQWGDcRhtqQ/Qaz72whK5V48AorwhGOc3XiKUXrohvNlKJhiN+0Y+AEsGTs8Q74zXv8lqmqLQgYw8YaigqTd9m6kN9BB1ja4JGWC+SEKans0G9BV8wqk8GNFkXWWmKZng62n8ia48sMtJpHjuVh+lsGtbvBoFj9vzw93atln+DbgQEjlR9qkS2okCQG/RoVw7l3XqjNQD52hcf4Fk2a43Hg4buKVHW5jxESPqHqEKbhvQS86feAjc3s9FTdPFoSHTsNnyPZ4FHOTv8N+hK8ehDb4fTdllpG5p9uoI3nv8bG1CoEsWpDZHG4CJuAgDSmsAwPNNxmLXRm6AtALK6UdceIQkGV5GzFiGEIF47ccI/IQyI0SOjPCWd0LNSShg5tYYlOPTAgDtYQIdBwjpL2JBppYjQZHVYnMeXF/gWQLiIlQYJSU9gjN6sLtPTdPYpFA0H0bOH22+EvnMhQsGaE9q5UuGb4CgqLW9DzUSYX8NKCUmwYerSifHw176ADgMzUGd0exF6Ex01RqSVUEMXKk/6eYKm9xvAlo0MfK1FCANmekbTV7QovAv/ktcURttk0YJSmnjOfFMzoYpbntCPeObgj56i90IBX+KZCqh4+ps1U35FVKw7XpSQR2zSKY4LGHH0qtRh6KbeUIm0lGNDYrwb1kfeiu44wi+Ocjv6cNm7gTaAyTyAHi9WeKgxSmRWQcftxvT8HdNFEOmsDEwadFxwSS3yh26MqNH+tjcS497xooQsIl71BQR+nTrqIxRkAm5toT7NDAGRJow7OxO7e7gwIYPY/A9/6kh3K0rJv+WBTx1QkZ1WFXXKLFaI3TLsulvU4otL+EtRBzNloqPxDEX6UeXYTrE//VQD2dtjYBPXfMLG/CeFIDb/sQ2jhD0V6rZJXBJFBRX6qA7O4wlfOhBj3+wyhG72piEXk3I37UvsCFPpM6MPTVV8W5BQ72lzum4iOaOulEaaLlKh1JUYVJCdmpgQVsJ0IMYmPFgoHFLBhE+UEPceE7DRLRoUUUsxfwf+elwwWBhm/PrXE2ymzb9cQlt6KPQELY16cAmVBT1NKbLlFkqIzLT5kxKilFtL6FfKz91cBteJpyETolKFEP4ihDjlTuzHrb0E/BsT89dC+EMxMaGWQLD3ZIgDRj5fu3INdgXCWH6HpDW/4T5FnYSK5H7zCfuaq+1T3K658nVG5wHx20mMrFOKfD5L+AWGiMdetyc/5eYFz0HRGTwSLmIS6iN7MBgc1HuNUqnbLTUm01jlIiEsMp8scc4pKNxJe+ZiATHrrveZjcdoOMVpn2a52imJlJuXY/aTvi4Y8g3/DrZJnJEYIEz41/PO20HC2OWTbjQcZhlYvP6wnzDxnwfsDN21GbNFCYEYqcbowhnYA6dVipPiEFfqESboZVx5634arRAXIEzRResxV2lkd3yEiQV7RnIBwiRjvp9Q4magMOkoAUIJq6BCJHsWIJR3ZGmYvA8SrriTK0p2AoTJ/0znUEAY20zpXHbMp7udKDZaJFDd83IjIIxhpiBdA5nMqAXK/G5rBLKa1Jz9wDC4uMu/WMLEf348IyDcjrpZ4DuzpUZr4gxMy1IhoW1Z5sCZ1HulLsQMs4CW7a67ZAkT/w15TUCYH/VEwQ0uOdBLo7ozUC3TNFVVNeFBJvoB+tM0LdMeTEYN8QqG7pQ5i4cllPYTXWGiBAlh620kWCCbarQcyAGBCBEqJ0q2SYjRw6ZTbwRLqUdF8VZ+sYTtDRBu11TlNmBsjYmN4UxonQN40kCJHlpW6kGrVS1Kb6kTHhI2nJhVUVzmnVALI5Iwv634+tl6w4FqAmy2c1AfNfCIY58APE8JWPCFYyNO03JYS9ct9sgvd9Zi3YTe5EzN5kshvQ7p7HqrF+004aVuFyjUtgAjc9RJV1E0T4U8YdLhoioihM039sYn4IadRuwNlsDd9gZAj2qDmnGL3YXg29OVtA6PvI9i91l+VQ48JTYs1arHjuuYKXUBhqRJGhuGo4x3djZEeOl9FLP9CfYX3Z69PjHNi4XbvzCImMTWDUVjl9A2f7KECQOyeSk7l5//dOrqTAfu0T0JscUPUCJCRuhV8eu6H/hNa/9jABOPh33vs7j9znnvyNKSadIpCqOBdAsGGkzaSo0Rll4JtqN032l8LdM1U9+6vd8MYeJFPjl1CIp/FSaJiPrIskjsMOBsZt0Adf0gcCr0bHqrDg5avYZ72lDXIoSBE1xQq414AGk/LRMqnplqPCBNwEGosBqEFaFEbk6cTe2DFrFN6moCa74wGuKUu3dULDD3xhvY/ZvW31CXQQhTzsBxLuqTOivYThsluHeEP3QolBCnNH3k5NbRqIFKPP4YGIiuneogHSOE5OBLgSAQn6dxCX3rofBhA8UOXEyQzMyhXwBdH3VOA4dHYCUOXIexoNAX+oYhDocZlE/J3EYSLh2l2scLFgNbniFi1w4hhN4UutCwgJElhH5Hg09T+Air7+QdKZa9fbI2+mtgFWY2nFBvOVgOJvUWiBiBWf1QQjQMX29tDZNv0lCBye9bf9CnSiyZYTo8YMSZdP3jkBL6lyUe0jjYTzph4wWF/qpfh0CJoYSpbt1x+URz3mJCcqpJIssvogV13QLL9t9EEKb0bqPXao1AlBdedkwhIc7ZEp1yChE01xY45iTbsKJ8aXhaSgkDGc0/a8plgoKyGzUwEmHtFGP1k6DFJiQkpwu11zsGiRyJIkY+lLCLmmpwMTjKxIOIxEpF0XBNod4veMLUty8xlFAfDeoNyNVttEAmLjBkISEpLNYVCX2CMkWVdzb5U1W1BM1FgOhYlmUPBrD1ZInWmooIiZFuxNEA6aB0nz9gMH9qhhCmGrhHilrDouuY0DcMSX2/GUBaSvG7uQGhKV4NbDgU0RQOVETod6XKJo10i9TDU7+VmuJliHrPIoDiOXxMKPIza5gYDRN8sgtfRgEC8TKLbMqMUqGIcAfPkyQ+HRMheD6RG4p2mI5gFw4ROuLLQUIyCoubU+EWPTeD3c32GSAICaiZhjgiQAivsITEkSY+Zxgp5DQ3pgH+2VTtkAXPXaRCS3wRZN6PLY6w+X3jNgoF2+nMDfy1q6lthS3pht7UDDHS7kCdHuiMKyVuRuaJUMsJnhaeUsTa1cwOS0z1uhnqaYGC7dmtwWyUedpgvsYJOSnrkSDWrjQ7JCDCXqoampeXLLtYLXn7ZA5fiAa33IN4nzFi7VmxzbCVlaB4VMNMuGfZmtJyz1A6nL2EMUiEHFP7dRvtqdEU4E1CZmZKKjOp4dNvy7QV5faMAmobquzFggkV7a6GFoGD6iKkBgbeRB2IL4FgCbca4PMgyaTohvJtgewRRLj4+ytQRKg3yYYTGrYKjwf93YQnYKHOxauNBnqfuEulnuFm6FtVtcUYUIchwbJkqWiH6odm86+XNASpvFMYeVTDygs4DsWexgAZHd6D+xPXvOuYoVhI2F9jG9shmsLRQrgPEXpZ9sCoTXSeooU9P3JmowpJ1GxCtZPI0cJsx/uRzhc1BKkw899VWEJYkwCIUceZdyOgRFx1uEvdE5/KXk6YhZkYZOLrjBojUgHbvh8J0ru4+qeE61jrvJR46zQecaE76LmLSXVdLx1YtE+jjlLe49nSxEKA9vSFAzKLT6GrQWocTEY9dMRgr3Vguo0oeKEOlyuUSr36xcAiF2w8Hy7xNAj54sYMr6tmWlhYPrxcD5SRJve49ZLHIBWK+GirC4uNlmu8gGIpWghicQlA5Gc225KJJQRx9n0xLdrk3Oj1TNSvJq+JoZa//4rPpxI3+mKqpUhxg0b71Y9f8zVp26q7YOpFFPQx5MQL/buZH7+iKG3Vuh27q+OT2/8qW47THqNS3M18/w4wga7gf1X8v/BX5qzbxzG3JCzx3SIS5UbhRasWxtNHILdA4P9/nRUDv/X3cir6OHJe8d//XPmzALe2OpfzmVjJ/Alxwif91/O5qOxudmpiaenc5+bDAXn3pxkoK8fDo+Cx9azk3r3fyEISmdLfG97sC+levR5e//F4VDr985Ph8PXrm6Ojo5uby+H9yV5nPXD/D/UWwMsbY3GTAAAAAElFTkSuQmCC";
        userItem.info = new UserItem.Info();
        userItem.info.data = new UserItem.Info.Data();
        userItem.info.data.address = "Makati City";
        userItem.info.data.contact_number = "09755124876";
        UserData.insert(userItem);
        registrationActivity.startMainActivity("main");
    }



//    private void attemptLogin(){
//        new LoginRequest(getContext(), emailACTV.getText().toString(), passwordET.getText().toString())
//                .setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Logging in...", false, false))
//                .execute();
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onStop() {
//        EventBus.getDefault().unregister(this);
//        super.onStop();
//    }
//
//
//    @Subscribe
//    public void onResponse(LoginRequest.ServerResponse responseData) {
//        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
//        if(userSingleTransformer.status){
//            UserData.insert(userSingleTransformer.data);
//            UserData.insert(UserData.AUTHORIZATION, userSingleTransformer.token);
//            UserData.insert(UserData.FIRST_LOGIN, userSingleTransformer.first_login);
//            ToastMessage.show(getActivity(), userSingleTransformer.msg + userSingleTransformer.data.fname + " " + userSingleTransformer.data.lname, ToastMessage.Status.SUCCESS);
//            registrationActivity.startMainActivity("home");
//        }else{
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
//            if(responseData.getData().hasRequirements()){
//                ErrorResponseManager.first(emailACTV,userSingleTransformer.requires.email);
//                ErrorResponseManager.first(passwordET,userSingleTransformer.requires.password);
//            }
//        }
//    }
}
