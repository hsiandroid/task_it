package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.task_it.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalendarScheduleAdapter extends BaseAdapter implements View.OnClickListener {
    private Context context;
    private List<CalendarData> data;
    private LayoutInflater layoutInflater;
    private String currentMonthName;
    private int currentMonthNum;
    private int currentYear;

    private int actualDay;
    private int actualMonth;
    private String actualMonthName;
    private int actualYear;


    public CalendarScheduleAdapter(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        initCalendar();
    }

    private void initCalendar(){
        Calendar calendar = Calendar.getInstance();
        actualMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        actualMonth = calendar.get(Calendar.MONTH);
        actualYear = calendar.get(Calendar.YEAR);
        actualDay = calendar.get(Calendar.DATE);

        currentMonthNum = actualMonth;
        currentYear = actualYear;
        currentMonthName = actualMonthName;
        printDays();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_calendar, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.calendarData = data.get(position);
        holder.dayTXT.setText(String.valueOf(holder.calendarData.getDayNum()));

        if(holder.calendarData.isOffsetFirst || holder.calendarData.isOffsetLast ){
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.white_dark_gray));
            holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
        }else if(isActualMonth() && isActualYear() && holder.calendarData.getDayNum() == getActualDay()){
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
            holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
        }else{
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
            holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
        }


        if (holder.calendarData.selected) {
            holder.dayTXT.setBackground(ActivityCompat.getDrawable(context, R.drawable.selector_calendar_selected));
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
        }

        holder.dayTXT.setTag(position);
        holder.dayTXT.setOnClickListener(this);

        return convertView;
    }


    public void nextMonth(){
        // 0 = January
        // 11 = December
        if(currentMonthNum == 11){
            currentMonthNum = 0;
            currentYear++;
        }else{
            currentMonthNum++;
        }
        printDays();
    }

    public void prevMonth(){
        if(currentMonthNum == 0){
            currentMonthNum = 11;
            currentYear--;
        }else{
            currentMonthNum--;
        }
        printDays();
    }

    public void setCurrentMonthNum(int currentMonthNum) {
        this.currentMonthNum = currentMonthNum;
    }
    public void setCurrentMonthName(String currentMonthName) {
        this.currentMonthName = currentMonthName;
    }
    public void setActualDay(int actualDay) {
        this.actualDay = actualDay;
    }

    public void setCurrentYear(int currentYear) {
        this.currentYear = currentYear;
    }

    public void refresh(){
        printDays();
    }

    public String getCurrentMonthName(){
        return currentMonthName;
    }

    public int getCurrentMonthNum() {
        return currentMonthNum;
    }

    public int getCurrentYear(){
        return currentYear;
    }

    public boolean isActualMonth(){
        return currentMonthNum == actualMonth;
    }

    public boolean isActualYear(){
        return currentYear == actualYear;
    }

    public int getActualDay(){
        return actualDay;
    }

    public int getActualYear(){
        return actualYear;
    }

    public int getActualDayPosition(){
        return actualDay - 1;
    }


    public String getSelectedDate() {
        for (CalendarData calendarData : data) {
            if (calendarData.selected) {
                return calendarData.dayName + " " + calendarData.dayNum + ", " + calendarData.year;
            }
        }
        return "";
    }

    private void printDays(){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DATE, 1);
        calendar.set(Calendar.YEAR, currentYear);
        calendar.set(Calendar.MONTH, currentMonthNum);

        currentMonthName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());

        data.clear();

        int numDays = calendar.getActualMaximum(Calendar.DATE);
        int currentWeekDay = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        calendar.set(Calendar.DATE, - currentWeekDay);


        for (int i = 1 ; i <= currentWeekDay; i ++){
            calendar.add(Calendar.DATE, 1);
            CalendarData calendarData = new CalendarData(calendar);
            calendarData.isOffsetFirst = true;
            calendarData.isOffsetLast = false;
            calendarData.isDays = false;
            calendarData.selected = false;
            data.add(calendarData);
        }

        for (int i = 1 ; i <= numDays; i ++){
            calendar.add(Calendar.DATE, 1);
            CalendarData calendarData = new CalendarData(calendar);
            calendarData.isOffsetFirst = false;
            calendarData.isOffsetLast = false;
            calendarData.isDays = true;
            calendarData.selected = actualDay == i;
            data.add(calendarData);
        }

        currentWeekDay = 7 - calendar.get(Calendar.DAY_OF_WEEK);

        for (int i = 1 ; i <= currentWeekDay; i ++){
            calendar.add(Calendar.DATE, 1);
            CalendarData calendarData = new CalendarData(calendar);
            calendarData.isOffsetFirst = false;
            calendarData.isOffsetLast = true;
            calendarData.isDays = false;
            calendarData.selected = false;
            data.add(calendarData);
        }

        notifyDataSetChanged();
    }


    public class CalendarData{
        private int dayNum;
        private String dayName;
        private int month;
        private int year;
        public boolean isOffsetFirst;
        public boolean isOffsetLast;
        public boolean isDays;
        public boolean selected;

        public CalendarData(Calendar calendar){
            dayNum = calendar.get(Calendar.DATE);
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            dayName = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        }

        public int getDayNum(){
            return dayNum;
        }

        public int getYear() {
            return year;
        }
    }

    public class ViewHolder{
        CalendarData calendarData;
        @BindView(R.id.dayTXT)           TextView dayTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.dayTXT:
                setSelected((int)v.getTag());
                break;
        }

    }

    private void setSelected(int position){
        for (CalendarData calendarData : data) {
            calendarData.selected = data.indexOf(calendarData) == position && calendarData.isDays;
        }
        notifyDataSetChanged();
    }
} 
