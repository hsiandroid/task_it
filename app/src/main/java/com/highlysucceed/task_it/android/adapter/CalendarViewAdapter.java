package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.task_it.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalendarViewAdapter extends BaseAdapter implements View.OnClickListener {
	private Context context;
	private List<String> data;
    private LayoutInflater layoutInflater;
    private int selectedPosition = 0;

	public CalendarViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<String> data){
        this.data = data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_calendar_view, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.string = data.get(position);
        holder.view.setOnClickListener(this);

        holder.calendarTXT.setText(holder.string);

        if(selectedPosition == position){
            holder.calendarTXT.setTextSize(TypedValue.COMPLEX_UNIT_SP, 26);
            holder.calendarTXT.setTextColor(ActivityCompat.getColor(context, R.color.colorPrimary));
        }else{
            holder.calendarTXT.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
            holder.calendarTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
        }

		return convertView;
	}


    public void setSelectedValue(int position){
        this.selectedPosition = position;
        notifyDataSetChanged();
    }

    public class ViewHolder{
        String string;
        @BindView(R.id.calendarTXT)     TextView calendarTXT;

        View view;
        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        CalendarViewAdapter.ViewHolder viewHolder = (CalendarViewAdapter.ViewHolder) v.getTag();
        if(clickListener != null){
            clickListener.onItemClick(data.indexOf(viewHolder.string), viewHolder.string);
        }
    }


    private CalendarViewAdapter.ClickListener clickListener;
    public void setOnItemClickListener(CalendarViewAdapter.ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(int index, String string);
    }
} 
