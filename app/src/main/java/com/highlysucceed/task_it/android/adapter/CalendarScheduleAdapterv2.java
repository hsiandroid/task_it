package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.DaysModel;
import com.highlysucceed.task_it.vendor.android.java.CalendarModel;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CalendarScheduleAdapterv2 extends BaseAdapter {
    private Context context;
    private List<CalendarModel> data;
    private LayoutInflater layoutInflater;
    private static final int DAY_OFFSET = 1;
    private final String[] weekdays = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    private final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    private int daysInMonth;
    private int currentDayOfMonth;
    private int currentMonth;
    private int currentYear;
    private int currentWeekDay;
    private OnDayClickListener onDayClickListener;
    private List<DaysModel> daysModels;
    private int displayedMonth;
    private int displayedYear;

    public CalendarScheduleAdapterv2(Context context) {
        this.context = context;
        this.daysModels = new ArrayList<>();
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        initCalendar();
    }

    public void setBlockDays(List<DaysModel> daysModels){
        this.daysModels = daysModels;
        printMonth(displayedMonth, displayedYear);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_calendar, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.calendarModel = data.get(position);

        switch (holder.calendarModel.status) {
            case "GREY":
                holder.dayTXT.setText("");
                holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
                holder.backgroundCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
                break;
            case "BLUE":
                holder.dayTXT.setText(String.valueOf(holder.calendarModel.day));
                holder.dayTXT.setBackground(ActivityCompat.getDrawable(context, R.drawable.selector_calendar_current_date));
                holder.backgroundCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
                holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
                break;
            case "WHITE":
                holder.dayTXT.setText(String.valueOf(holder.calendarModel.day));
                holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
                holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
                holder.backgroundCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.white));
                break;
            case "BLACK":
                holder.dayTXT.setText(String.valueOf(holder.calendarModel.day));
                holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.background));
                holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.text_gray));
                holder.backgroundCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.background));
                break;
            case "RED":
                holder.dayTXT.setText(String.valueOf(holder.calendarModel.day));
                holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
                holder.dayTXT.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_red));
                holder.backgroundCON.setBackgroundColor(ActivityCompat.getColor(context, R.color.light_red));
                holder.dayTXT.setEnabled(false);
                break;
        }

        if (holder.calendarModel.selected) {
            holder.dayTXT.setBackground(ActivityCompat.getDrawable(context, R.drawable.selector_calendar_selected));
            holder.dayTXT.setTextColor(ActivityCompat.getColor(context, R.color.white));
        }

//        holder.dayTXT.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                for (CalendarModel calendarModel : data) {
//                    calendarModel.selected = false;
//                }
//
//                if (!data.get(data.indexOf(holder.calendarModel)).status.equals("GREY") && !data.get(data.indexOf(holder.calendarModel)).status.equals("BLACK")) {
//                    data.get(position).selected = true;
//                    notifyDataSetChanged();
//                }
//
////                if(onDayClickListener != null){
////                    onDayClickListener.onDayClick(data.get(data.indexOf(holder.calendarModel)));
////                    if(!data.get(data.indexOf(holder.calendarModel)).status.equals("GREY") && !data.get(data.indexOf(holder.calendarModel)).status.equals("BLACK")){
////                        onDayClickListener.onActiveDayClick(data.get(data.indexOf(holder.calendarModel)));
////                    }
////                }
//            }
//        });

        return convertView;
    }

    public void initCalendar() {
        Calendar calendar = Calendar.getInstance();
        setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
        setCurrentMonth(calendar.get(Calendar.MONTH));
        setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
        setCurrentYear(calendar.get(Calendar.YEAR));
    }

    public String getSelectedDate() {
        String date = "";

        for (CalendarModel calendarModel : data) {
            if (calendarModel.selected) {
                date = calendarModel.month + " " + calendarModel.day + ", " + calendarModel.year;
                break;
            }
        }

        return date;
    }

    public String getSelectedTimestamp() {
        for (CalendarModel calendarModel : data) {
            if (calendarModel.selected) {
                try {
                    SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                    SimpleDateFormat currentFormat = new SimpleDateFormat("yyyy-MMMM-dd");
                    Date date = currentFormat.parse(calendarModel.year + "-" + calendarModel.month + "-" + calendarModel.day);
                    return newFormat.format(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        return "";
    }

    public int getCurrentDayOfMonth() {
        return currentDayOfMonth;
    }

    public int getCurrentMonth() {
        return currentMonth;
    }

    public int getCurrentYear() {
        return currentYear;
    }

    private void setCurrentDayOfMonth(int currentDayOfMonth) {
        this.currentDayOfMonth = currentDayOfMonth;
    }

    private void setCurrentMonth(int currentDayOfMonth) {
        this.currentMonth = currentDayOfMonth;
    }

    private void setCurrentYear(int year) {
        this.currentYear = year;
    }

    public void setCurrentWeekDay(int currentWeekDay) {
        this.currentWeekDay = currentWeekDay;
    }


    public void setOnDayClickListener(OnDayClickListener onDayClickListener) {
        this.onDayClickListener = onDayClickListener;
    }
    public int getCurrentWeekDay() {
        return currentWeekDay;
    }

    private String getMonthAsString(int i) {
        return months[i];
    }

    private String getWeekDayAsString(int i) {
        return weekdays[i];
    }

    private int getNumberOfDaysOfMonth(int i) {
        return daysOfMonth[i];
    }

    public void printMonth(int mm, int yy) {
        displayedMonth = mm;
        displayedYear = yy;
        data.clear();
        int trailingSpaces = 0;
        int daysInPrevMonth = 0;
        int prevMonth = 0;
        int prevYear = 0;
        int nextMonth = 0;
        int nextYear = 0;
        int currentMonth = mm - 1;
        daysInMonth = getNumberOfDaysOfMonth(currentMonth);
        GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);

        if (currentMonth == 11) {
            prevMonth = currentMonth - 1;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
            nextMonth = 0;
            prevYear = yy;
            nextYear = yy + 1;
        } else if (currentMonth == 0) {
            prevMonth = 11;
            prevYear = yy - 1;
            nextYear = yy;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
            nextMonth = 1;

        } else {
            prevMonth = currentMonth - 1;
            nextMonth = currentMonth + 1;
            nextYear = yy;
            prevYear = yy;
            daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);

        }

        int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
        trailingSpaces = currentWeekDay;

        if (cal.isLeapYear(cal.get(Calendar.YEAR)))
            if (mm == 2) ++daysInMonth;
            else if (mm == 3)
                ++daysInPrevMonth;

        for (int i = 0; i < trailingSpaces; i++) {
            CalendarModel calendarModel = new CalendarModel();
            calendarModel.day = (daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i;
            calendarModel.month = getMonthAsString(prevMonth);
            calendarModel.year = prevYear;
            calendarModel.status = "GREY";
            calendarModel.selected = false;
            data.add(calendarModel);
        }

        for (int i = 1; i <= daysInMonth; i++) {

            Log.e("DAY", ">>>" + i + ">>>" + getCurrentDayOfMonth());
            Log.e("Month", ">>>" + currentMonth + ">>>" + getCurrentMonth());
            Log.e("Year", ">>>" + yy + ">>>" + getCurrentYear());

//             if (inactiveDate(currentMonth, i, yy)) {
//                CalendarModel calendarModel = new CalendarModel();
//                calendarModel.day = i;
//                calendarModel.month = getMonthAsString(currentMonth);
//                calendarModel.year = yy;
//                calendarModel.status = "BLACK";
//                calendarModel.selected = false;
//                data.add(calendarModel);}else
             if (invalidDate(currentMonth, i, yy)) {
                CalendarModel calendarModel = new CalendarModel();
                calendarModel.day = i;
                calendarModel.month = getMonthAsString(currentMonth);
                calendarModel.year = yy;
                calendarModel.status = "RED";
                 if (i == getCurrentDayOfMonth() && getCurrentMonth() == currentMonth && yy == getCurrentYear()){
                     calendarModel.selected = false;
                 }else {
                     calendarModel.selected = i == getCurrentDayOfMonth();
                 }
                data.add(calendarModel);
            }else if (i == getCurrentDayOfMonth() && getCurrentMonth() == currentMonth && yy == getCurrentYear()) {
                CalendarModel calendarModel = new CalendarModel();
                calendarModel.day = i;
                calendarModel.month = getMonthAsString(currentMonth);
                calendarModel.year = yy;
                calendarModel.status = "BLUE";
                 if (invalidDate(currentMonth, i, yy)){

                 }else {
                     calendarModel.selected = i == getCurrentDayOfMonth();
                 }
                data.add(calendarModel);

            }
           else {
                CalendarModel calendarModel = new CalendarModel();
                calendarModel.day = i;
                calendarModel.month = getMonthAsString(currentMonth);
                calendarModel.year = yy;
                calendarModel.status = "WHITE";
                calendarModel.selected = i == getCurrentDayOfMonth();
                data.add(calendarModel);

            }

        }

        for (int i = 0; i < data.size() % 7; i++) {
            CalendarModel calendarModel = new CalendarModel();
            calendarModel.day = i + 1;
            calendarModel.month = getMonthAsString(nextMonth);
            calendarModel.year = nextYear;
            calendarModel.status = "GREY";
            calendarModel.selected = false;
            data.add(calendarModel);
        }
        notifyDataSetChanged();
    }

    public boolean invalidDate(int month, int day, int year){
        for (DaysModel daysModel : daysModels){
            if (day == daysModel.day && getCurrentMonth() == month && year == getCurrentYear()){
                return true;
            }
        }
        return false;
    }

    public boolean inactiveDate(int month, int day, int year){
        month ++;
        try {
            String dateString = day + "/" + month + "/" + year;
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date date = sdf.parse(dateString);

            Calendar pastData = Calendar.getInstance();
            pastData.add(Calendar.DATE, -1);
            Calendar futureData = Calendar.getInstance();
            futureData.add(Calendar.DAY_OF_MONTH, 6);
            Log.e("Days" , ">>>" + futureData.getFirstDayOfWeek());

            if(date.getTime() < pastData.getTime().getTime()){
                return true;
            }

            if(date.getTime() > futureData.getTime().getTime()){
                return true;
            }

           return false;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public class ViewHolder {
        CalendarModel calendarModel;
        View view;
        @BindView(R.id.dayTXT)          TextView dayTXT;
        @BindView(R.id.backgroundCON)   View backgroundCON;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
    }

    public interface OnDayClickListener{
        void onDayClick(CalendarModel calendarModel);
        void onActiveDayClick(CalendarModel calendarModel);
    }
} 
