package com.highlysucceed.task_it.android.fragment;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.adapter.DefaultRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.SampleModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class SampleRecylerViewFragment extends BaseFragment {
    public static final String TAG = SampleRecylerViewFragment.class.getName().toString();

    private DefaultRecyclerViewAdapter defaultRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.defaultRV)  RecyclerView defaultRV;

    public static SampleRecylerViewFragment newInstance() {
        SampleRecylerViewFragment fragment = new SampleRecylerViewFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        defaultRecyclerViewAdapter = new DefaultRecyclerViewAdapter(getContext());
        defaultRecyclerViewAdapter.setNewData(getDefaultData());
        linearLayoutManager = new LinearLayoutManager(getContext());
        defaultRV.setLayoutManager(linearLayoutManager);
        defaultRV.setAdapter(defaultRecyclerViewAdapter);
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_recylerview;
    }

    private List<SampleModel> getDefaultData(){
        List<SampleModel> androidModels = new ArrayList<>();
        SampleModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new SampleModel();
            defaultItem.id = i;
            defaultItem.name = "name " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }
}
