package com.highlysucceed.task_it.android.fragment.location;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.LocationActivity;
import com.highlysucceed.task_it.data.model.server.LocationModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class LocationEditFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = LocationEditFragment.class.getName().toString();

    private LocationActivity locationActivity;

    private LocationModel locationModel;

    @BindView(R.id.streetAddressTXT)           EditText streetAddressTXT;
    @BindView(R.id.cityTXT)                    EditText cityTXT;
    @BindView(R.id.saveBTN)                    TextView saveBTN;

    public static LocationEditFragment newInstance(LocationModel locationModel) {
        LocationEditFragment locationEditFragment = new LocationEditFragment();
        locationEditFragment.locationModel = locationModel;
        return locationEditFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_create_location;
    }

    @Override
    public void onViewReady() {
        locationActivity = (LocationActivity) getActivity();
        locationActivity.setTitle(getString(R.string.title_location_edit));

        saveBTN.setOnClickListener(this);

        displayData(locationModel);
    }

    private void displayData(LocationModel locationModel){
        streetAddressTXT.setText(locationModel.street_address);
        cityTXT.setText(locationModel.city);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveBTN:
                getActivity().finish();
                break;
        }
    }
}
