package com.highlysucceed.task_it.android.fragment.main;

import android.content.Context;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.maps.GeoApiContext;
import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.activity.SearchActivity;
import com.highlysucceed.task_it.android.dialog.FilterDialog;
import com.highlysucceed.task_it.android.dialog.GreetingsDialog;
import com.highlysucceed.task_it.android.dialog.HomeDateDialog;
import com.highlysucceed.task_it.android.dialog.HomePriceRangeDialog;
import com.highlysucceed.task_it.android.dialog.HomeServiceDialog;
import com.highlysucceed.task_it.android.dialog.HomeTimeDialog;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.android.java.CurrencyFormatter;
import com.highlysucceed.task_it.vendor.android.java.Keyboard;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import icepick.State;

public class HomeFragment extends BaseFragment implements
//        GreetingsDialog.Callback,
        HomeServiceDialog.Callback,
//        HomeDateDialog.Callback,
//        HomeTimeDialog.Callback,
        HomePriceRangeDialog.Callback,
        View.OnClickListener, TextView.OnEditorActionListener {
    public static final String TAG = HomeFragment.class.getName().toString();

    private MainActivity mainActivity;

//    @BindView(R.id.serviceBTN)      View serviceBTN;
//    @BindView(R.id.dateBTN)         View dateBTN;
//    @BindView(R.id.timeDurationBTN) View timeDurationBTN;
//    @BindView(R.id.dateTXT)         TextView dateTXT;
//    @BindView(R.id.rangeBTN)        View rangeBTN;
//    @BindView(R.id.rangeTXT)        TextView rangeTXT;
//    @BindView(R.id.bookBTN)         View bookBTN;
//    @BindView(R.id.filterBTN)       ImageView filterBTN;
//    @BindView(R.id.serviceIV)       ImageView serviceIV;
//    @BindView(R.id.serviceNameTXT)  TextView serviceNameTXT;

    @BindView(R.id.pickupTXT)           EditText pickupTXT;
    @BindView(R.id.serviceTXT)           EditText serviceTXT;



    @State  int defaultMin;
    @State  int defaultMax;

    public static HomeFragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_home;
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setSelectedItem(R.id.nav_home);
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity = (MainActivity) getContext();
//        mainActivity.setTitle(getString(R.string.title_main_home));
        mainActivity.setupSettingButton(0);
        serviceTXT.setOnClickListener(this);
        pickupTXT.setOnClickListener(this);


//        serviceBTN.setOnClickListener(this);
//        timeDurationBTN.setOnClickListener(this);
//        dateBTN.setOnClickListener(this);
//        rangeBTN.setOnClickListener(this);
//        bookBTN.setOnClickListener(this);
//        filterBTN.setOnClickListener(this);

//        GreetingsDialog.newInstance(this).show(getChildFragmentManager(), GreetingsDialog.TAG);
    }

//    @Override
//    public void openLocationFragment(GreetingsDialog greetingsDialog) {
//        mainActivity.openLocationFragment();
//        greetingsDialog.dismiss();
//    }



    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent keyEvent) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            InputMethodManager imm = (InputMethodManager)v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            return true;
        }
        return false;

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pickupTXT:
                break;
            case R.id.serviceTXT:
            mainActivity.openSearchRefineFragment();
                break;
//            case R.id.serviceBTN:
//                HomeServiceDialog.newInstance(this).show(getChildFragmentManager(), HomeServiceDialog.TAG);
//                break;
//            case R.id.dateBTN:
//                HomeDateDialog.newInstance(this, "now").show(getChildFragmentManager(), HomeDateDialog.TAG);
//                break;
//            case R.id.timeDurationBTN:
//                HomeTimeDialog.newInstance(this, "now").show(getChildFragmentManager(), HomeTimeDialog.TAG);
//                break;
//            case R.id.rangeBTN:
//                HomePriceRangeDialog.newInstance(this, defaultMin, defaultMax).show(getChildFragmentManager(), HomePriceRangeDialog.TAG);
//                break;
//            case R.id.filterBTN:
//                FilterDialog.newInstance().show(getChildFragmentManager(), FilterDialog.TAG);
//                break;
//            case R.id.bookBTN:
//                mainActivity.startBookingServiceActivity("book");
//                break;
        }
    }

    @Override
    public void onServiceSelect(HomeServiceDialog homeServiceDialog) {

    }

//    @Override
//    public void onDateSelected(String date) {
//        dateTXT.setText(date);
//    }

    @Override
    public void onRangeChange(int min, int max) {
        defaultMin = min;
        defaultMax = max;

        String range = CurrencyFormatter.format("Php", defaultMin) + " - " +
                CurrencyFormatter.format("Php", defaultMax);
//        rangeTXT.setText(range);
    }



//    public void serviceHomeImages(Integer a){
//
//        switch (a){
//            case 0:
//                serviceIV.setImageResource(R.drawable.icon_service_default);
//                serviceNameTXT.setText("INTERIOR DESIGN");
//                break;
//            case 1:
//                serviceIV.setImageResource(R.drawable.service_pest_control);
//                serviceNameTXT.setText("PEST CONTROL");
//                break;
//            case 2:
//                serviceIV.setImageResource(R.drawable.service_plumbing);
//                serviceNameTXT.setText("PLUMBING");
//        }
//    }

}
