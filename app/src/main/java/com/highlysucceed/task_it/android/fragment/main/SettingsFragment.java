package com.highlysucceed.task_it.android.fragment.main;

import android.content.pm.PackageManager;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.SettingsListViewAdapter;
import com.highlysucceed.task_it.android.dialog.LogoutDialog;
import com.highlysucceed.task_it.android.dialog.ReferralDialog;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.android.widget.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

public class SettingsFragment extends BaseFragment implements SettingsListViewAdapter.ClickListener{

    public static final String TAG = SettingsFragment.class.getName().toString();
    private MainActivity mainActivity;

    private SettingsListViewAdapter profileAdapter;
    private SettingsListViewAdapter othersAdapter;
    private SettingsListViewAdapter notificationAdapter;
    private SettingsListViewAdapter aboutAdapter;

    private List<SettingItem> settingItem;

   /* @BindView(R.id.profileEHLV)         ExpandableHeightListView profileEHLV;*/
    @BindView(R.id.othersEHLV)          ExpandableHeightListView othersEHLV;

    public static SettingsFragment newInstance() {
        SettingsFragment settingsFragment = new SettingsFragment();
        return settingsFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_settings;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        mainActivity.setTitle(getString(R.string.title_main_settings));
        mainActivity.setupSettingButton(0);
      /*  mainActivity.setSelectedItem(R.id.nav_settings);*/

        /*setupProfileListView();
        sampleProfileData();*/

        setupOthersListView();
        sampleOtherData();
    }

    @Override
    public void onResume() {
        super.onResume();
       /* mainActivity.setSelectedItem(R.id.nav_settings);*/
    }

    /*private void setupProfileListView() {

        settingItem = new ArrayList<>();
        profileAdapter = new SettingsListViewAdapter(getContext());
        profileAdapter.setClickListener(this);
        profileEHLV.setAdapter(profileAdapter);
        profileAdapter.setNewData(settingItem);
    }

    private void sampleProfileData() {

        settingItem.clear();

        SettingItem settingItems = new SettingItem();
        *//*settingItems.id = 1;
        settingItems.name = "Profile";
        settingItems.isEnable = false;
        settingItems.description = "Profile Information";
        settingItem.add(settingItems);*//*

        settingItems = new SettingItem();
        settingItems.id = 2;
        settingItems.name = "Change Password";
        settingItems.isEnable =false;
        settingItems.description = "Update your password regularly";
        settingItem.add(settingItems);
    }*/

    private void setupOthersListView() {

        settingItem = new ArrayList<>();
        othersAdapter = new SettingsListViewAdapter(getContext());
        othersAdapter.setClickListener(this);
        othersEHLV.setAdapter(othersAdapter);
        othersAdapter.setNewData(settingItem);
    }

    private void sampleOtherData() {

        settingItem.clear();



        SettingItem settingItems;

        settingItems = new SettingItem();
        settingItems.id = 6;
        settingItems.name = "About";
        settingItems.isEnable = false;
        settingItems.description = getAppVersion();
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 7;
        settingItems.name = "Referral";
        settingItems.isEnable =false;
        settingItems.description ="Referral Section";
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 8;
        settingItems.name = "History";
        settingItems.isEnable =false;
        settingItems.description ="History Section";
        settingItem.add(settingItems);

        settingItems = new SettingItem();
        settingItems.id = 9;
        settingItems.name = "Help";
        settingItems.isEnable =false;
        settingItems.description ="Help Section";
        settingItem.add(settingItems);
    }

    private String getAppVersion(){
        String version;
        try {
            version = "App Build version " + getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName + " (Beta)";
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            version = "App Build version -- (Beta)";
        }

        return version;
    }



    @Override
    public void onItemClick(SettingsFragment.SettingItem settingItem) {
        switch (settingItem.id){
            case 1:
              /* mainActivity.openProfileFragment();*/
                break;
            case 2:

                break;
            case 3:
                break;
            case 4:
//                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
//                sharingIntent.setType("text/plain");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Stratus");
//                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "https://thingsilikeapp.com/");
//                startActivity(Intent.createChooser(sharingIntent, "Share"));
                break;
            case 5:
                mainActivity.startSettingsActivity("about");
                break;

            /*case 6:
                LogoutDialog.newInstance().show(getFragmentManager(), LogoutDialog.TAG);
                break;
*/
            case 7:
                ReferralDialog.newInstance().show(getFragmentManager(),ReferralDialog.TAG);
                break;

            case 8:
                mainActivity.startSettingsActivity("history");
                break;
            case 9:
                mainActivity.startSettingsActivity("help");
                break;
        }
    }


    public static class SettingItem extends AndroidModel {
        public int id;
        public String name;
        public String description;
        public boolean isEnable;
    }
}