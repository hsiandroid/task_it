package com.highlysucceed.task_it.android.fragment.settings;

import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.SettingsActivity;
import com.highlysucceed.task_it.android.dialog.ImagePickerDialog;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import icepick.State;

public class EditProfileFragment extends BaseFragment implements
        View.OnClickListener,
        ImagePickerDialog.ImageCallback{

    public static final String TAG = EditProfileFragment.class.getName().toString();

    private SettingsActivity settingsActivity;

    @BindView(R.id.avatarCIV)  ImageView avatarCIV;

    @State  File file;

    public static EditProfileFragment newInstance() {
        EditProfileFragment editProfileFragment = new EditProfileFragment();
        return editProfileFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_edit_profile;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle("Edit Profile");

        avatarCIV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveBTN:
                attemptSignUp();
                break;
            case R.id.changeAvatarTXT:
            case R.id.avatarCIV:
                ImagePickerDialog.newInstance("Change Avatar", true, this).show(getChildFragmentManager(), ImagePickerDialog.TAG);
                break;
        }
    }

    private void attemptSignUp(){
//        ProfileEditRequest profileEditRequest = new ProfileEditRequest(getContext());
//
//        if(file != null){
//            profileEditRequest.addPart(Variable.server.key.FILE, file);
//        }
//
//        profileEditRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Updating Profile...", false, false))
//                .addAuthorization(UserData.getString(UserData.AUTHORIZATION))
//                .addRequestBodyParameters(Variable.server.key.INCLUDE, "info,social,statistics")
//                .addRequestBodyParameters(Variable.server.key.NAME, nameET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.CONTACT_NUMBER, "+63" + result.getExtractedValue())
//                .addRequestBodyParameters(Variable.server.key.USERNAME, usernameET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.EMAIL, emailET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.GENDER, femaleRBTN.isChecked() ? "female" :  "male")
//                .addRequestBodyParameters(Variable.server.key.STREET_ADDRESS, street_addressET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.CITY, cityET.getText().toString())
//                .addRequestBodyParameters(Variable.server.key.BIRTHDATE, birthdayReFormat(birthdayTXT.getText().toString()))
//                .addRequestBodyParameters(Variable.server.key.STATE, stateET.getText().toString())
////                .addRequestBodyParameters(Variable.server.key.COUNTRY, countryData.code1)
//                .addRequestBodyParameters(Variable.server.key.PASSWORD, passwordET.getText().toString())
//                .execute();
    }

    @Override
    public void result(File file) {
        this.file = file;
        Picasso.with(getContext())
                .load(file)
                .into(avatarCIV);
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//
//    @Override
//    public void onStop() {
//        EventBus.getDefault().unregister(this);
//        super.onStop();
//    }
//
//    @Subscribe
//    public void onResponse(ProfileEditRequest.ServerResponse responseData) {
//        UserSingleTransformer userSingleTransformer = responseData.getData(UserSingleTransformer.class);
//        if(userSingleTransformer.status){
//            passwordET.setText("");
//            displayData(userSingleTransformer.data);
//            UserData.insert(userSingleTransformer.data);
//            scrollView.smoothScrollTo(0, 0);
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.SUCCESS);
//        }else{
//            ToastMessage.show(getActivity(), userSingleTransformer.msg, ToastMessage.Status.FAILED);
//            if(userSingleTransformer.hasRequirements()){
//                ErrorResponseManager.first(nameET, userSingleTransformer.requires.fname);
//                ErrorResponseManager.first(stateET, userSingleTransformer.requires.state);
//                ErrorResponseManager.first(countryTXT, userSingleTransformer.requires.country);
//                ErrorResponseManager.first(emailET, userSingleTransformer.requires.email);
//                ErrorResponseManager.first(passwordET, userSingleTransformer.requires.password);
//            }
//        }
//        Log.e("Address",">>>" + userSingleTransformer.data.info.data.street_address);
//    }

//    @Override
//    public void onSelectCountry(CountryData.Country country) {
//        countryData = country;
//        countryTXT.setText(country.country);
//    }
}
