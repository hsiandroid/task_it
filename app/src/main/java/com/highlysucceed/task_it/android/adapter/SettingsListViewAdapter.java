package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.fragment.main.SettingsFragment;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;

import butterknife.BindView;

public class SettingsListViewAdapter extends BaseListViewAdapter<SettingsListViewAdapter.ViewHolder, SettingsFragment.SettingItem> {

    private ClickListener clickListener;

    public SettingsListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_setting));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().name);
        holder.descriptionTXT.setText(holder.getItem().description);
        holder.switchBTN.setVisibility(holder.getItem().isEnable ? View.VISIBLE : View.GONE);

    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)         TextView nameTXT;
        @BindView(R.id.descriptionTXT)  TextView descriptionTXT;
        @BindView(R.id.switchBTN)       Switch switchBTN;

        public ViewHolder(View view) {
            super(view);
        }

        public SettingsFragment.SettingItem getItem() {
            return (SettingsFragment.SettingItem) super.getItem();
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener{
        void onItemClick(SettingsFragment.SettingItem settingItem);
    }
} 
