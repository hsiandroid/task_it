package com.highlysucceed.task_it.android.fragment.main;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.dialog.FinishRequestDialog;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ServiceStatusFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = ServiceStatusFragment.class.getName().toString();

    private MainActivity mainActivity;

    @BindView(R.id.serviceMessageBTN)         ImageView serviceMessageBTN;
    @BindView(R.id.serviceCancelBTN)          ImageView serviceCancelBTN;

    long time;

    public static ServiceStatusFragment newInstance() {
        ServiceStatusFragment fragment = new ServiceStatusFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity)getActivity();
        mainActivity.setupSettingButton(0);
        mainActivity.setTitle("Service Start");
        mainActivity = (MainActivity)getContext();
        serviceCancelBTN.setOnClickListener(this);
        serviceMessageBTN.setOnClickListener(this);
        setTextBTN();
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_service_status;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.serviceMessageBTN:
                mainActivity.startConversationActivity();
                break;
            case R.id.serviceCancelBTN:
                mainActivity.openHomeFragment();
                break;
        }
    }

    public void setTextBTN() {

            new CountDownTimer(10000, 1000) {

                public void onTick(long millisUntilFinished) {
                   time = millisUntilFinished / 1000;
                }

                public void onFinish() {
                    FinishRequestDialog.newInstance().show(getChildFragmentManager(),FinishRequestDialog.TAG);
                }
            }.start();


    }
}
