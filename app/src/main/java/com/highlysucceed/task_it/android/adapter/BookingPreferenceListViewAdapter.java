package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.BookingPreferenceModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class BookingPreferenceListViewAdapter extends BaseListViewAdapter<BookingPreferenceListViewAdapter.ViewHolder, BookingPreferenceModel> {

    private ClickListener clickListener;

    public BookingPreferenceListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_service_preference));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().name);
        holder.serviceNameTXT.setText(holder.getItem().service_name);
        holder.designationTXT.setText(holder.getItem().designation);

        Picasso.with(getContext())
                .load(holder.getItem().full_path)
                .error(R.drawable.placeholder_avatar)
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.imageCIV);
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.serviceNameTXT)      TextView serviceNameTXT;
        @BindView(R.id.designationTXT)      TextView designationTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public BookingPreferenceModel getItem() {
            return (BookingPreferenceModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onItemClick(BookingPreferenceModel bookingPreferenceModel);
    }
} 
