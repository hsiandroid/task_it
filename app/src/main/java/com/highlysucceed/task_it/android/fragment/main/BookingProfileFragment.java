package com.highlysucceed.task_it.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.BookingProfileRecyclerViewAdapter;
import com.highlysucceed.task_it.android.dialog.defaultdialog.ConfirmationDialog;
import com.highlysucceed.task_it.data.model.server.BookingProfileModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BookingProfileFragment extends BaseFragment
        implements View.OnClickListener,BookingProfileRecyclerViewAdapter.ClickListener {
    public static final String TAG = BookingProfileFragment.class.getName().toString();

    private MainActivity mainActivity;

    @BindView(R.id.bookingProfileRV)              RecyclerView bookingProfileRV;
    @BindView(R.id.addBookingProfileBTN)          TextView addBookingProfileBTN;
    @BindView(R.id.bookingProfileSRL)             SwipeRefreshLayout bookingProfileSRL;

    private BookingProfileRecyclerViewAdapter bookingProfileRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    public static BookingProfileFragment newInstance() {
        BookingProfileFragment bookingProfileFragment = new BookingProfileFragment();
        return bookingProfileFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_booking_profile;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_booking_profile));
        mainActivity.setupSettingButton(0);
        addBookingProfileBTN.setOnClickListener(this);

        setUpBookingListView();
    }

    private void setUpBookingListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        bookingProfileRecyclerViewAdapter = new BookingProfileRecyclerViewAdapter(getContext());
        bookingProfileRecyclerViewAdapter.setClickListener(this);
        bookingProfileRecyclerViewAdapter.setNewData(getData());
        bookingProfileRV.setLayoutManager(linearLayoutManager);
        bookingProfileRV.setAdapter(bookingProfileRecyclerViewAdapter);

    }

    private List<BookingProfileModel> getData(){
        List<BookingProfileModel> bookingProfileModels = new ArrayList<>();

       for (int i=0;i<=4;i++){
           BookingProfileModel bookingProfileModel = new BookingProfileModel();
           bookingProfileModel.id = 1;
           bookingProfileModel.address = "Makati City, Philipines";
           bookingProfileModel.name = "John Doe";
           bookingProfileModel.full_path = "https://cdn.pixabay.com/photo/2014/04/03/10/32/businessman-310819_960_720.png";
           bookingProfileModels.add(bookingProfileModel);
       }

        return bookingProfileModels;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
       /* mainActivity.setSelectedItem(R.id.nav_booking_profile);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addBookingProfileBTN:
                mainActivity.startBookingProfileActivity(null,"create");
                break;
        }
    }

    @Override
    public void onItemView(BookingProfileModel bookingProfileModel) {

    }

    @Override
    public void onItemEdit(BookingProfileModel bookingProfileModel) {
        mainActivity.startBookingProfileActivity(bookingProfileModel, "edit");
    }

    @Override
    public void onItemDelete(BookingProfileModel bookingProfileModel) {
            deleteBookingProfileDialog(bookingProfileModel);
    }

    private void deleteBookingProfileDialog(final BookingProfileModel bookingProfileModel){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Booking Profile")
                .setNote("Are you sure you want to remove this booking profile?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bookingProfileRecyclerViewAdapter.removeItem(bookingProfileModel);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }
}
