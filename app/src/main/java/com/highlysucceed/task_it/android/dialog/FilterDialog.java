package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class FilterDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = FilterDialog.class.getName().toString();

	@BindView(R.id.genderSPR) 			Spinner genderSPR;
	@BindView(R.id.saveBTN) 			TextView saveBTN;
	@BindView(R.id.star1) 				ImageView star1;
	@BindView(R.id.star2)				ImageView star2;
	@BindView(R.id.star3)				ImageView star3;
	@BindView(R.id.star4)				ImageView star4;
	@BindView(R.id.star5)				ImageView star5;

	public static FilterDialog newInstance() {
		FilterDialog dialog = new FilterDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_filter_option;
	}

	public void starsOnClick(){
		star1.setOnClickListener(this);
		star2.setOnClickListener(this);
		star3.setOnClickListener(this);
		star4.setOnClickListener(this);
		star5.setOnClickListener(this);
	}

	@Override
	public void onViewReady() {
		starsOnClick();
		saveBTN.setOnClickListener(this);
		setupTypeSPR();
	}

	private void setupTypeSPR(){
		final List<String> orderBy = new ArrayList<String>();
		orderBy.add("Male");
		orderBy.add("Female");

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getContext(), R.layout.item_spinner, orderBy);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		genderSPR.setAdapter(dataAdapter);

		genderSPR.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				switch (parent.getItemAtPosition(position).toString()){
					case "By percentage":

						break;
					case "By exact amount":

						break;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){

			case R.id.saveBTN:
				dismiss();
				break;
			case R.id.star1:
				setUpRating(1);
				break;
			case R.id.star2:
				setUpRating(2);
				break;
			case R.id.star3:
				setUpRating(3);
				break;
			case R.id.star4:
				setUpRating(4);
				break;
			case R.id.star5:
				setUpRating(5);
				break;

		}
	}

	private void setUpRating(int count){
		switch (count){
			case 1:
				star1.setImageResource(R.drawable.icon_star_full);
				star2.setImageResource(R.drawable.icon_star_empty);
				star3.setImageResource(R.drawable.icon_star_empty);
				star4.setImageResource(R.drawable.icon_star_empty);
				star5.setImageResource(R.drawable.icon_star_empty);
				break;
			case 2:
				star1.setImageResource(R.drawable.icon_star_full);
				star2.setImageResource(R.drawable.icon_star_full);
				star3.setImageResource(R.drawable.icon_star_empty);
				star4.setImageResource(R.drawable.icon_star_empty);
				star5.setImageResource(R.drawable.icon_star_empty);
				break;
			case 3:
				star1.setImageResource(R.drawable.icon_star_full);
				star2.setImageResource(R.drawable.icon_star_full);
				star3.setImageResource(R.drawable.icon_star_full);
				star4.setImageResource(R.drawable.icon_star_empty);
				star5.setImageResource(R.drawable.icon_star_empty);
				break;
			case 4:
				star1.setImageResource(R.drawable.icon_star_full);
				star2.setImageResource(R.drawable.icon_star_full);
				star3.setImageResource(R.drawable.icon_star_full);
				star4.setImageResource(R.drawable.icon_star_full);
				star5.setImageResource(R.drawable.icon_star_empty);
				break;
			case 5:
				star1.setImageResource(R.drawable.icon_star_full);
				star2.setImageResource(R.drawable.icon_star_full);
				star3.setImageResource(R.drawable.icon_star_full);
				star4.setImageResource(R.drawable.icon_star_full);
				star5.setImageResource(R.drawable.icon_star_full);
				break;
			default:
				star1.setImageResource(R.drawable.icon_star_empty);
				star2.setImageResource(R.drawable.icon_star_empty);
				star3.setImageResource(R.drawable.icon_star_empty);
				star4.setImageResource(R.drawable.icon_star_empty);
				star5.setImageResource(R.drawable.icon_star_empty);
		}
	}
}
