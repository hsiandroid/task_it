package com.highlysucceed.task_it.android.fragment.settings;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.activity.SettingsActivity;
import com.highlysucceed.task_it.android.adapter.BookingHistoryRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.BookingHistoryModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BookingHistoryFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = BookingHistoryFragment.class.getName().toString();

    private SettingsActivity settingsActivity;
    private LinearLayoutManager linearLayoutManager;
    private BookingHistoryRecyclerViewAdapter bookingHistoryRecyclerViewAdapter;

    @BindView(R.id.bookingHistorySRL)           SwipeRefreshLayout bookingHistorySRL;
    @BindView(R.id.bookingHistoryRV)            RecyclerView bookingHistoryRV;

    public static BookingHistoryFragment newInstance() {
        BookingHistoryFragment bookingHistoryFragment = new BookingHistoryFragment();
        return bookingHistoryFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_booking_history;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getContext();
        settingsActivity.setTitle(getString(R.string.title_main_booking_history));

        setUpBookingHistoryListView();
    }

    private void setUpBookingHistoryListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        bookingHistoryRecyclerViewAdapter = new BookingHistoryRecyclerViewAdapter(getContext());
        bookingHistoryRV.setLayoutManager(linearLayoutManager);
        bookingHistoryRV.setAdapter(bookingHistoryRecyclerViewAdapter);
        bookingHistoryRecyclerViewAdapter.setNewData(getData());
    }

    private List<BookingHistoryModel> getData(){
        List<BookingHistoryModel> bookingHistoryModels = new ArrayList<>();

       for (int i=0;i<=3;i++){
           BookingHistoryModel bookingHistoryModel = new BookingHistoryModel();
           bookingHistoryModel.id =1;
           bookingHistoryModel.name = "Shia Shanks";
           bookingHistoryModel.away = "5km away";
           bookingHistoryModel.time_passed = "08/10/2017 05:00pm";
           bookingHistoryModel.priceRange = "Php 5,000.00";
           bookingHistoryModel.full_path = "http://rotarymeansbusiness.com/wp-content/uploads/avatar-female.png";
           bookingHistoryModels.add(bookingHistoryModel);
       }

        return  bookingHistoryModels;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
       /* mainActivity.setSelectedItem(R.id.nav_history);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
