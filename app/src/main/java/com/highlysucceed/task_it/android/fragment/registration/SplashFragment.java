package com.highlysucceed.task_it.android.fragment.registration;

import android.os.Handler;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.data.preference.UserData;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

public class SplashFragment extends BaseFragment{

    public static final String TAG = SplashFragment.class.getName().toString();

    private RegistrationActivity registrationActivity;

    private Handler handler;
    private Runnable runnable;

    public static SplashFragment newInstance() {
        SplashFragment splashFragment = new SplashFragment();
        return splashFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_splash;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();

    }
    @Override
    public void onResume() {
        super.onResume();


        runnable = new Runnable() {
            @Override
            public void run() {
                registrationActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (UserData.isLogin()) {
                            registrationActivity.startMainActivity("main");
                        } else {
                            registrationActivity.openLoginFragment();
                        }
                    }
                });
            }
        };

        handler = new Handler();
        handler.postDelayed(runnable, 3000);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (handler != null) {
            handler.removeCallbacks(runnable);
        }
    }

}
