package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.LocationModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class LocationRecyclerViewAdapter extends BaseRecylerViewAdapter<LocationRecyclerViewAdapter.ViewHolder, LocationModel>{

    private ClickListener clickListener;

    public LocationRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_location));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.otherOptionBTN.setTag(holder.getItem());
        holder.otherOptionBTN.setOnClickListener(this);
        holder.streetAddressTXT.setText(holder.getItem().street_address);
        holder.cityTXT.setText(holder.getItem().city + ", " + holder.getItem().province);
    }

    private void otherOption(final View v){
        final PopupMenu popup = new PopupMenu(getContext(), v);
        popup.getMenuInflater().inflate(R.menu.location_other_option, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.view:
                        if(clickListener != null){
                            clickListener.onItemView((LocationModel) v.getTag());
                        }
                        break;
                    case R.id.edit: if(clickListener != null){
                        clickListener.onItemEdit((LocationModel) v.getTag());
                    }
                        break;
                    case R.id.delete:
                        if(clickListener != null){
                            clickListener.onItemDelete((LocationModel) v.getTag());
                        }
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.streetAddressTXT)      TextView streetAddressTXT;
        @BindView(R.id.cityTXT)               TextView cityTXT;
        @BindView(R.id.otherOptionBTN)        ImageView otherOptionBTN;

        public ViewHolder(View view) {
            super(view);
        }

        public LocationModel getItem() {
            return (LocationModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.otherOptionBTN:
                otherOption(v);
                break;
        }
//        if(clickListener != null){
//            clickListener.onItemClick((LocationModel) v.getTag());
//        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener{
        void onItemView(LocationModel locationModel);
        void onItemEdit(LocationModel locationModel);
        void onItemDelete(LocationModel locationModel);
    }
} 
