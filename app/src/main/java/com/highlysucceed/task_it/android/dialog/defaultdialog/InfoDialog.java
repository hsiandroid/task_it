package com.highlysucceed.task_it.android.dialog.defaultdialog;

import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class InfoDialog extends BaseDialog {
	public static final String TAG = InfoDialog.class.getName().toString();

    @State int icon;
    @State String title;
    @State String description;
    @State String button;

    @BindView(R.id.iconIV)                  ImageView iconIV;
    @BindView(R.id.descriptionTXT)          TextView descriptionTXT;
    @BindView(R.id.positiveBTN)             TextView positiveBTN;

    private View.OnClickListener clickListener;

	public static InfoDialog Builder() {
		InfoDialog infoDialog = new InfoDialog();
		return infoDialog;
	}
	@Override
	public int onLayoutSet() {
		return R.layout.dialog_info;
	}

	@Override
	public void onViewReady() {
        if(icon != 0){
            iconIV.setImageDrawable(ActivityCompat.getDrawable(getContext(), icon));
        }

        if(description != null){
            descriptionTXT.setText(description);
        }

        if(positiveBTN != null){
            positiveBTN.setText(button);
        }

        if(clickListener != null){
            positiveBTN.setOnClickListener(clickListener);
        }
	}

	@Override
	public void onStart() {
		super.onStart();
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	public InfoDialog setIcon(int icon){
		this.icon = icon;
		return this;
	}

	public InfoDialog setTitle(String title){
        this.title = title;
		return this;
	}

	public InfoDialog setDescription(String description){
        this.description = description;
		return this;
	}

	public InfoDialog setButtonText(String button){
        this.button = button;
		return this;
	}

	public InfoDialog setButtonClickListener(View.OnClickListener clickListener){
        this.clickListener = clickListener;
		return this;
	}

	public void build(FragmentManager manager){
        show(manager, TAG);
	}
}


