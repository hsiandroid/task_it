package com.highlysucceed.task_it.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.InboxRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.InboxModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class InboxFragment extends BaseFragment implements InboxRecyclerViewAdapter.ClickListener {
    public static final String TAG = InboxFragment.class.getName().toString();

    private MainActivity mainActivity;
    private InboxRecyclerViewAdapter inboxRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.inboxSRL)            SwipeRefreshLayout inboxSRL;
    @BindView(R.id.inboxRV)             RecyclerView inboxRV;

    public static InboxFragment newInstance() {
        InboxFragment fragment = new InboxFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_inbox;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_inbox));
        mainActivity.setupSettingButton(0);
        setUpInboxListView();
    }

    private void setUpInboxListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        inboxRecyclerViewAdapter = new InboxRecyclerViewAdapter(getContext());
        inboxRV.setLayoutManager(linearLayoutManager);
        inboxRV.setAdapter(inboxRecyclerViewAdapter);
        inboxRecyclerViewAdapter.setNewData(getData());
        inboxRecyclerViewAdapter.setClickListener(this);
    }

    private List<InboxModel> getData(){
        List<InboxModel> inboxModels = new ArrayList<>();
        InboxModel inboxModel = new InboxModel();
        inboxModel.id = 1;
        inboxModel.name = "John Doe";
        inboxModels.add(inboxModel);

        inboxModel = new InboxModel();
        inboxModel.id = 1;
        inboxModel.name = "Jane Doe";
        inboxModels.add(inboxModel);

        return inboxModels;
    }

    @Override
    public void onItemClick(InboxModel inboxModel) {
        mainActivity.startConversationActivity();
    }
}
