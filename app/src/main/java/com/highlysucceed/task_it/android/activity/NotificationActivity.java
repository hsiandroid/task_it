package com.highlysucceed.task_it.android.activity;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.fragment.notification.NotificationSettingsFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class NotificationActivity extends RouteActivity {
    public static final String TAG = NotificationActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {
        registerBackPress(true);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "settings":
                openNotificationSettingsFragment();
                break;
        }
    }

    public void openNotificationSettingsFragment(){
        switchFragment(NotificationSettingsFragment.newInstance());
    }
}
