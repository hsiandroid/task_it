package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;

import com.highlysucceed.task_it.vendor.android.base.BaseDialog;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class FinishRequestDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = FinishRequestDialog.class.getName().toString();

	private MainActivity mainActivity;

	@BindView(R.id.imageCIV)			ImageView imageCIV;
	@BindView(R.id.nameTXT)				TextView nameTXT;
	@BindView(R.id.contentTXT)			TextView contentTXT;
	@BindView(R.id.destinationTXT)		TextView destinationTXT;
	@BindView(R.id.finishBTN)			TextView finishBTN;

	public static FinishRequestDialog newInstance() {
		FinishRequestDialog dialog = new FinishRequestDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_finish_request;
	}

	@Override
	public void onViewReady() {
		finishBTN.setOnClickListener(this);
		mainActivity = (MainActivity)getContext();
		Picasso.with(getContext())
				.load("https://site.sportame.com/wp-content/uploads/sites/5/2014/07/avatar-3.jpg")
				.placeholder(R.drawable.placeholder_avatar)
				.error(R.drawable.placeholder_avatar)
				.into(imageCIV);

		nameTXT.setText("John Smith");
		contentTXT.setText("Interior Design");
		destinationTXT.setText("San Juan City");
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.finishBTN:
				/*ClientRatingDialog.newInstance().show(((BaseActivity) getContext()).getSupportFragmentManager(),ClientRatingDialog.TAG);*/
				mainActivity.openHomeFragment();
				break;
		}
	}
}
