package com.highlysucceed.task_it.android.fragment.settings;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.SettingsActivity;
import com.highlysucceed.task_it.android.adapter.HelpRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.HelpModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class HelpFragment extends BaseFragment implements HelpRecyclerViewAdapter.ClickListener{
    public static final String TAG = HelpFragment.class.getName().toString();

    private SettingsActivity settingActivity;

    private HelpRecyclerViewAdapter helpRecyclerViewAdapter;
    public LinearLayoutManager helpLLM;

    @BindView(R.id.helpRV)          RecyclerView helpRV;
    @BindView(R.id.helpSRL)         SwipeRefreshLayout helpSRL;

    public static HelpFragment newInstance() {
        HelpFragment helpFragment = new HelpFragment();
        return helpFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_help;
    }

    @Override
    public void onViewReady() {
        settingActivity = (SettingsActivity) getContext();
        settingActivity.setTitle(getString(R.string.title_main_help));

        setUpHelpListView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void setUpHelpListView(){
        helpLLM = new LinearLayoutManager(getContext());
        helpRV.setLayoutManager(helpLLM);
        helpRecyclerViewAdapter = new HelpRecyclerViewAdapter(getContext());
        helpRV.setAdapter(helpRecyclerViewAdapter);
        helpRecyclerViewAdapter.setNewData(getData());
        helpRecyclerViewAdapter.setClickListener(this);
    }

    private List<HelpModel> getData(){
        List<HelpModel> helpModels = new ArrayList<>();

        HelpModel helpModel =  new HelpModel();
        helpModel.id = 1;
        helpModel.title = "Terms of Conditions";
        helpModels.add(helpModel);

        helpModel = new HelpModel();
        helpModel.id = 2;
        helpModel.title ="FAQs";
        helpModels.add(helpModel);

        helpModel = new HelpModel();
        helpModel.id = 3;
        helpModel.title ="Support";
        helpModels.add(helpModel);

        return  helpModels;
    }

    @Override
    public void onItemClick(HelpModel helpModel) {
       switch (helpModel.id){
           case 1:
               /*settingActivity.startNotificationActivity("settings");*/
               break;
       }
    }
}
