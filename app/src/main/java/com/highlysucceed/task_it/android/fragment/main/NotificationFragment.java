package com.highlysucceed.task_it.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.NotificationRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.NotificationModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NotificationFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = NotificationFragment.class.getName().toString();

    private MainActivity mainActivity;
    private NotificationRecyclerViewAdapter notificationRecyclerViewAdapter;
    public LinearLayoutManager notificationLLM;
    private List<NotificationModel> notificationModels;

    @BindView(R.id.notificationRV)      RecyclerView notificationRV;
    @BindView(R.id.notificationSRL)     SwipeRefreshLayout notificationSRL;


    public static NotificationFragment newInstance() {
        NotificationFragment notificationFragment = new NotificationFragment();
        return notificationFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_notification;
    }

    @Override
    public void onViewReady() {
        mainActivity =(MainActivity) getContext();
        mainActivity.setupSettingButton(2);
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_notification));

        setUpNotificationListView();
        sampleData();
    }

    private void setUpNotificationListView(){
        notificationLLM = new LinearLayoutManager(getContext());
        notificationRV.setLayoutManager(notificationLLM);
        notificationRecyclerViewAdapter = new NotificationRecyclerViewAdapter(getContext());
        notificationRV.setAdapter(notificationRecyclerViewAdapter);
    }

    private void sampleData(){
        notificationModels = new ArrayList<>();
        NotificationModel notificationModel = new NotificationModel();
        notificationModel.id = 1;
        notificationModel.title = "New Feature";
        notificationModel.content = "The service provider for \"Interior Design\" is now on it\'s way.";
        notificationModel.time_passed = "Just Now";
        notificationModels.add(notificationModel);

        notificationModel = new NotificationModel();
        notificationModel.id = 2;
        notificationModel.title = "Welcome to TaskIt!";
        notificationModel.content = "Your request for \"Interior Design\" is accepted by John Smith of Shag Carpet";
        notificationModel.time_passed = "10:12 am";
        notificationModels.add(notificationModel);

        notificationRecyclerViewAdapter.setNewData(notificationModels);

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
       /* mainActivity.setSelectedItem(R.id.nav_notification);*/
    }

    @Override
    public void onClick(View view) {

    }
}
