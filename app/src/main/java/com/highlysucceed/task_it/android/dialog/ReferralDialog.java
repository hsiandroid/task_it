package com.highlysucceed.task_it.android.dialog;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class ReferralDialog extends BaseDialog implements View.OnClickListener {
	public static final String TAG = ReferralDialog.class.getName().toString();

	@BindView(R.id.facebookCIV)			ImageView facebookCIV;
	@BindView(R.id.messageCIV)			ImageView messageCIV;
	@BindView(R.id.emailCIV)			ImageView emailCIV;

	public static ReferralDialog newInstance() {
		ReferralDialog dialog = new ReferralDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_referral;
	}

	@Override
	public void onViewReady() {
		facebookCIV.setOnClickListener(this);
		messageCIV.setOnClickListener(this);
		emailCIV.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogWrapContent();
	}

	private void sendMessage(){
		Intent sendIntent = new Intent(Intent.ACTION_VIEW);
		sendIntent.setData(Uri.parse("sms:"));
		sendIntent.putExtra("sms_body", getCode());
		startActivity(sendIntent);
	}

	private void redirectToGmail(){
		Intent emailIntent = new Intent(Intent.ACTION_SEND);
		emailIntent.setType("plain/text");
		emailIntent.putExtra(Intent.EXTRA_SUBJECT, "TASK IT App");
		emailIntent.putExtra(Intent.EXTRA_TEXT, getCode());
		Intent webIntent = new Intent(Intent.ACTION_VIEW,
				Uri.parse("http://mail.google.com/mail/u/0/#sent?compose=new"));
		try {
			startActivity(emailIntent);
		}catch (ActivityNotFoundException ex){
			startActivity(webIntent);
		}
	}

    public void shareTheApp(){
        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "TASK IT");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, getCode());
        startActivity(Intent.createChooser(sharingIntent, "Share TASK IT"));
    }

    private String getCode(){
		return "Referral Code: " + "#####";
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.facebookCIV:
			    shareTheApp();
				break;
			case R.id.messageCIV:
				sendMessage();
				break;
			case R.id.emailCIV:
				redirectToGmail();
				break;
		}
	}
}
