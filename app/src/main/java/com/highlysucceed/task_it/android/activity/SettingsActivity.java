package com.highlysucceed.task_it.android.activity;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.dialog.LogoutDialog;
import com.highlysucceed.task_it.android.fragment.main.ProfileFragment;
import com.highlysucceed.task_it.android.fragment.settings.BookingHistoryFragment;
import com.highlysucceed.task_it.android.fragment.settings.AboutFragment;
import com.highlysucceed.task_it.android.fragment.settings.ChangePasswordFragment;
import com.highlysucceed.task_it.android.fragment.settings.EditProfileFragment;
import com.highlysucceed.task_it.android.fragment.settings.HelpFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class SettingsActivity extends RouteActivity{
    public static final String TAG = SettingsActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {
        registerBackPress(true);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "edit":
                openEditProfileFragment();
                break;
            case "password":
                openChangePasswordFragment();
                break;
            case "about":
                openAboutFragment();
                break;
            case "history":
                openHistoryFragment();
                break;
            case "help":
                openHelpFragment();
                break;
            case "logout":
                openLogoutDialog();
                break;
            default:
                openEditProfileFragment();
                break;
        }
    }

    public void openChangePasswordFragment(){
        switchFragment(ChangePasswordFragment.newInstance());
    }

    public void openLogoutDialog(){
        switchFragment(LogoutDialog.newInstance());
    }

    public void openEditProfileFragment(){
        switchFragment(EditProfileFragment.newInstance());
    }

    public void openAboutFragment(){
        switchFragment(AboutFragment.newInstance());
    }
    public void openHistoryFragment(){
        switchFragment(BookingHistoryFragment.newInstance());
    }
    public void openHelpFragment(){
        switchFragment(HelpFragment.newInstance());
    }
}
