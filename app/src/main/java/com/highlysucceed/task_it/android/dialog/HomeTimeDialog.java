package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import icepick.State;

public class HomeTimeDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = HomeTimeDialog.class.getName().toString();
	private static final String DEFAULT_FORMAT  = "MMM dd, yyyy";

	public Callback  callback;
	private Calendar calendar;

	/*@BindView(R.id.calendarDP)		DatePicker calendarDP;*/
	@BindView(R.id.continueBTN)     View continueBTN;


	@State String selectedDate;
	@State String format;
	@State long maxDate;
	@State long minDate;
	@State boolean maxDateIsEdited = false;
	@State boolean minDateIsEdited = false;

	public static HomeTimeDialog newInstance(Callback callback, String selectedDate) {
		HomeTimeDialog homeDateDialog = new HomeTimeDialog();
		homeDateDialog.callback = callback;
		homeDateDialog.selectedDate = selectedDate;
		homeDateDialog.format = DEFAULT_FORMAT;
		return homeDateDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_home_time;
	}

	@Override
	public void onViewReady() {
        continueBTN.setOnClickListener(this);
		SimpleDateFormat dayFormat = new SimpleDateFormat(format);
		calendar = Calendar.getInstance();
		try {
			calendar.setTime(dayFormat.parse(selectedDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);

		/*calendarDP.init(year, month, day, new DatePicker.OnDateChangedListener() {
			@Override
			public void onDateChanged(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
				calendar.set(year, monthOfYear, dayOfMonth);
			}
		});
		if(maxDateIsEdited){
			calendarDP.setMaxDate(maxDate);
		}

		if(minDateIsEdited){
			calendarDP.setMinDate(minDate);
		}*/
	}
	public void setMaxDate(long maxDate){
		this.maxDate = maxDate;
		this.maxDateIsEdited = true;
	}

	public void setMinDate(long minDate) {
		this.minDate = minDate;
		this.minDateIsEdited = true;
	}


	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
                if (callback != null){
                    callback.onDateSelected(new SimpleDateFormat(format).format(calendar.getTime()));
                }
                dismiss();
				break;
		}
	}
	public interface Callback{
		void onDateSelected(String date);
	}
}
