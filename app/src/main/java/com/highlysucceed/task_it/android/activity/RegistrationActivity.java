package com.highlysucceed.task_it.android.activity;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.fragment.registration.ForgotPasswordFragment;
import com.highlysucceed.task_it.android.fragment.registration.LoginFragment;
import com.highlysucceed.task_it.android.fragment.registration.ResetPasswordFragment;
import com.highlysucceed.task_it.android.fragment.registration.SignUpFragment;
import com.highlysucceed.task_it.android.fragment.registration.SignUpFragmentB;
import com.highlysucceed.task_it.android.fragment.registration.SignUpFragmentC;
import com.highlysucceed.task_it.android.fragment.registration.SignUpFragmentD;
import com.highlysucceed.task_it.android.fragment.registration.SplashFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;


/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class RegistrationActivity extends RouteActivity {
    public static final String TAG = RegistrationActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_registration;
    }

    @Override
    public void onViewReady(){

    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        openSplashFragment();
    }

    public void openSplashFragment(){
        switchFragment(SplashFragment.newInstance());
    }

    public void openLoginFragment(){
        switchFragment(LoginFragment.newInstance());
    }

    public void openSignUpFragment(){
        switchFragment(SignUpFragment.newInstance());
    }

    public void openSignUpFragmentB(){
        switchFragment(SignUpFragmentB.newInstance());
    }

    public void openSignUpFragmentC(){
        switchFragment(SignUpFragmentC.newInstance());
    }

    public void openSignUpFragmentD(){
        switchFragment(SignUpFragmentD.newInstance());
    }

    public void openForgotPasswordFragment(){
        switchFragment(ForgotPasswordFragment.newInstance());
    }

    public void openResetPasswordFragment(String email){
        switchFragment(ResetPasswordFragment.newInstance(email));
    }

}
