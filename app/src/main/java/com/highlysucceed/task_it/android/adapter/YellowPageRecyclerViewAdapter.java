package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.DirectoryModel;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class YellowPageRecyclerViewAdapter extends BaseRecylerViewAdapter<YellowPageRecyclerViewAdapter.ViewHolder, DirectoryModel>{

    private ClickListener clickListener;

    public YellowPageRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_yellow_page));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().name);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.addressTXT)          TextView addressTXT;
        @BindView(R.id.contactTXT)          TextView contactTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public DirectoryModel getItem() {
            return (DirectoryModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(DirectoryModel directoryModel);
    }
} 
