package com.highlysucceed.task_it.android.fragment.booking;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.BookingServiceActivity;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class ServiceProviderDetailsFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = ServiceProviderDetailsFragment.class.getName().toString();

    private BookingServiceActivity bookingServiceActivity;

    @BindView(R.id.messageBTN)                  RelativeLayout messageBTN;
    @BindView(R.id.bookNowBTN)                  TextView bookNowBTN;
    @BindView(R.id.ratingTXT)                   TextView ratingTXT;
    @BindView(R.id.star1)                       ImageView star1;
    @BindView(R.id.star2)				        ImageView star2;
    @BindView(R.id.star3)				        ImageView star3;
    @BindView(R.id.star4)				        ImageView star4;
    @BindView(R.id.star5)				        ImageView star5;

    public static ServiceProviderDetailsFragment newInstance() {
        ServiceProviderDetailsFragment fragment = new ServiceProviderDetailsFragment();
        return fragment;
    }

    public void starsOnClick(){
        star1.setOnClickListener(this);
        star2.setOnClickListener(this);
        star3.setOnClickListener(this);
        star4.setOnClickListener(this);
        star5.setOnClickListener(this);
    }

    @Override
    public void onViewReady() {
        starsOnClick();
        bookingServiceActivity = (BookingServiceActivity) getContext();
        bookingServiceActivity.setTitle("Service Provider Details");
        messageBTN.setOnClickListener(this);
        bookNowBTN.setOnClickListener(this);

        ratingSet(4);
        setUpRating(4);

    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_service_provider_details;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
            switch (view.getId()){
                case R.id.messageBTN:
                    bookingServiceActivity.startConversationActivity();
                    break;
                case R.id.bookNowBTN:
                    bookingServiceActivity.startMainActivity("status");
                    break;
                case R.id.star1:
                    ratingSet(1);
                    setUpRating(1);
                    break;
                case R.id.star2:
                    ratingSet(2);
                    setUpRating(2);
                    break;
                case R.id.star3:
                    ratingSet(3);
                    setUpRating(3);
                    break;
                case R.id.star4:
                    ratingSet(4);
                    setUpRating(4);
                    break;
                case R.id.star5:
                    ratingSet(5);
                    setUpRating(5);
                    break;

            }
    }

    private void setUpRating(int count){
        switch (count){
            case 1:
                star1.setImageResource(R.drawable.icon_star_full);
                star2.setImageResource(R.drawable.icon_star_empty);
                star3.setImageResource(R.drawable.icon_star_empty);
                star4.setImageResource(R.drawable.icon_star_empty);
                star5.setImageResource(R.drawable.icon_star_empty);
                break;
            case 2:
                star1.setImageResource(R.drawable.icon_star_full);
                star2.setImageResource(R.drawable.icon_star_full);
                star3.setImageResource(R.drawable.icon_star_empty);
                star4.setImageResource(R.drawable.icon_star_empty);
                star5.setImageResource(R.drawable.icon_star_empty);
                break;
            case 3:
                star1.setImageResource(R.drawable.icon_star_full);
                star2.setImageResource(R.drawable.icon_star_full);
                star3.setImageResource(R.drawable.icon_star_full);
                star4.setImageResource(R.drawable.icon_star_empty);
                star5.setImageResource(R.drawable.icon_star_empty);
                break;
            case 4:
                star1.setImageResource(R.drawable.icon_star_full);
                star2.setImageResource(R.drawable.icon_star_full);
                star3.setImageResource(R.drawable.icon_star_full);
                star4.setImageResource(R.drawable.icon_star_full);
                star5.setImageResource(R.drawable.icon_star_empty);
                break;
            case 5:
                star1.setImageResource(R.drawable.icon_star_full);
                star2.setImageResource(R.drawable.icon_star_full);
                star3.setImageResource(R.drawable.icon_star_full);
                star4.setImageResource(R.drawable.icon_star_full);
                star5.setImageResource(R.drawable.icon_star_full);
                break;
            default:
                star1.setImageResource(R.drawable.icon_star_empty);
                star2.setImageResource(R.drawable.icon_star_empty);
                star3.setImageResource(R.drawable.icon_star_empty);
                star4.setImageResource(R.drawable.icon_star_empty);
                star5.setImageResource(R.drawable.icon_star_empty);
        }
    }

    private void ratingSet(Integer i){
        String a = Integer.toString(i);
        a += ".0";
        ratingTXT.setText(a);
    }


}
