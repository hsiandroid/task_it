package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.ProviderModel;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProviderRecyclerViewAdapter extends BaseRecylerViewAdapter<ProviderRecyclerViewAdapter.ViewHolder, ProviderModel>{

    private ClickListener clickListener;

    public ProviderRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_provider_search));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.companyTXT.setText(holder.getItem().companyName);
        holder.priceRangeTXT.setText(holder.getItem().priceRange);
        holder.awayTXT.setText(holder.getItem().away);

        Picasso.with(getContext())
                .load(holder.getItem().full_path)
                .error(R.drawable.placeholder_avatar)
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.imageCIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.companyTXT)      TextView companyTXT;
        @BindView(R.id.priceRangeTXT)   TextView priceRangeTXT;
        @BindView(R.id.awayTXT)         TextView awayTXT;
        @BindView(R.id.imageCIV)        CircleImageView imageCIV;

        public ViewHolder(View view) {
            super(view);
        }

        public ProviderModel getItem() {
            return (ProviderModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ProviderModel) v.getTag()));
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(ProviderModel providerModel);
    }
} 
