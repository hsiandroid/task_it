package com.highlysucceed.task_it.android.fragment.booking;

import android.util.Log;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class BookServiceFragment extends BaseFragment {
    public static final String TAG = BookServiceFragment.class.getName().toString();


    public static BookServiceFragment newInstance() {
        BookServiceFragment fragment = new BookServiceFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {

    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_default;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }
}
