package com.highlysucceed.task_it.android.route;

import android.content.Intent;
import android.os.Bundle;

import com.highlysucceed.task_it.android.activity.BookingProfileActivity;
import com.highlysucceed.task_it.android.activity.BookingServiceActivity;
import com.highlysucceed.task_it.android.activity.ConversationActivity;
import com.highlysucceed.task_it.android.activity.LocationActivity;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.activity.NotificationActivity;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.android.activity.SearchActivity;
import com.highlysucceed.task_it.android.activity.SettingsActivity;
import com.highlysucceed.task_it.data.model.server.BookingProfileModel;
import com.highlysucceed.task_it.data.model.server.LocationModel;
import com.highlysucceed.task_it.vendor.android.base.BaseActivity;
import com.highlysucceed.task_it.vendor.android.base.RouteManager;

/**
 * Created by BCTI 3 on 2/6/2017.
 */

public class RouteActivity extends BaseActivity {

    public void startMainActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(MainActivity.class)
                .addActivityTag("main")
                .addFragmentTag(fragmentTAG)
                .startActivity(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        finish();
    }

    public void startRegistrationActivity() {
        RouteManager.Route.with(this)
                .addActivityClass(RegistrationActivity.class)
                .addActivityTag("registration")
                .addFragmentTag("splash")
                .startActivity();

        finish();
    }

    public void startSettingsActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(SettingsActivity.class)
                .addActivityTag("settings")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }


    public void startConversationActivity() {
        RouteManager.Route.with(this)
                .addActivityClass(ConversationActivity.class)
                .addActivityTag("conversation")
                .startActivity();
    }

    public void startLocationActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(LocationActivity.class)
                .addActivityTag("settings")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startLocationActivity(LocationModel locationModel, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("location_item", locationModel);
        RouteManager.Route.with(this)
                .addActivityClass(LocationActivity.class)
                .addActivityTag("settings")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startBookingProfileActivity(BookingProfileModel bookingProfileModel, String fragmentTAG) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("booking_profile_item", bookingProfileModel);
        RouteManager.Route.with(this)
                .addActivityClass(BookingProfileActivity.class)
                .addActivityTag("booking_profile")
                .addFragmentTag(fragmentTAG)
                .addFragmentBundle(bundle)
                .startActivity();
    }

    public void startNotificationActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(NotificationActivity.class)
                .addActivityTag("notification")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startBookingServiceActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(BookingServiceActivity.class)
                .addActivityTag("book")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }

    public void startSearchActivity(String fragmentTAG) {
        RouteManager.Route.with(this)
                .addActivityClass(SearchActivity.class)
                .addActivityTag("search")
                .addFragmentTag(fragmentTAG)
                .startActivity();
    }
}
