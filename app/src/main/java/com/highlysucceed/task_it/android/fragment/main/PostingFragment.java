package com.highlysucceed.task_it.android.fragment.main;

import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.PostingSpinnerAdapter;
import com.highlysucceed.task_it.android.dialog.SummaryDialog;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class PostingFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = PostingFragment.class.getName().toString();

    private MainActivity mainActivity;
    private PostingSpinnerAdapter monthSpinnerAdapter;
    private PostingSpinnerAdapter daySpinnerAdapter;
    private PostingSpinnerAdapter yearSpinnerAdapter;
    private PostingSpinnerAdapter serviceTypeSpinnerAdapter;

    private List<String> monthItems;
    private List<String> yearItems;

    @BindView(R.id.serviceTypeSPR)              Spinner serviceTypeSPR;
    @BindView(R.id.monthSPR)                    Spinner monthSPR;
    @BindView(R.id.daySPR)                      Spinner daySPR;
    @BindView(R.id.yearSPR)                     Spinner yearSPR;
    @BindView(R.id.serviceDurationET)           EditText serviceDurationET;
    @BindView(R.id.amountRangeET)               EditText amountRangeET;
    @BindView(R.id.submitBTN)                   TextView submitBTN;

    public static PostingFragment newInstance() {
        PostingFragment fragment = new PostingFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_posting;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_posting));

        submitBTN.setOnClickListener(this);

        setUpMonthSPR();
        setUpDaySPR();
        setUpYearSPR();

        setUpServiceTypeSPR();
    }

    private void setUpMonthSPR() {

        monthItems = new ArrayList<>();

        monthItems.clear();

        monthItems.add("January");
        monthItems.add("February");
        monthItems.add("March");
        monthItems.add("April");
        monthItems.add("May");
        monthItems.add("June");
        monthItems.add("July");
        monthItems.add("August");
        monthItems.add("September");
        monthItems.add("October");
        monthItems.add("November");
        monthItems.add("December");
        monthSpinnerAdapter = new PostingSpinnerAdapter(getContext());
        monthSpinnerAdapter.setNewData(monthItems);
        monthSPR.setAdapter(monthSpinnerAdapter);
        monthSPR.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                daySpinnerAdapter.setNewData(getDayItems());
                switch (monthItems.get(position)){
                    case "February":
                        daySpinnerAdapter.removeItemPos(28);
                        daySpinnerAdapter.removeItemPos(28);
                        daySpinnerAdapter.removeItemPos(28);
                        break;
                    case "April":
                    case "June":
                    case "September":
                    case "November":
                        daySpinnerAdapter.removeItemPos(30);
                        break;
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    private void setUpServiceTypeSPR(){
        serviceTypeSpinnerAdapter = new PostingSpinnerAdapter(getContext());
        serviceTypeSPR.setAdapter(serviceTypeSpinnerAdapter);
        serviceTypeSpinnerAdapter.setNewData(getServiceTypeData());

    }

    private List<String> getServiceTypeData(){
        List<String> serviceTypeItems = new ArrayList<>();
        serviceTypeItems.add("Interior Design");
        serviceTypeItems.add("Exterior Design");

        return serviceTypeItems;
    }

    private List<String> getDayItems(){
        List<String> dayItems = new ArrayList<>();

        dayItems.add("1");
        dayItems.add("2");
        dayItems.add("3");
        dayItems.add("4");
        dayItems.add("5");
        dayItems.add("6");
        dayItems.add("7");
        dayItems.add("8");
        dayItems.add("9");
        dayItems.add("10");
        dayItems.add("11");
        dayItems.add("12");
        dayItems.add("13");
        dayItems.add("14");
        dayItems.add("15");
        dayItems.add("16");
        dayItems.add("17");
        dayItems.add("18");
        dayItems.add("19");
        dayItems.add("20");
        dayItems.add("21");
        dayItems.add("22");
        dayItems.add("23");
        dayItems.add("24");
        dayItems.add("25");
        dayItems.add("26");
        dayItems.add("27");
        dayItems.add("28");
        dayItems.add("29");
        dayItems.add("30");
        dayItems.add("31");

      return  dayItems;
    }

    private void setUpDaySPR(){
        daySpinnerAdapter = new PostingSpinnerAdapter(getContext());
        daySpinnerAdapter.setNewData(getDayItems());
        daySPR.setAdapter(daySpinnerAdapter);

    }

    private void setUpYearSPR(){
        yearItems =  new ArrayList<>();

        yearItems.add("2017");
        yearItems.add("2018");
        yearItems.add("2019");
        yearItems.add("2020");
        yearItems.add("2021");
        yearItems.add("2022");
        yearItems.add("2023");
        yearItems.add("2024");
        yearItems.add("2025");
        yearItems.add("2026");
        yearItems.add("2027");
        yearItems.add("2028");
        yearItems.add("2029");
        yearItems.add("2030");

        yearSpinnerAdapter = new PostingSpinnerAdapter(getContext());
        yearSpinnerAdapter.setNewData(yearItems);
        yearSPR.setAdapter(yearSpinnerAdapter);
        yearSPR.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                daySpinnerAdapter.setNewData(getDayItems());
                switch (yearItems.get(position)){
                    case "2020":
                    case "2024":
                    case "2028":
                        if (monthSPR.getSelectedItem().equals("February")){
                            daySpinnerAdapter.removeItemPos(29);
                            daySpinnerAdapter.removeItemPos(29);
                        }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*mainActivity.setSelectedItem(R.id.nav_posting);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submitBTN:
                SummaryDialog.newInstance().show(getChildFragmentManager(),SummaryDialog.TAG);
                break;
        }
    }
}
