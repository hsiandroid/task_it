package com.highlysucceed.task_it.android.fragment.registration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;
import icepick.State;

public class ResetPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ResetPasswordFragment.class.getName().toString();

    private RegistrationActivity registrationActivity;

    @BindView(R.id.confirmNewPasswordTV)        TextView confirmNewPasswordTV;
//    @BindView(R.id.resendVerificationCodeTV)    TextView resendVerificationCodeTV;
    @BindView(R.id.emailET)                     EditText emailET;
//    @BindView(R.id.verificationCodeET)          EditText verificationCodeET;
    @BindView(R.id.passwordET)                  EditText passwordET;
    @BindView(R.id.confirmPasswordET)           EditText confirmPasswordET;

    @State String email;

    public static ResetPasswordFragment newInstance(String email) {
        ResetPasswordFragment resetPasswordFragment = new ResetPasswordFragment();
        resetPasswordFragment.email = email;
        return resetPasswordFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_reset_password;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();

        emailET.setText(email);
        confirmNewPasswordTV.setOnClickListener(this);
//        resendVerificationCodeTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.confirmNewPasswordTV:
                attemptRequestVerification();
                break;
//            case R.id.resendVerificationCodeTV:
//                attemptResendRequestVerification();
//                registrationActivity.openLoginFragment();
//                break;
        }
    }

    private void attemptRequestVerification(){
//        ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest(getContext());
//        resetPasswordRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resetting password...", false, false))
//                .addParameters(Variable.server.key.EMAIL, emailET.getText().toString())
//                .addParameters(Variable.server.key.VALIDATION_TOKEN, verificationCodeET.getText().toString())
//                .addParameters(Variable.server.key.PASSWORD, passwordET.getText().toString())
//                .addParameters(Variable.server.key.PASSWORD_CONFIRMATION, confirmPasswordET.getText().toString())
//                .execute();
    }

    private void attemptResendRequestVerification(){
//        VerificationCodeRequest verificationCodeRequest = new VerificationCodeRequest(getContext());
//        verificationCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Resending verification code...", false, false))
//                .addParameters(Variable.server.key.EMAIL, emailET.getText().toString())
//                .execute();
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onStop() {
//        EventBus.getDefault().unregister(this);
//        super.onStop();
//    }
//
//    @Subscribe
//    public void onResponse(VerificationCodeRequest.ServerResponse responseData) {
//        if(responseData.getData().status){
//
//        }
//    }
//
//    @Subscribe
//    public void onResponse(ResetPasswordRequest.ServerResponse responseData) {
//        if(responseData.getData().status){
//            registrationActivity.openLoginFragment();
//        }
//    }
}
