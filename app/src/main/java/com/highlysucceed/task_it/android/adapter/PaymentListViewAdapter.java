package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.PaymentModel;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;

import butterknife.BindView;

public class PaymentListViewAdapter extends BaseListViewAdapter<PaymentListViewAdapter.ViewHolder, PaymentModel> {

    private ClickListener clickListener;

    public PaymentListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_payment));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.emailTXT.setText(holder.getItem().name);
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.emailTXT)     TextView emailTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public PaymentModel getItem() {
            return (PaymentModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((SettingsListViewAdapter.ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(AndroidModel androidModel);
    }
} 
