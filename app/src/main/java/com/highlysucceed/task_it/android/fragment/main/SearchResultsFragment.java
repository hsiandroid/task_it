package com.highlysucceed.task_it.android.fragment.main;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class SearchResultsFragment extends BaseFragment {
    public static final String TAG = ProfileFragment.class.getName().toString();

//    private ResultsViewAdapter resultsViewAdapter;
    @BindView(R.id.recyclerView)               RecyclerView recyclerView;


    public static SearchResultsFragment newInstance() {
        SearchResultsFragment searchResultsFragmentfragment = new SearchResultsFragment();
        return searchResultsFragmentfragment;
    }

    @Override
    public void onViewReady(){

    }

    @Override
    public int onLayoutSet(){
        return R.layout.fragment_search_results;
    }


//    private void setUpRV(){
//        recyclerView = new RecyclerView(getContext());
////        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
////        recyclerView.setLayoutManager(linearLayoutManager);
////        resultsViewAdapter = new ResultsViewAdapter(getContext());
//        recyclerView.setAdapter(resultsViewAdapter);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
//        recyclerView.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView
//    }

}
