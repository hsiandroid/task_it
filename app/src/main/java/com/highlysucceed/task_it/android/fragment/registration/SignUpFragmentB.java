package com.highlysucceed.task_it.android.fragment.registration;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class SignUpFragmentB extends BaseFragment implements View.OnClickListener {

    public static final String TAG = SignUpFragmentB.class.getName().toString();
    public RegistrationActivity registrationActivity;

    @BindView(R.id.signUpEMAIL)         TextView signUpEMAIL;

    public static SignUpFragmentB newInstance(){
        SignUpFragmentB signUpFragmentb = new SignUpFragmentB();
        return signUpFragmentb;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_sign_up_b;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();
        signUpEMAIL.setOnClickListener(this);
//        backToSignUpBTN.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.signUpEMAIL:
                registrationActivity.openSignUpFragment();
                break;
//            case R.id.backToSignUpBTN:
//                registrationActivity.openLoginFragment();
        }
    }
}
