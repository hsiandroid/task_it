package com.highlysucceed.task_it.android.dialog;

import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.adapter.FragmentViewPagerAdapter;
import com.highlysucceed.task_it.android.fragment.settings.CalendarViewFragment;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class MonthYearDialog extends BaseDialog implements View.OnClickListener,
        CalendarViewFragment.MonthCallback,
        CalendarViewFragment.YearCallback{
    public static final String TAG = MonthYearDialog.class.getName().toString();

    private FragmentViewPagerAdapter fragmentViewPagerAdapter;
    private DialogCallback dialogCallback;

    @BindView(R.id.monthBTN)            TextView monthBTN;
    @BindView(R.id.yearBTN)             TextView yearBTN;
    @BindView(R.id.monthVP)             ViewPager monthVP;
    @BindView(R.id.saveBTN)             View saveBTN;
    @BindView(R.id.cancelBTN)           View cancelBTN;

    @State int month;
    @State int year;

    private CalendarViewFragment monthFragment;
    private CalendarViewFragment yearFragment;

    public static MonthYearDialog newInstance(int month, int year, DialogCallback dialogCallback) {
        MonthYearDialog monthYearDialog = new MonthYearDialog();
        monthYearDialog.month = month;
        monthYearDialog.year = year;
        monthYearDialog.dialogCallback = dialogCallback;
        return monthYearDialog;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.dialog_month_year;
    }

    @Override
    public void onViewReady() {
        monthBTN.setOnClickListener(this);
        yearBTN.setOnClickListener(this);
        saveBTN.setOnClickListener(this);
        cancelBTN.setOnClickListener(this);
        setupViewPager();
    }

    @Override
    public void onStart() {
        super.onStart();
        setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.monthBTN:
                monthActive();
                break;
            case R.id.yearBTN:
                yearActive();
                break;
            case R.id.saveBTN:
                if(dialogCallback != null){
                    dialogCallback.dialogCallback(month, year);
                }
                dismiss();
                break;
            case R.id.cancelBTN:
                dismiss();
                break;
        }
    }

    public void monthActive(){
        monthVP.post(new Runnable() {
            @Override
            public void run() {
                monthVP.setCurrentItem(0);
            }
        });
    }

    public void yearActive(){
        monthVP.post(new Runnable() {
            @Override
            public void run() {
                monthVP.setCurrentItem(1);
            }
        });
    }

    private void setupViewPager(){
        monthFragment = CalendarViewFragment.newInstance(month, CalendarViewFragment.Type.MONTH);
        monthFragment.setMonthCallback(this);
        monthBTN.setText(monthFragment.getMonth().get(month));

        yearFragment = CalendarViewFragment.newInstance(year, CalendarViewFragment.Type.YEAR);
        yearFragment.setYearCallback(this);
        yearBTN.setText(String.valueOf(year));

        fragmentViewPagerAdapter = new FragmentViewPagerAdapter(getChildFragmentManager());
        fragmentViewPagerAdapter.addFragment(monthFragment);
        fragmentViewPagerAdapter.addFragment(yearFragment);
        monthVP.setPageMargin(20);
        monthVP.setPageMarginDrawable(null);
        monthVP.setAdapter(fragmentViewPagerAdapter);
        monthVP.setOffscreenPageLimit(2);

    }

    @Override
    public void month(int id, String month) {
        monthBTN.setText(month);
        this.month = id;
    }

    @Override
    public void year(int year) {
        yearBTN.setText(String.valueOf(year));
        this.year = year;
    }

    public interface DialogCallback{
        void dialogCallback(int month, int year);
    }
}
