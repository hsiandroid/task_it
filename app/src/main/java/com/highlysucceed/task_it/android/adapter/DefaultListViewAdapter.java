package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.SampleModel;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;

import butterknife.BindView;

public class DefaultListViewAdapter extends BaseListViewAdapter<DefaultListViewAdapter.ViewHolder, SampleModel> {

    private ClickListener clickListener;

    public DefaultListViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_default));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.nameTXT.setText("Position " + holder.getItem().id);
    }

    public class ViewHolder extends BaseListViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)     TextView nameTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public SampleModel getItem() {
            return (SampleModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((SettingsListViewAdapter.ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(AndroidModel androidModel);
    }
} 
