package com.highlysucceed.task_it.android.fragment.settings;


import android.view.View;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.SettingsActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

public class AboutFragment extends BaseFragment implements View.OnClickListener {

    public static final String TAG = AboutFragment.class.getName().toString();

    private SettingsActivity settingsActivity;

    public static AboutFragment newInstance() {
        AboutFragment aboutFragment = new AboutFragment();
        return aboutFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_about;
    }

    @Override
    public void onViewReady() {
        settingsActivity = (SettingsActivity) getActivity();
        settingsActivity.setTitle(getString(R.string.about_header));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
