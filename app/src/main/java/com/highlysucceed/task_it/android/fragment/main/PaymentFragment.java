package com.highlysucceed.task_it.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.PaymentListViewAdapter;
import com.highlysucceed.task_it.data.model.server.PaymentModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.android.widget.ExpandableHeightListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class PaymentFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = PaymentFragment.class.getName().toString();

    private PaymentListViewAdapter paymentListViewAdapter;

   /* @BindView(R.id.paymentLV)                   ExpandableHeightListView paymentLV;
    @BindView(R.id.addCreditAccountBTN)         View addCreditAccountBTN;
    @BindView(R.id.addPaypalAccountBTN)         View addPaypalAccountBTN;
    @BindView(R.id.paymentSRL)                  SwipeRefreshLayout paymentSRL;
*/
    private MainActivity mainActivity;

    public static PaymentFragment newInstance() {
        PaymentFragment paymentFragment = new PaymentFragment();
        return paymentFragment;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_payment));
        mainActivity.setupSettingButton(0);
       /* addCreditAccountBTN.setOnClickListener(this);
        addPaypalAccountBTN.setOnClickListener(this);*/

      /*  setUpPaymentListView();*/
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_payments;
    }

   /* private void setUpPaymentListView(){
        paymentListViewAdapter = new PaymentListViewAdapter(getContext());
        paymentLV.setAdapter(paymentListViewAdapter);
        paymentListViewAdapter.setNewData(getData());
    }*/

    private List<PaymentModel> getData(){
        List<PaymentModel> paymentModels = new ArrayList<>();

        PaymentModel paymentModel = new PaymentModel();
        paymentModel.id =1;
        paymentModel.name = "johndoe@gmail.com";
        paymentModels.add(paymentModel);

        paymentModel = new PaymentModel();
        paymentModel.id =2;
        paymentModel.name = "janedoe@gmail.com";
        paymentModels.add(paymentModel);

        return  paymentModels;
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setTitle(R.id.nav_payment);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addCreditAccountBTN:
                break;
            case R.id.addPaypalAccountBTN:
                break;
        }
    }
}