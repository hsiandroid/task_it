package com.highlysucceed.task_it.android.activity;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.fragment.location.LocationCreateFragment;
import com.highlysucceed.task_it.android.fragment.location.LocationEditFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;
import com.highlysucceed.task_it.data.model.server.LocationModel;


/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class LocationActivity extends RouteActivity {
    public static final String TAG = LocationActivity.class.getName().toString();
    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {
        registerBackPress(true);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "create":
                openCreateLocationFragment();
                break;
            case "edit":
                openEditLocationFragment((LocationModel) getFragmentBundle().getParcelable("location_item"));
                break;
        }
    }

    public void openCreateLocationFragment(){
        switchFragment(LocationCreateFragment.newInstance());
    }

    public void openEditLocationFragment(LocationModel locationModel){
        switchFragment(LocationEditFragment.newInstance(locationModel));
    }
}
