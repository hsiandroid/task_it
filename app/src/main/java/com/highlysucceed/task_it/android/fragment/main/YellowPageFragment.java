package com.highlysucceed.task_it.android.fragment.main;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.YellowPageRecyclerViewAdapter;
import com.highlysucceed.task_it.android.dialog.FilterDialog;
import com.highlysucceed.task_it.data.model.server.DirectoryModel;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class YellowPageFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = YellowPageFragment.class.getName().toString();

    private MainActivity mainActivity;
    private YellowPageRecyclerViewAdapter yellowPageRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    /*@BindView(R.id.defaultRV)       RecyclerView defaultRV;*/
    @BindView(R.id.filterBTN)         ImageView filterBTN;

    public static YellowPageFragment newInstance() {
        YellowPageFragment fragment = new YellowPageFragment();
        return fragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_yellow_pages;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_yellow_page));
        mainActivity.setupSettingButton(0);
        filterBTN.setOnClickListener(this);

      /*  setUpLV();*/
    }

   /* private void setUpLV(){
        yellowPageRecyclerViewAdapter = new YellowPageRecyclerViewAdapter(getContext());
        yellowPageRecyclerViewAdapter.setNewData(getDefaultData());
        linearLayoutManager = new LinearLayoutManager(getContext());
        defaultRV.setLayoutManager(linearLayoutManager);
        defaultRV.setAdapter(yellowPageRecyclerViewAdapter);
    }*/

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setSelectedItem(R.id.nav_yellow_pages);
    }

  /*  private List<DirectoryModel> getDefaultData(){
        List<DirectoryModel> androidModels = new ArrayList<>();
        DirectoryModel defaultItem;
        for(int i = 0; i < 20; i++){
            defaultItem = new DirectoryModel();
            defaultItem.id = i;
            defaultItem.name = "Directory " + i;
            androidModels.add(defaultItem);
        }
        return androidModels;
    }*/

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.filterBTN:
                FilterDialog.newInstance().show(getChildFragmentManager(), FilterDialog.TAG);
                break;
        }
    }
}
