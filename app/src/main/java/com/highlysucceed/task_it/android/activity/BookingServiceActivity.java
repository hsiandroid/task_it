package com.highlysucceed.task_it.android.activity;

import com.highlysucceed.task_it.R;

import com.highlysucceed.task_it.android.fragment.booking.BookingSummaryFragment;
import com.highlysucceed.task_it.android.fragment.booking.ServiceProviderDetailsFragment;
import com.highlysucceed.task_it.android.fragment.booking.provider.ProviderFragment;
import com.highlysucceed.task_it.android.fragment.booking.provider.ProviderSelectFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class BookingServiceActivity extends RouteActivity {
    public static final String TAG = BookingServiceActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }


    @Override
    public void onViewReady() {
        registerBackPress(true);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName) {
            case "book":
                openBookServiceFragment();
                break;
            case "personnel":
                openProviderPersonnelFragment();
                break;
            case "booking":
                openBookingSummaryFragment();
                break;
        }
    }

    public void openBookServiceFragment() {
        switchFragment(ProviderFragment.newInstance());
    }

    public void openProviderPersonnelFragment(){
        switchFragment(ProviderSelectFragment.newInstance());
    }
    public void openBookingSummaryFragment(){
        switchFragment(BookingSummaryFragment.newInstance());
    }

    public void openServiceDetailFragment(){
        switchFragment(ServiceProviderDetailsFragment.newInstance());
    }


}
