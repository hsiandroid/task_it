package com.highlysucceed.task_it.android.fragment.main;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.CalendarScheduleAdapter;
import com.highlysucceed.task_it.android.dialog.MonthYearDialog;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.android.widget.ExpandableHeightGridView;

import butterknife.BindView;

public class ScheduleFragment extends BaseFragment implements
        View.OnClickListener,
        MonthYearDialog.DialogCallback {

    public static final String TAG = ScheduleFragment.class.getName().toString();

    private MainActivity mainActivity;

    private CalendarScheduleAdapter calendarScheduleAdapter;

    @BindView(R.id.calendarGV) 		ExpandableHeightGridView calendarGV;
    @BindView(R.id.prevBTN)			View prevBTN;
    @BindView(R.id.nextBTN)			View nextBTN;
    @BindView(R.id.monthBTN)		TextView monthBTN;

    @BindView(R.id.messageBTN)		View messageBTN;


    private String month;
    private String day;
    private String year;
    private int monthNum = 0;

    public static ScheduleFragment newInstance() {
        ScheduleFragment scheduleFragment = new ScheduleFragment();
        return scheduleFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_schedule;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getContext();
        mainActivity.setupSettingButton(0);
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_schedule));

        prevBTN.setOnClickListener(this);
        nextBTN.setOnClickListener(this);
        monthBTN.setOnClickListener(this);

        messageBTN.setOnClickListener(this);

        setUpCalendar();
    }

    private void setUpCalendar(){
        calendarScheduleAdapter = new CalendarScheduleAdapter(getContext());
        if ((monthNum >=1) || (day !=null) || (month !=null) || (year != null)){
            calendarScheduleAdapter.setCurrentMonthNum(monthNum - 1);
            calendarScheduleAdapter.setCurrentYear(Integer.parseInt(year));
            calendarScheduleAdapter.setCurrentMonthName(month);
            calendarScheduleAdapter.setActualDay(Integer.parseInt(day));
            calendarScheduleAdapter.refresh();
        }
        calendarGV.setAdapter(calendarScheduleAdapter);
        calendarGV.setExpanded(true);
        displayMonth();
    }

    public void prevClick(){
        calendarScheduleAdapter.prevMonth();
        displayMonth();
    }

    public void nextClick(){
        calendarScheduleAdapter.nextMonth();
        displayMonth();
    }

    private void displayMonth(){
        monthBTN.setText(calendarScheduleAdapter.getCurrentMonthName() + " " + calendarScheduleAdapter.getCurrentYear());
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        /*mainActivity.setSelectedItem(R.id.nav_schedule);*/
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.prevBTN:
                prevClick();
                break;
            case R.id.nextBTN:
                nextClick();
                break;
            case R.id.monthBTN:
                MonthYearDialog.newInstance(calendarScheduleAdapter.getCurrentMonthNum(), calendarScheduleAdapter.getCurrentYear(), this).show(getChildFragmentManager(), MonthYearDialog.TAG);
                break;
            case R.id.messageBTN:
                mainActivity.startConversationActivity();
                break;
        }
    }

    @Override
    public void dialogCallback(int month, int year) {
        calendarScheduleAdapter.setCurrentMonthNum(month);
        calendarScheduleAdapter.setCurrentYear(year);
        calendarScheduleAdapter.refresh();
        displayMonth();
    }
}
