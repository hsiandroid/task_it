package com.highlysucceed.task_it.android.fragment.booking.provider;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.BookingServiceActivity;
import com.highlysucceed.task_it.android.adapter.ProviderPersonnelRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.ProviderPersonnelModel;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ProviderSelectFragment extends BaseFragment implements ProviderPersonnelRecyclerViewAdapter.ClickListener{
    public static final String TAG = ProviderSelectFragment.class.getName().toString();
    private BookingServiceActivity bookingServiceActivity;
    private LinearLayoutManager linearLayoutManager;
    private ProviderPersonnelRecyclerViewAdapter providerPersonnelRecyclerViewAdapter;

    @BindView(R.id.companyPersonnelRV)              RecyclerView companyPersonnelRV;

    public static ProviderSelectFragment newInstance() {
        ProviderSelectFragment fragment = new ProviderSelectFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        bookingServiceActivity = (BookingServiceActivity)getActivity();
        bookingServiceActivity = (BookingServiceActivity)getContext();
        bookingServiceActivity.setTitle("Select Service Provider");
        setUpProviderListView();
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_provider_select;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    private void setUpProviderListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        providerPersonnelRecyclerViewAdapter = new ProviderPersonnelRecyclerViewAdapter(getContext());
        companyPersonnelRV.setLayoutManager(linearLayoutManager);
        companyPersonnelRV.setAdapter(providerPersonnelRecyclerViewAdapter);
        providerPersonnelRecyclerViewAdapter.setClickListener(this);
        providerPersonnelRecyclerViewAdapter.setNewData(getData());
    }

    private List<ProviderPersonnelModel> getData(){
        List<ProviderPersonnelModel> providerPersonnelModels = new ArrayList<>();

        ProviderPersonnelModel providerPersonnelModel = new ProviderPersonnelModel();
        providerPersonnelModel.id = 1;
        providerPersonnelModel.companyPersonnel = "John Doe";
        providerPersonnelModel.away = "5km away";
        providerPersonnelModel.priceRange = "Php 2000 - 5000";
        providerPersonnelModels.add(providerPersonnelModel);

        providerPersonnelModel = new ProviderPersonnelModel();
        providerPersonnelModel.id = 2;
        providerPersonnelModel.companyPersonnel = "Jane Doe";
        providerPersonnelModel.away = "3km away";
        providerPersonnelModel.priceRange = "Php 2000 - 5000";
        providerPersonnelModels.add(providerPersonnelModel);

        return providerPersonnelModels;
    }

    @Override
    public void onItemClick(ProviderPersonnelModel providerPersonnelModel) {
        bookingServiceActivity.startConversationActivity();
    }

    @Override
    public void onBookClick(ProviderPersonnelModel providerPersonnelModel) {
        bookingServiceActivity.openBookingSummaryFragment();
    }

    @Override
    public void onItemClick(AndroidModel androidModel) {

    }

    @Override
    public void onItemLongClick(AndroidModel androidModel) {

    }
}
