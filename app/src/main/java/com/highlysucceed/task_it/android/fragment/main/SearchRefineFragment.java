package com.highlysucceed.task_it.android.fragment.main;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.android.java.Keyboard;

import butterknife.BindView;

public class SearchRefineFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = SearchRefineFragment.class.getName().toString();

    private MainActivity mainActivity;


    @BindView(R.id.searchServiceBTN)        TextView searchServiceBTN;;

    public static SearchRefineFragment newInstance(){
        SearchRefineFragment searchRefineFragment = new SearchRefineFragment();
        return searchRefineFragment;
    }

    @Override
    public int onLayoutSet(){
        return R.layout.fragment_search_refine;
    }

    @Override
    public void onViewReady(){
        mainActivity = (MainActivity) getActivity();
        mainActivity = (MainActivity) getContext();
        Keyboard.hideKeyboard(getActivity());
        searchServiceBTN.setOnClickListener(this);
    }

    @Override
    public void onClick(View view){
        switch(view.getId()){
            case R.id.searchServiceBTN:
//                mainActivity.openSearchResultsFragment();
                break;
        }
    }

}
