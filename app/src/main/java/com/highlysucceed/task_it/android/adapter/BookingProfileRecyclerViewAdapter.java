package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.BookingProfileModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class BookingProfileRecyclerViewAdapter extends BaseRecylerViewAdapter<BookingProfileRecyclerViewAdapter.ViewHolder, BookingProfileModel>{

    private ClickListener clickListener;

    public BookingProfileRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_booking_profile));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.otherOptionBTN.setTag(holder.getItem());
        holder.otherOptionBTN.setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().name);
        holder.addressTXT.setText(holder.getItem().address);

        Picasso.with(getContext())
                .load(holder.getItem().full_path)
                .error(R.drawable.placeholder_avatar)
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.imageCIV);
    }

    private void otherOption(final View v){
        final PopupMenu popup = new PopupMenu(getContext(), v);
        popup.getMenuInflater().inflate(R.menu.location_other_option, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.view:
                        if(clickListener != null){
                            clickListener.onItemView((BookingProfileModel) v.getTag());
                        }
                        break;
                    case R.id.edit: if(clickListener != null){
                        clickListener.onItemEdit((BookingProfileModel) v.getTag());
                    }
                        break;
                    case R.id.delete:
                        if(clickListener != null){
                            clickListener.onItemDelete((BookingProfileModel) v.getTag());
                        }
                        break;
                }
                return false;
            }
        });
        popup.show();
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.addressTXT)          TextView addressTXT;
        @BindView(R.id.imageCIV)            ImageView imageCIV;
        @BindView(R.id.otherOptionBTN)      ImageView otherOptionBTN;

        public ViewHolder(View view) {
            super(view);
        }

        public BookingProfileModel getItem() {
            return (BookingProfileModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.otherOptionBTN:
                otherOption(v);
                break;
        }
//        if(clickListener != null){
//            clickListener.onItemClick((BookingProfileModel) v.getTag());
//        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener{
        void onItemView(BookingProfileModel bookingProfileModel);
        void onItemEdit(BookingProfileModel bookingProfileModel);
        void onItemDelete(BookingProfileModel bookingProfileModel);
    }
} 
