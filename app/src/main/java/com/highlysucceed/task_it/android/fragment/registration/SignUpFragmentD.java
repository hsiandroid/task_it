package com.highlysucceed.task_it.android.fragment.registration;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class SignUpFragmentD extends BaseFragment implements View.OnClickListener {

    public RegistrationActivity registrationActivity;

    @BindView(R.id.loginBTN)        TextView loginBTN;

    public static SignUpFragmentD newInstance(){
        SignUpFragmentD signUpFragmentD = new SignUpFragmentD();
        return signUpFragmentD;
    }

    @Override
    public int onLayoutSet(){
        return R.layout.fragment_signup_d;
    }

    @Override
    public void onViewReady(){
        registrationActivity = (RegistrationActivity) getContext();
        loginBTN.setOnClickListener(this);

    }


    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.loginBTN:
                registrationActivity.openLoginFragment();
                break;
        }

    }
}
