package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.fragment.main.HomeFragment;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class HomeServiceDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = HomeServiceDialog.class.getName().toString();


	public Callback  callback;

	@BindView(R.id.continueBTN)     View continueBTN;
	@BindView(R.id.leftBTN)     	View leftBTN;
	@BindView(R.id.rightBTN)     	View rightBTN;
	@BindView(R.id.serviceIV) 		ImageView serviceIV;
	@BindView(R.id.serviceNameTXT)	TextView serviceNameTXT;

	Integer clickCount = 0;
	public Integer set = 0;

	public static HomeServiceDialog newInstance(Callback callback) {
		HomeServiceDialog greetingsDialog = new HomeServiceDialog();
		greetingsDialog.callback = callback;
		return greetingsDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_home_service;
	}

	@Override
	public void onViewReady() {
		leftBTN.setOnClickListener(this);
		rightBTN.setOnClickListener(this);
        continueBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
                if (callback != null){
                    callback.onServiceSelect(this);
                }
                dismiss();
				break;
		}

		leftBTN.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {

				if(clickCount < 3){
					serviceImages(clickCount);
					clickCount += 1;
				}else if (clickCount == 3){
					clickCount = 0;
				}

			}
		});

		rightBTN.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(clickCount < 3){
					serviceImages(clickCount);
					clickCount += 1;
				}else if (clickCount == 3){
					clickCount = 0;
				}
			}
		});
	}
	public interface Callback{
		void onServiceSelect(HomeServiceDialog homeServiceDialog);
	}

	public void serviceImages(Integer a){

		switch (a){
			case 0:
				serviceIV.setImageResource(R.drawable.icon_service_default);
				serviceNameTXT.setText("INTERIOR DESIGN");
				set = 0;
				break;
			case 1:
				serviceIV.setImageResource(R.drawable.service_pest_control);
				serviceNameTXT.setText("PEST CONTROL");
				set = 1;
				break;
			case 2:
				serviceIV.setImageResource(R.drawable.service_plumbing);
				serviceNameTXT.setText("PLUMBING");
				set = 2;
		}
	}



}
