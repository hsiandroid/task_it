package com.highlysucceed.task_it.android.fragment.notification;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.NotificationActivity;
import com.highlysucceed.task_it.android.adapter.NotificationSettingsRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.NotificationSettingModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class NotificationSettingsFragment extends BaseFragment implements NotificationSettingsRecyclerViewAdapter.ClickListener {
    public static final String TAG = NotificationSettingsFragment.class.getName().toString();

    private NotificationActivity notificationActivity;
    private NotificationSettingsRecyclerViewAdapter notificationSettingsRecyclerViewAdapter;
    private LinearLayoutManager notificationSettingsLLM;

    @BindView(R.id.notificationSettingsRV)          RecyclerView notificationSettingsRV;

    public static NotificationSettingsFragment newInstance() {
        NotificationSettingsFragment notificationSettingsFragment = new NotificationSettingsFragment();
        return notificationSettingsFragment;
    }

    @Override
    public void onViewReady() {
        notificationActivity = (NotificationActivity) getActivity();
        notificationActivity.setTitle(getString(R.string.title_notification_settings));
        setUpHelpListView();
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_notification_settings;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void setUpHelpListView(){
        notificationSettingsLLM = new LinearLayoutManager(getContext());
        notificationSettingsRV.setLayoutManager(notificationSettingsLLM);
        notificationSettingsRecyclerViewAdapter = new NotificationSettingsRecyclerViewAdapter(getContext());
        notificationSettingsRV.setAdapter(notificationSettingsRecyclerViewAdapter);
        notificationSettingsRecyclerViewAdapter.setNewData(getData());
        notificationSettingsRecyclerViewAdapter.setClickListener(this);
    }
    private List<NotificationSettingModel> getData(){
        List<NotificationSettingModel> notificationSettingModels = new ArrayList<>();

        NotificationSettingModel notificationSettingModel = new NotificationSettingModel();
        notificationSettingModel.id = 1;
        notificationSettingModel.name = "Booking Status";
        /*notificationSettingModel.is_activated = true;*/
        notificationSettingModels.add(notificationSettingModel);

        notificationSettingModel = new NotificationSettingModel();
        notificationSettingModel.id = 2;
        notificationSettingModel.name = "What\'s New";
        /*notificationSettingModel.is_activated = false;*/
        notificationSettingModels.add(notificationSettingModel);

        notificationSettingModel = new NotificationSettingModel();
        notificationSettingModel.id = 3;
        notificationSettingModel.name = "Promotions";
        /*notificationSettingModel.is_activated = false;*/
        notificationSettingModels.add(notificationSettingModel);

        return notificationSettingModels;
    }

    @Override
    public void onItemClick(NotificationSettingModel helpModel) {

    }
}
