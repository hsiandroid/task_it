package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.NotificationModel;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class NotificationRecyclerViewAdapter extends BaseRecylerViewAdapter<NotificationRecyclerViewAdapter.ViewHolder, NotificationModel>{

    private ClickListener clickListener;

    public NotificationRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_notification));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.contentTXT.setText(holder.getItem().content);
        holder.dateTXT.setText(holder.getItem().time_passed);
        holder.dateTXT.setText(holder.getItem().title);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.contentTXT)      TextView contentTXT;
        @BindView(R.id.titleTXT)        TextView titleTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public NotificationModel getItem() {
            return (NotificationModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(AndroidModel directoryItem);
    }
} 
