package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.highlysucceed.task_it.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PostingSpinnerAdapter extends ArrayAdapter {
	private Context context;
	private List<String> data;
    private LayoutInflater layoutInflater;

	public PostingSpinnerAdapter(Context context) {
        super(context, R.layout.adapter_spinner);
		this.context = context;
		this.data  = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
	}

    public void setNewData(List<String> data){
        this.data = data;
        notifyDataSetChanged();

    }

    public void removeItemPos(int pos){
        this.data.remove(pos);
        notifyDataSetChanged();
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = layoutInflater.inflate(R.layout.adapter_spinner, parent, false);

        TextView spinner = (TextView) convertView.findViewById(R.id.spinnerTXT);
        spinner.setText(data.get(position));
        spinner.setTextSize(16);
        return spinner;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.adapter_spinner, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        holder.posting = data.get(position);
        holder.spinnerTXT.setText(holder.posting);
        holder.spinnerTXT.setTextColor(ActivityCompat.getColor(getContext(),R.color.fade_gray));
        holder.spinnerTXT.setTextSize(16);
		return convertView;
	}

	public class ViewHolder{
        String posting;

        @BindView(R.id.spinnerTXT)  TextView spinnerTXT;
        View view;

        public ViewHolder(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}
} 
