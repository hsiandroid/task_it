package com.highlysucceed.task_it.android.activity;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.fragment.main.SearchRefineFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;

public class SearchActivity extends RouteActivity{
    public static final String TAG = SearchActivity.class.getName().toString();



    @Override
    public int onLayoutSet(){
        return R.layout.activity_search;

    }

    @Override
    public void onViewReady(){

    }


    public void openSearchRefineFragment() {
        switchFragment(SearchRefineFragment.newInstance());
    }

}
