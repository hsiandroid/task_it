package com.highlysucceed.task_it.android.fragment.settings;

import android.widget.ListView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.adapter.CalendarViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class CalendarViewFragment extends BaseFragment
        implements CalendarViewAdapter.ClickListener {

    public static final String TAG = CalendarViewFragment.class.getName().toString();
    private CalendarViewAdapter calendarViewAdapter;
    private MonthCallback monthCallback;
    private YearCallback yearCallback;

    public enum Type {
        MONTH,
        YEAR
    }

    @BindView(R.id.listView)    ListView listView;

    @State int value;
    @State Type type;

    public static CalendarViewFragment newInstance(int value, Type type) {
        CalendarViewFragment calendarViewFragment = new CalendarViewFragment();
        calendarViewFragment.type = type;
        calendarViewFragment.value = value;
        return calendarViewFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_calendar_view;
    }

    @Override
    public void onViewReady() {
        calendarViewAdapter = new CalendarViewAdapter(getContext());
        calendarViewAdapter.setOnItemClickListener(this);
        listView.setAdapter(calendarViewAdapter);

        switch (type){
            case MONTH:
                setupMonth();
                break;

            case YEAR:
                setupYear();
                break;
        }
    }

    private void setupMonth(){
        calendarViewAdapter.setNewData(getMonth());
        calendarViewAdapter.setSelectedValue(value);
        listView.setSelection(value);
    }

    private void setupYear(){
        calendarViewAdapter.setNewData(getYear());
        calendarViewAdapter.setSelectedValue(getYearIndex(value));
        listView.setSelection(getYearIndex(value));
    }

    public void setMonthCallback(MonthCallback monthCallback) {
        this.monthCallback = monthCallback;
    }

    public void setYearCallback(YearCallback yearCallback) {
        this.yearCallback = yearCallback;
    }

    private int getYearIndex(int year){
        List<String> years = getYear();
        for(String string : years){
            if(string.equals(String.valueOf(year))){
                return years.indexOf(string);
            }
        }
        return 0;
    }

    public List<String> getMonth(){
        List<String> month = new ArrayList<>();
        month.add("January");
        month.add("February");
        month.add("March");
        month.add("April");
        month.add("May");
        month.add("June");
        month.add("July");
        month.add("August");
        month.add("September");
        month.add("October");
        month.add("November");
        month.add("December");
        return month;
    }

    public List<String> getYear(){
        int currentYear = Calendar.getInstance().get(Calendar.YEAR) -3;
        List<String> year = new ArrayList<>();
        for(int i = currentYear - 100 ; i <= currentYear ; i++){
            year.add(String.valueOf(i));
        }
        return year;
    }

    public interface MonthCallback{
        void month(int id, String month);
    }

    public interface YearCallback{
        void year(int year);
    }

    @Override
    public void onItemClick(int index, String string) {
        calendarViewAdapter.setSelectedValue(index);
        switch (type){
            case MONTH:
                if(monthCallback != null){
                    monthCallback.month(index, string);
                }
                break;
            case YEAR:
                if(yearCallback != null){
                    yearCallback.year(Integer.parseInt(getYear().get(index)));
                }
                break;
        }
    }
}
