package com.highlysucceed.task_it.android.fragment.location;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.LocationActivity;
import com.highlysucceed.task_it.android.adapter.AddLocationRecyclerViewAdapter;
import com.highlysucceed.task_it.android.adapter.LocationRecyclerViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.data.model.server.AddLocationModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class LocationCreateFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = LocationCreateFragment.class.getName().toString();

    private LocationActivity locationActivity;
    private LinearLayoutManager linearLayoutManager;
    private AddLocationRecyclerViewAdapter addLocationRecyclerViewAdapter;


    @BindView(R.id.saveBTN)                    TextView saveBTN;
   /* @BindView(R.id.locationAddressTXT)         TextView locationAddressTXT;
    @BindView(R.id.locationTitleTXT)           TextView locationTitleTXT;*/
    @BindView(R.id.locationTXT)                TextView locationTXT;
    @BindView(R.id.addLocationRV)               RecyclerView addLocationRV;

    public static LocationCreateFragment newInstance() {
        LocationCreateFragment locationCreateFragment = new LocationCreateFragment();
        return locationCreateFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_create_location;
    }

    @Override
    public void onViewReady() {
        locationActivity = (LocationActivity) getActivity();
        locationActivity.setTitle(getString(R.string.title_location_create));
        setUpLocationListView();

        saveBTN.setOnClickListener(this);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.saveBTN:
                getActivity().finish();
                break;
        }
    }

   /* private void sampleLocationData() {

        addLocationModels.clear();

        AddLocationModel addLocationModel = new AddLocationModel();
        addLocationModel.id = 1;
        addLocationModel.location_title = "SMDC Boni";
        addLocationModel.location_address = "BGC Taguig";
        addLocationModels.add(addLocationModel);

        addLocationModel = new AddLocationModel();
        addLocationModel.id = 2;
        addLocationModel.location_title = "SM Megamall";
        addLocationModel.location_address ="Shaw Blvd. Mandaluyong";
        addLocationModels.add(addLocationModel);
    }*/

    private void setUpLocationListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        addLocationRecyclerViewAdapter = new AddLocationRecyclerViewAdapter(getContext());
        addLocationRV.setLayoutManager(linearLayoutManager);
        addLocationRV.setAdapter(addLocationRecyclerViewAdapter);
        /*addLocationRecyclerViewAdapter.setClickListener(this);*/
        addLocationRecyclerViewAdapter.setNewData(getData());
    }

    private List<AddLocationModel> getData(){
        List<AddLocationModel> locationModels = new ArrayList<>();

        AddLocationModel locationModel = new AddLocationModel();
        locationModel.id = 1;
        locationModel.location_title= "SM Light Mall";
        locationModel.location_address= "San Juan City,";
        locationModels.add(locationModel);

        return locationModels;
    }


}
