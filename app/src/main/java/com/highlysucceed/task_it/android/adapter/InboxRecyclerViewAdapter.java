package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.InboxModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

public class InboxRecyclerViewAdapter extends BaseRecylerViewAdapter<InboxRecyclerViewAdapter.ViewHolder, InboxModel>{

    private ClickListener clickListener;

    public InboxRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_inbox));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setTag(holder.getItem());
        holder.getBaseView().setOnClickListener(this);
        holder.nameTXT.setText(holder.getItem().name);

        Picasso.with(getContext())
                .load(holder.getItem().full_path)
                .error(R.drawable.placeholder_avatar)
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.imageCIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)             TextView nameTXT;
        @BindView(R.id.imageCIV)            ImageView imageCIV;

        public ViewHolder(View view) {
            super(view);
        }

        public InboxModel getItem() {
            return (InboxModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((InboxModel) v.getTag());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((InboxModel) v.getTag());
        }
        return super.onLongClick(v);
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener{
        void onItemClick(InboxModel inboxModel);
    }
} 
