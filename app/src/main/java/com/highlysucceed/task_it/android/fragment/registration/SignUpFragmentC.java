package com.highlysucceed.task_it.android.fragment.registration;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class SignUpFragmentC extends BaseFragment implements View.OnClickListener {

    public RegistrationActivity registrationActivity;

    @BindView(R.id.resendEmailBTN)      TextView resendEmailBTN;

    public static SignUpFragmentC newInstance(){
        SignUpFragmentC signUpFragmentC = new SignUpFragmentC();
        return signUpFragmentC;
    }


    @Override
    public int onLayoutSet(){
        return R.layout.fragment_signup_c;
    }

    @Override
    public void onViewReady(){
        registrationActivity = (RegistrationActivity) getContext();
        resendEmailBTN.setOnClickListener(this);

    }
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.resendEmailBTN:
                registrationActivity.openSignUpFragmentD();
                break;
        }

    }
}
