package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.BookingServiceActivity;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.data.model.server.ProviderPersonnelModel;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProviderPersonnelRecyclerViewAdapter extends BaseRecylerViewAdapter<ProviderPersonnelRecyclerViewAdapter.ViewHolder, ProviderPersonnelModel>{

    private ClickListener clickListener;

    private BookingServiceActivity bookingServiceActivity;


    public ProviderPersonnelRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_provider_personnel));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        holder.bookServiceBTN.setTag(holder.getItem());
        holder.bookServiceBTN.setOnClickListener(this);
        holder.serviceMessageBTN.setTag(holder.getItem());
        holder.serviceMessageBTN.setOnClickListener(this);
        holder.companyPersonnelTXT.setText(holder.getItem().companyPersonnel);
        holder.priceRangeTXT.setText(holder.getItem().priceRange);
        holder.awayTXT.setText(holder.getItem().away);

        Picasso.with(getContext())
                .load(holder.getItem().full_path)
                .error(R.drawable.placeholder_avatar)
                .placeholder(R.drawable.placeholder_avatar)
                .into(holder.imageCIV);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.companyPersonnelTXT)     TextView companyPersonnelTXT;
        @BindView(R.id.priceRangeTXT)           TextView priceRangeTXT;
        @BindView(R.id.awayTXT)                 TextView awayTXT;
        @BindView(R.id.imageCIV)                CircleImageView imageCIV;
        @BindView(R.id.serviceMessageBTN)       ImageView serviceMessageBTN;
        @BindView(R.id.bookServiceBTN)          ImageView bookServiceBTN;

        public ViewHolder(View view) {
            super(view);
        }

        public ProviderPersonnelModel getItem() {
            return (ProviderPersonnelModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.serviceMessageBTN:
                if (clickListener != null) {
                    clickListener.onItemClick(((ProviderPersonnelModel) v.getTag()));
                }
                break;
            case R.id.bookServiceBTN:
                if (clickListener != null) {
                    clickListener.onBookClick(((ProviderPersonnelModel) v.getTag()));
                }
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(ProviderPersonnelModel providerPersonnelModel);
        void onBookClick(ProviderPersonnelModel providerPersonnelModel);
    }
} 
