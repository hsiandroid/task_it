package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.route.RouteActivity;
import com.highlysucceed.task_it.data.preference.UserData;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;
import com.highlysucceed.task_it.vendor.android.java.ToastMessage;

import butterknife.BindView;

public class LogoutDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = LogoutDialog.class.getName().toString();

	@BindView(R.id.continueTV) TextView continueTV;
	@BindView(R.id.cancelTV) TextView cancelTV;

	public static LogoutDialog newInstance() {
		LogoutDialog logoutDialog = new LogoutDialog();
		return logoutDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_logout;
	}

	@Override
	public void onViewReady() {
		continueTV.setOnClickListener(this);
		cancelTV.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
//		EventBus.getDefault().register(this);
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
	}

//	@Override
//	public void onStop() {
//		EventBus.getDefault().unregister(this);
//		super.onStop();
//	}

	private void attemptLogout(){
//		LogoutRequest refreshTokenRequest = new LogoutRequest(getContext());
//		refreshTokenRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Logging out...", false, false))
//				.addParameters(Variable.server.key.ID,UserData.getUserItem().id)
//				.execute();
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.continueTV:
				logout();
				break;
			case R.id.cancelTV:
				dismiss();
				break;
		}
	}

	private void logout(){
		ToastMessage.show(getActivity(), "Successful logout", ToastMessage.Status.SUCCESS);
		UserData.clearData();
		dismiss();
		getActivity().finish();
		((RouteActivity) getContext()).startRegistrationActivity();
	}

//	@Subscribe
//	public void onResponse(LogoutRequest.ServerResponse responseData) {
//		BaseTransformer baseTransformer = responseData.getData(BaseTransformer.class);
//		if(baseTransformer.status){
//			ToastMessage.show(getActivity(), baseTransformer.msg + UserData.getUserItem().fname + " " + UserData.getUserItem().lname, ToastMessage.Status.SUCCESS);
//			UserData.clearData();
//			dismiss();
//			((RouteActivity) getContext()).startRegistrationActivity();
//			((RouteActivity) getContext()).finish();
////			ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.SUCCESS);
//		}else{
//			ToastMessage.show(getActivity(), baseTransformer.msg, ToastMessage.Status.FAILED);
//		}
//	}
}
