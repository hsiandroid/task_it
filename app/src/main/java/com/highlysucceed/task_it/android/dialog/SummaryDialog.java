package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.fragment.main.HomeFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class SummaryDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = SummaryDialog.class.getName().toString();

	@BindView(R.id.designationTXT)			TextView designationTXT;
	@BindView(R.id.timeTXT)					TextView timeTXT;
	@BindView(R.id.subTotalTXT)				TextView subTotalTXT;
	@BindView(R.id.discountTXT)				TextView discountTXT;
	@BindView(R.id.totalTXT)				TextView totalTXT;
	@BindView(R.id.addBTN)					View addBTN;
	@BindView(R.id.confirmBTN)				TextView confirmBTN;

	public static SummaryDialog newInstance() {
		SummaryDialog greetingsDialog = new SummaryDialog();
		greetingsDialog.setCancelable(false);
		return greetingsDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_summary;
	}

	@Override
	public void onViewReady() {
		confirmBTN.setOnClickListener(this);
		addBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.confirmBTN:
				dismiss();
				((RouteActivity)getContext()).switchFragment(HomeFragment.newInstance());
				break;
			case R.id.addBTN:
				break;

		}
	}

}
