package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import butterknife.BindView;

public class GreetingsDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = GreetingsDialog.class.getName().toString();

	public Callback  callback;

	@BindView(R.id.placeTXT)			TextView placeTXT;
	@BindView(R.id.continueBTN)			TextView continueBTN;
	@BindView(R.id.newLocationBTN)		TextView newLocationBTN;
	@BindView(R.id.nameTXT)				TextView nameTXT;

	public static GreetingsDialog newInstance(Callback callback) {
		GreetingsDialog greetingsDialog = new GreetingsDialog();
		greetingsDialog.callback = callback;
		greetingsDialog.setCancelable(false);
		return greetingsDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_greetings;
	}

	@Override
	public void onViewReady() {
		nameTXT.setText(" " + "James" + "!");

		continueBTN.setOnClickListener(this);
		newLocationBTN.setOnClickListener(this);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
				dismiss();
				break;
			case R.id.newLocationBTN:
				if (callback != null){
					callback.openLocationFragment(this);
				}
				dismiss();
				break;
		}
	}
	public interface Callback{
		void openLocationFragment(GreetingsDialog greetingsDialog);
	}
}
