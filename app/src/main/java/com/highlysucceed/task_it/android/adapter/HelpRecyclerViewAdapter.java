package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.HelpModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class HelpRecyclerViewAdapter extends BaseRecylerViewAdapter<HelpRecyclerViewAdapter.ViewHolder, HelpModel>{

    private ClickListener clickListener;

    public HelpRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_help));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setTag(holder.getItem());
        holder.getBaseView().setOnClickListener(this);
        holder.titleTXT.setText(holder.getItem().title);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.titleTXT)                TextView titleTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public HelpModel getItem() {
            return (HelpModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((HelpModel) v.getTag());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onItemClick(HelpModel helpModel);
    }
} 
