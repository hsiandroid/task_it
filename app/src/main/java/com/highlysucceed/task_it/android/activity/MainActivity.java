package com.highlysucceed.task_it.android.activity;

import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.dialog.LogoutDialog;
import com.highlysucceed.task_it.android.dialog.ReferralDialog;
import com.highlysucceed.task_it.android.fragment.main.BookingProfileFragment;
import com.highlysucceed.task_it.android.fragment.main.HomeFragment;
import com.highlysucceed.task_it.android.fragment.main.InboxFragment;
import com.highlysucceed.task_it.android.fragment.main.LocationFragment;
import com.highlysucceed.task_it.android.fragment.main.NotificationFragment;
import com.highlysucceed.task_it.android.fragment.main.PaymentFragment;
import com.highlysucceed.task_it.android.fragment.main.PostingFragment;
import com.highlysucceed.task_it.android.fragment.main.ProfileFragment;
import com.highlysucceed.task_it.android.fragment.main.ScheduleFragment;
import com.highlysucceed.task_it.android.fragment.booking.ServiceProviderDetailsFragment;
import com.highlysucceed.task_it.android.fragment.main.SearchRefineFragment;
import com.highlysucceed.task_it.android.fragment.main.SearchResultsFragment;
import com.highlysucceed.task_it.android.fragment.main.ServiceStatusFragment;
import com.highlysucceed.task_it.android.fragment.main.SettingsFragment;
import com.highlysucceed.task_it.android.fragment.main.YellowPageFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;
import com.highlysucceed.task_it.data.model.db.UserItem;
import com.highlysucceed.task_it.data.preference.UserData;
import com.highlysucceed.task_it.vendor.android.java.Keyboard;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends RouteActivity
        implements NavigationView.OnNavigationItemSelectedListener,View.OnClickListener {

    private NavigationHeader navigationHeader;
    private static Unbinder navigationUnBinder;

    @BindView(R.id.drawerLayout)    DrawerLayout drawerLayout;
    @BindView(R.id.navigationView)  NavigationView navigationView;
    @BindView(R.id.mainTitleTXT)    TextView mainTitleTXT;
    @BindView(R.id.mainIconIV)      View mainIconIV;
    @BindView(R.id.notifSettingIV)  View notifSettingIV;
    @BindView(R.id.profileIV)       View profileIV;
    @BindView(R.id.backBTN)         View backBTN;
    @BindView(R.id.drawerBTN)       View drawerBTN;


    @Override
    public int onLayoutSet() {
        return R.layout.activity_main;
    }

    @Override
    public void onViewReady() {
        navigationView.setNavigationItemSelectedListener(this);
        navigationHeader = NavigationHeader.newInstance(navigationView.getHeaderView(0));
        mainIconIV.setOnClickListener(this);
        profileIV.setOnClickListener(this);
        disableNavigationViewScrollbars();
        notifSettingIV.setOnClickListener(this);
        displayUserData(UserData.getUserItem());
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "home":
                openHomeFragment();
                break;
            case "chat":
                openChatFragment();
                break;
            case "service":
                openServiceProviderFragment();
                break;
            case "status":
                    openServiceStatusFragment();
                    break;
                default:
                    openHomeFragment();
                    break;

        }
    }

    public void openHomeFragment(){
        switchFragment(HomeFragment.newInstance());
        setSelectedItem(R.id.nav_home);
    }

    public void openYellowPageFragment(){
        switchFragment(YellowPageFragment.newInstance());
        setSelectedItem(R.id.nav_yellow_pages);
    }

    public void openChatFragment(){
        switchFragment(InboxFragment.newInstance());
        /*setSelectedItem(R.id.nav_home);*/
    }


    public void openPaymentFragment(){
        switchFragment(PaymentFragment.newInstance());
        setSelectedItem(R.id.nav_payment);
    }

    public void openNotificationFragment(){
        switchFragment(NotificationFragment.newInstance());
        setSelectedItem(R.id.nav_notification);
    }


    public void openLocationFragment(){
        switchFragment(LocationFragment.newInstance());
        setSelectedItem(R.id.nav_location);
    }

    public void openSettingsFragment(){
        switchFragment(SettingsFragment.newInstance());
        setSelectedItem(R.id.nav_settings);
    }

    public void openBookingProfileFragment(){
        switchFragment(BookingProfileFragment.newInstance());
        setSelectedItem(R.id.nav_booking_profile);
    }
    public void openProfileFragment(){
        switchFragment(ProfileFragment.newInstance());
    }

    public void openServiceStatusFragment(){
        switchFragment(ServiceStatusFragment.newInstance());
    }

    /*booking history goes to setting activity*/
   /* public void openBookingHistoryFragment(){
        switchFragment(BookingHistoryFragment.newInstance());
        *//*setSelectedItem(R.id.nav_history);*//*
    }*/

    public void openScheduleFragment(){
        switchFragment(ScheduleFragment.newInstance());
        setSelectedItem(R.id.nav_schedule);
    }



    public void openServiceProviderFragment(){
        switchFragment(ServiceProviderDetailsFragment.newInstance());
       /* setSelectedItem(R.id.nav_posting);*/
    }

    public void openReferralFragment(){
//        switchFragment(ScheduleFragment.newInstance());
        ReferralDialog.newInstance().show(getSupportFragmentManager(),ReferralDialog.TAG);
       /* setSelectedItem(R.id.nav_referral);*/
    }

    public void openSearchRefineFragment() {
        switchFragment(SearchRefineFragment.newInstance());
//        drawerBTN.setVisibility(View.GONE);
//        backBTN.setVisibility(View.VISIBLE);
    }

    public void openSearchResultsFragment(){
        switchFragment(SearchResultsFragment.newInstance());
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.mainIconIV:
                Keyboard.hideKeyboard(this);
                showNavigationDrawer();
                break;
            case R.id.notifSettingIV:
                this.startNotificationActivity("settings");
                break;
            case R.id.profileIV:
                this.startSettingsActivity("edit");
                break;
        }
    }

    public void displayUserData(UserItem userItem){
//        navigationHeader.nameTXT.setText(userItem.fname + " " + userItem.lname);
        navigationHeader.nameTXT.setText("John Doe");
        Picasso.with(getContext())
                .load(userItem.image)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(navigationHeader.profileCIV);
        navigationHeader.profileCIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openProfileFragment();
                drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

    }

    public static class NavigationHeader {

        private MainActivity mainActivity;

        @BindView(R.id.profileCIV)          CircleImageView profileCIV;
        @BindView(R.id.nameTXT)             TextView nameTXT;

        public static NavigationHeader newInstance(View view) {
            return new NavigationHeader(view);
        }


        public NavigationHeader(View view){
            navigationUnBinder = ButterKnife.bind(this, view);
        }

    }

    private void disableNavigationViewScrollbars() {
        if (navigationView != null) {
            NavigationMenuView navigationMenuView = (NavigationMenuView) navigationView.getChildAt(0);
            if (navigationMenuView != null) {
                navigationMenuView.setVerticalScrollBarEnabled(false);
            }
        }
    }

    public void openLogoutDialogFragment(){
        LogoutDialog logoutDialog = LogoutDialog.newInstance();
        logoutDialog.show(getSupportFragmentManager(), logoutDialog.TAG);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void showNavigationDrawer(){
        drawerLayout.openDrawer(Gravity.LEFT);
    }

    public void setTitle(String title){
        mainTitleTXT.setText(title);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_home:
                openHomeFragment();
                break;
            case R.id.nav_yellow_pages:
                openYellowPageFragment();
                break;
            case R.id.nav_schedule:
                openScheduleFragment();
                break;
            case R.id.nav_booking_profile:
                openBookingProfileFragment();
                break;
            case R.id.nav_location:
                openLocationFragment();
                break;
            case R.id.nav_notification:
                openNotificationFragment();
                break;
            case R.id.nav_payment:
                openPaymentFragment();
                break;
            case R.id.nav_settings:
                openSettingsFragment();
                break;
            case R.id.nav_logout:
                openLogoutDialogFragment();
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void setSelectedItem(int itemId) {

        for (int pos = 0; pos < navigationView.getMenu().size(); pos++) {
            navigationView.getMenu().getItem(pos).setChecked(false);
        }

        navigationView.getMenu().findItem(itemId).setChecked(true);
    }

    public void setupSettingButton(int a){
        if(a == 2){ //notification
            profileIV.setVisibility(View.GONE);
            notifSettingIV.setVisibility(View.VISIBLE);
        }else if (a == 3){ //profile
            notifSettingIV.setVisibility(View.GONE);
            profileIV.setVisibility(View.VISIBLE);
        } else {
            notifSettingIV.setVisibility(View.GONE);
            profileIV.setVisibility(View.GONE);
        }
    }


}
