package com.highlysucceed.task_it.android.fragment.bookingprofile;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.BookingProfileActivity;
import com.highlysucceed.task_it.android.adapter.BookingPreferenceListViewAdapter;
import com.highlysucceed.task_it.data.model.server.BookingPreferenceModel;
import com.highlysucceed.task_it.data.model.server.BookingProfileModel;
import com.highlysucceed.task_it.data.model.server.ServiceTypeModel;
import com.highlysucceed.task_it.data.preference.UserData;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.android.widget.ExpandableHeightListView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class BookingProfileCreateFragment extends BaseFragment implements View.OnClickListener {
    public static final String TAG = BookingProfileCreateFragment.class.getName().toString();

    private BookingProfileActivity bookingProfileActivity;

    private BookingPreferenceListViewAdapter bookingPreferenceListViewAdapter;
    private List<ServiceTypeModel> serviceTypeModels;

    private BookingProfileModel bookingProfileModel;

    private int count = 0;

    @BindView(R.id.preferenceEHLV)          ExpandableHeightListView preferenceEHLV;
    @BindView(R.id.imageCIV)                ImageView imageCIV;
    @BindView(R.id.imageIV)                 ImageView imageIV;
    @BindView(R.id.previousBTN)             ImageView previousBTN;
    @BindView(R.id.nextBTN)                 ImageView nextBTN;
    @BindView(R.id.serviceTypeTXT)          TextView serviceTypeTXT;
    @BindView(R.id.nameTXT)                 TextView nameTXT;
    @BindView(R.id.addressTXT)              TextView addressTXT;
    @BindView(R.id.contactTXT)              TextView contactTXT;
    @BindView(R.id.emailTXT)                TextView emailTXT;
    @BindView(R.id.saveBTN)                 TextView saveBTN;

    public static BookingProfileCreateFragment newInstance(BookingProfileModel bookingProfileModel) {
        BookingProfileCreateFragment bookingProfileEditFragment = new BookingProfileCreateFragment();
        bookingProfileEditFragment.bookingProfileModel = bookingProfileModel;
        return bookingProfileEditFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_booking_profile_edit;
    }

    @Override
    public void onViewReady() {
        bookingProfileActivity  = (BookingProfileActivity) getActivity();
        bookingProfileActivity.setTitle(getString(R.string.booking_profile_create_title));

        nextBTN.setOnClickListener(this);
        previousBTN.setOnClickListener(this);
        saveBTN.setOnClickListener(this);
        displayData();

        serviceTypeData();

        setUpBookingPreference();
    }

    private void setUpBookingPreference(){
        bookingPreferenceListViewAdapter = new BookingPreferenceListViewAdapter(getContext());
        preferenceEHLV.setAdapter(bookingPreferenceListViewAdapter);
        bookingPreferenceListViewAdapter.setNewData(getData());
    }

    private List<BookingPreferenceModel> getData(){
        List<BookingPreferenceModel> bookingPreferenceModels = new ArrayList<>();

        BookingPreferenceModel bookingPreferenceModel = new BookingPreferenceModel();
        bookingPreferenceModel.id = 1;
        bookingPreferenceModel.name = "John Doe";
        bookingPreferenceModel.service_name = "Interior Design";
        bookingPreferenceModel.designation = "Makati";
        bookingPreferenceModel.full_path = "https://cdn.pixabay.com/photo/2014/04/03/10/32/businessman-310819_960_720.png";
        bookingPreferenceModels.add(bookingPreferenceModel);

        bookingPreferenceModel = new BookingPreferenceModel();
        bookingPreferenceModel.id = 2;
        bookingPreferenceModel.name = "John Smith";
        bookingPreferenceModel.service_name = "Exterior Design";
        bookingPreferenceModel.designation = "Makati";
        bookingPreferenceModel.full_path = "https://bootdey.com/img/Content/avatar/avatar8.png";
        bookingPreferenceModels.add(bookingPreferenceModel);

        return bookingPreferenceModels;
    }

    private void displayData(){
        Picasso.with(getContext())
                .load(UserData.getUserItem().image)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(imageCIV);

        nameTXT.setText(UserData.getUserItem().fname + " "  + UserData.getUserItem().lname);
        addressTXT.setText(UserData.getUserItem().info.data.address);
        contactTXT.setText(UserData.getUserItem().info.data.contact_number);
        emailTXT.setText(UserData.getUserItem().email);
    }

    private void serviceTypeData(){
       serviceTypeModels = new ArrayList<>();

        ServiceTypeModel serviceTypeModel = new ServiceTypeModel();
        serviceTypeModel.id = 1;
        serviceTypeModel.full_path = "https://marketplace.canva.com/MAB79xYLTlo/1/thumbnail/canva-businessman-avatar-business-icon-MAB79xYLTlo.png";
        serviceTypeModel.title  = "Interior Design";
        serviceTypeModels.add(serviceTypeModel);

        serviceTypeModel = new ServiceTypeModel();
        serviceTypeModel.id = 2;
        serviceTypeModel.full_path = "https://cdn.pixabay.com/photo/2013/07/13/10/07/man-156584_960_720.png";
        serviceTypeModel.title  = "Exterior Design";
        serviceTypeModels.add(serviceTypeModel);

        setUpServiceType(serviceTypeModels.get(0));
    }

    private void setUpServiceType(ServiceTypeModel serviceTypeModel){
        Picasso.with(getContext())
                .load(serviceTypeModel.full_path)
                .placeholder(R.drawable.placeholder_avatar)
                .error(R.drawable.placeholder_avatar)
                .into(imageIV);
        serviceTypeTXT.setText(serviceTypeModel.title);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.previousBTN:
                count-=1;
                if (count >0){
                    setUpServiceType(serviceTypeModels.get(count));
                }else {
                    count = 0;
                    setUpServiceType(serviceTypeModels.get(count));
                }
                Log.e("Count Size",">>>" + count);
                break;
            case R.id.nextBTN:
                count+=1;
                if (count < serviceTypeModels.size()){
                    setUpServiceType(serviceTypeModels.get(count));
                }else {
                    count = 0;
                    setUpServiceType(serviceTypeModels.get(count));
                }
                Log.e("Count Size",">>>" + count);
                break;
            case R.id.saveBTN:
                getActivity().finish();
                break;
        }
    }
}
