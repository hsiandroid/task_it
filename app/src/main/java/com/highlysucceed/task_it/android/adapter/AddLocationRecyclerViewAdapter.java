package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.AddLocationModel;
import com.highlysucceed.task_it.data.model.server.AddLocationModel;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class AddLocationRecyclerViewAdapter extends BaseRecylerViewAdapter<AddLocationRecyclerViewAdapter.ViewHolder, AddLocationModel>{

    private ClickListener clickListener;

    public AddLocationRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_add_location));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setOnClickListener(this);
        /*holder.nameTXT.setText("Position " + holder.getItem().id);*/
        holder.locationTitleTXT.setText(holder.getItem().location_title);
        holder.locationAddressTXT.setText(holder.getItem().location_address);
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.locationTitleTXT)     TextView locationTitleTXT;
        @BindView(R.id.locationAddressTXT)   TextView locationAddressTXT;



        public ViewHolder(View view) {
            super(view);
        }

        public AddLocationModel getItem() {
            return (AddLocationModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick(((ViewHolder) v.getTag()).getItem());
        }
    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends Listener{
        void onItemClick(AndroidModel directoryItem);
    }
} 
