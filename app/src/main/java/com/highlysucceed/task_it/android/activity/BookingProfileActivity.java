package com.highlysucceed.task_it.android.activity;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.fragment.bookingprofile.BookingProfileCreateFragment;
import com.highlysucceed.task_it.android.fragment.bookingprofile.BookingProfileEditFragment;
import com.highlysucceed.task_it.android.route.RouteActivity;
import com.highlysucceed.task_it.data.model.server.BookingProfileModel;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class BookingProfileActivity extends RouteActivity {
    public static final String TAG = BookingProfileActivity.class.getName().toString();

    @Override
    public int onLayoutSet() {
        return R.layout.activity_default;
    }

    @Override
    public void onViewReady() {
        registerBackPress(true);
    }

    @Override
    public void initialFragment(String activityName, String fragmentName) {
        switch (fragmentName){
            case "edit":
                openBookingProfileEditFragment((BookingProfileModel) getFragmentBundle().getParcelable("booking_profile_item"));
                break;
            case "create":
                openBookingProfileCreateFragment((BookingProfileModel) getFragmentBundle().getParcelable("booking_profile_item"));
                break;

        }
    }

    public void openBookingProfileEditFragment(BookingProfileModel bookingProfileModel){
        switchFragment(BookingProfileEditFragment.newInstance(bookingProfileModel));
    }

    public void openBookingProfileCreateFragment(BookingProfileModel bookingProfileModel){
        switchFragment(BookingProfileCreateFragment.newInstance(bookingProfileModel));
    }
}
