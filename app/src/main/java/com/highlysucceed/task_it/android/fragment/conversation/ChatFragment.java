package com.highlysucceed.task_it.android.fragment.conversation;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.EditText;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.ConversationActivity;
import com.highlysucceed.task_it.android.adapter.ConversationRecycleViewAdapter;
import com.highlysucceed.task_it.android.dialog.CommentOtherOptionDialog;
import com.highlysucceed.task_it.android.dialog.defaultdialog.ConfirmationDialog;
import com.highlysucceed.task_it.data.model.server.MessageModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.android.java.ScrollView.ObservableRecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import icepick.State;

public class ChatFragment extends BaseFragment implements
        View.OnClickListener,
        ConversationRecycleViewAdapter.ClickListener,
        SwipeRefreshLayout.OnRefreshListener,CommentOtherOptionDialog.CommentCallback{

    public static final String TAG = ChatFragment.class.getName().toString();

    private ConversationActivity conversationActivity;
    private ConversationRecycleViewAdapter conversationRecycleViewAdapter;
    public LinearLayoutManager messageLLM;

    @BindView(R.id.messageORV)          ObservableRecyclerView messageORV;
    @BindView(R.id.conversationSRL)     SwipeRefreshLayout conversationSRL;
    @BindView(R.id.messageET)           EditText messageET;
    @BindView(R.id.sendBTN)             View sendBTN;

    @State int messageID;
    public static ChatFragment newInstance(int messageID) {
        ChatFragment conversationFragment = new ChatFragment();
        conversationFragment.messageID = messageID;
        return conversationFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_conversation;
    }

    @Override
    public void onViewReady() {
        conversationActivity = (ConversationActivity) getContext();
        conversationActivity.setTitle(getString(R.string.title_main_inbox));
        setupConversationList();

        sendBTN.setOnClickListener(this);

        attemptInboxShow();

        sampleData();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.sendBTN:
                attemptSend();
                break;
        }
    }

    public void setupConversationList(){
        conversationRecycleViewAdapter = new ConversationRecycleViewAdapter(getContext());
        messageLLM = new LinearLayoutManager(getContext());
        messageLLM.setOrientation(LinearLayoutManager.VERTICAL);
        messageLLM.setReverseLayout(true);
        messageORV.setLayoutManager(messageLLM);
        messageORV.setAdapter(conversationRecycleViewAdapter);
        conversationRecycleViewAdapter.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
//        refreshList();
    }

    private void attemptInboxShow(){
        conversationSRL.setColorSchemeResources(R.color.colorPrimary);
        conversationSRL.measure(View.MEASURED_SIZE_MASK, View.MEASURED_HEIGHT_STATE_SHIFT);
        conversationSRL.setOnRefreshListener(this);
    }

    private void refreshList(){
//        inboxShowRequest
//                .showSwipeRefreshLayout(true)
//                .first();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    private void sampleData(){
        List<MessageModel> messageModels = new ArrayList<>();
        MessageModel messageModel = new MessageModel();
        messageModel.id = 1;
        messageModel.content = "Hi!";
        messageModel.time_passed = "12 mins ago.";
        messageModels.add(messageModel);

        messageModel = new MessageModel();
        messageModel.id = 1;
        messageModel.content = "Hello!";
        messageModel.time_passed = "8 mins ago.";
        messageModels.add(messageModel);

        messageModel = new MessageModel();
        messageModel.id = 0;
        messageModel.content = "This is just a test messge!";
        messageModel.time_passed = "5 mins ago";
        messageModels.add(messageModel);

        messageModel = new MessageModel();
        messageModel.id = 1;
        messageModel.content = "Nice";
        messageModel.time_passed = "2 mins ago";
        messageModels.add(messageModel);

        messageModel = new MessageModel();
        messageModel.id = 1;
        messageModel.content = "Wow";
        messageModel.time_passed = "1 mins ago";
        messageModels.add(messageModel);

        conversationRecycleViewAdapter.setNewData(messageModels);
    }

    public void attemptSend(){
        MessageModel messageModel = new MessageModel();
        messageModel.id = 0;
        messageModel.content = messageET.getText().toString();
        messageModel.time_passed = "Just now.";

        conversationRecycleViewAdapter.newMessage(messageModel);
        messageORV.smoothScrollToPosition(0);
        messageET.setText("");
    }

    @Override
    public void onRefresh() {
        refreshList();
    }

    @Override
    public void onItemClick(MessageModel messageModel) {

    }

    @Override
    public void onItemLongClick(MessageModel messageModel) {
        if (messageModel.id == 0){
            CommentOtherOptionDialog.newInstance(messageModel, this).show(getChildFragmentManager(),CommentOtherOptionDialog.TAG);
        }
    }

    @Override
    public void editComment(MessageModel messageModel) {

    }

    @Override
    public void deleteComment(final MessageModel messageModel) {
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Message")
                .setNote("Are you sure you want to remove this message?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        conversationRecycleViewAdapter.removeItem(messageModel);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }
}
