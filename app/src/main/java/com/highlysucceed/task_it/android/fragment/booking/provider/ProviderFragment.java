package com.highlysucceed.task_it.android.fragment.booking.provider;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.BookingServiceActivity;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.ProviderRecyclerViewAdapter;
import com.highlysucceed.task_it.data.model.server.ProviderModel;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.AndroidModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class ProviderFragment extends BaseFragment implements View.OnClickListener,ProviderRecyclerViewAdapter.ClickListener{
    public static final String TAG = ProviderFragment.class.getName().toString();

    private LinearLayoutManager linearLayoutManager;
    private ProviderRecyclerViewAdapter providerRecyclerViewAdapter;
    private BookingServiceActivity bookingServiceActivity;

    @BindView(R.id.provideRV)               RecyclerView providerRV;
    @BindView(R.id.editBookingDetailsBTN)   LinearLayout editBookingDetailsBTN;

    public static ProviderFragment newInstance() {
        ProviderFragment fragment = new ProviderFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
        bookingServiceActivity = (BookingServiceActivity) getContext();
        bookingServiceActivity.setTitle("Search Result");
        setUpProviderListView();
        editBookingDetailsBTN.setOnClickListener(this);
    }


    @Override
    public int onLayoutSet() {
        return R.layout.fragment_provider_search;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    private void setUpProviderListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        providerRecyclerViewAdapter = new ProviderRecyclerViewAdapter(getContext());
        providerRV.setLayoutManager(linearLayoutManager);
        providerRV.setAdapter(providerRecyclerViewAdapter);
        providerRecyclerViewAdapter.setClickListener(this);
        providerRecyclerViewAdapter.setNewData(getData());
    }

    private List<ProviderModel> getData(){
        List<ProviderModel> providerModels = new ArrayList<>();

        ProviderModel providerModel = new ProviderModel();
        providerModel.id = 1;
        providerModel.companyName = "Company Name";
        providerModel.away = "5km away";
        providerModel.priceRange = "Php 2000 - 5000";
        providerModels.add(providerModel);

        providerModel = new ProviderModel();
        providerModel.id = 2;
        providerModel.companyName = "Company Name";
        providerModel.away = "3km away";
        providerModel.priceRange = "Php 2000 - 5000";
        providerModels.add(providerModel);

        return providerModels;
    }

    @Override
    public void onItemClick(ProviderModel providerModel) {
            bookingServiceActivity.startBookingServiceActivity("personnel");
    }

    @Override
    public void onItemClick(AndroidModel androidModel) {

    }

    @Override
    public void onItemLongClick(AndroidModel androidModel) {

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.editBookingDetailsBTN:
                bookingServiceActivity.startMainActivity("home");
        }
    }
}
