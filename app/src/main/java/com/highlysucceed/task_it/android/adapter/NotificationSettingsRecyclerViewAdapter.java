package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.NotificationSettingModel;
import com.highlysucceed.task_it.vendor.android.base.BaseListViewAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseRecylerViewAdapter;

import butterknife.BindView;

public class NotificationSettingsRecyclerViewAdapter extends BaseRecylerViewAdapter<NotificationSettingsRecyclerViewAdapter.ViewHolder, NotificationSettingModel>{

    private ClickListener clickListener;

    public NotificationSettingsRecyclerViewAdapter(Context context) {
        super(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(getDefaultView(parent, R.layout.adapter_notification_setting));
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.setItem(getItem(position));
        holder.getBaseView().setTag(holder.getItem());
        holder.nameTXT.setText(holder.getItem().name);
        holder.getBaseView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(clickListener != null){
                    clickListener.onItemClick((NotificationSettingModel) v.getTag());
                }

                /*holder.switchBTN.setChecked(holder.getItem().is_activated);*/
            }
        });
    }

    public class ViewHolder extends BaseRecylerViewAdapter.ViewHolder{

        @BindView(R.id.nameTXT)                TextView nameTXT;

        public ViewHolder(View view) {
            super(view);
        }

        public NotificationSettingModel getItem() {
            return (NotificationSettingModel) super.getItem();
        }
    }

    @Override
    public void onClick(View v) {

    }

    public void setClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener extends BaseListViewAdapter.Listener {
        void onItemClick(NotificationSettingModel helpModel);
    }
} 
