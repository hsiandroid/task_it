package com.highlysucceed.task_it.android.fragment.main;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.dialog.LogoutDialog;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = ProfileFragment.class.getName().toString();

    private MainActivity mainActivity;

    @BindView(R.id.avatarCIV)           CircleImageView avatarCIV;
    @BindView(R.id.firstNameTXT)        TextView firstNameTXT;
    @BindView(R.id.lastNameTXT)         TextView lastNameTXT;
    @BindView(R.id.emailTXT)            TextView emailTXT;
    @BindView(R.id.logoutBTN)           TextView logoutBTN;
    @BindView(R.id.changePasswordBTN)   TextView changePasswordBTN;

        public static ProfileFragment newInstance() {
            ProfileFragment fragment = new ProfileFragment();
            return fragment;
        }

    @Override
    public void onViewReady() {
        changePasswordBTN.setOnClickListener(this);
        logoutBTN.setOnClickListener(this);
        mainActivity =(MainActivity) getActivity();
        mainActivity.setTitle("Profile");
        mainActivity = (MainActivity) getContext();
        mainActivity.setupSettingButton(3);
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_test_profile;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.logoutBTN:
                LogoutDialog.newInstance().show(getFragmentManager(), LogoutDialog.TAG);
                break;
            case R.id.changePasswordBTN:
                mainActivity.startSettingsActivity("password");
                break;
        }
    }
}
