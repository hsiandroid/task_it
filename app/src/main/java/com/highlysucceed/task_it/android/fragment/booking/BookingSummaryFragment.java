package com.highlysucceed.task_it.android.fragment.booking;

import android.util.Log;
import android.util.MutableBoolean;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.BookingServiceActivity;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.server.request.Auth;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.BindView;

public class BookingSummaryFragment extends BaseFragment implements View.OnClickListener{
    public static final String TAG = BookingSummaryFragment.class.getName().toString();

    private BookingServiceActivity bookingServiceActivity;

    @BindView(R.id.continueBTN)             TextView continueBTN;

    public static BookingSummaryFragment newInstance() {
        BookingSummaryFragment fragment = new BookingSummaryFragment();
        return fragment;
    }

    @Override
    public void onViewReady() {
    bookingServiceActivity = (BookingServiceActivity) getContext();
    bookingServiceActivity.setTitle("Confirm Booking Summary");
    continueBTN.setOnClickListener(this);
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_booking_summary;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void onResponse(Auth.LoginResponse loginResponse){
        Log.e("Message", loginResponse.getData(BaseTransformer.class).msg);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.continueBTN:
            bookingServiceActivity.openServiceDetailFragment();
            break;
        }
    }
}
