package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.widget.TextView;
import android.widget.TimePicker;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.adapter.CalendarAdapter;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;
import com.highlysucceed.task_it.vendor.android.widget.ExpandableHeightGridView;

import butterknife.BindView;

public class CalendarDialog extends BaseDialog implements
		View.OnClickListener,
		MonthYearDialog.DialogCallback	{
	public static final String TAG = CalendarDialog.class.getName().toString();

	private CalendarAdapter calendarAdapter;
	private DateTimePickerListener dateTimePickerListener;

	@BindView(R.id.calendarGV) 		ExpandableHeightGridView calendarGV;
	@BindView(R.id.mainIconIV)		View mainIconIV;
	/*@BindView(R.id.doneBTN)			View doneBTN;*/
	@BindView(R.id.prevBTN)			View prevBTN;
	@BindView(R.id.nextBTN)			View nextBTN;
	@BindView(R.id.monthBTN)		TextView monthBTN;
	@BindView(R.id.mainTitleTXT)	TextView mainTitleTXT;
	@BindView(R.id.timePicker)		TimePicker timePicker;

	private String month;
	private String day;
	private String year;
	private int monthNum;

	public static CalendarDialog newInstance(DateTimePickerListener dateTimePickerListener, String month, String day, String year,int monthNum) {
		CalendarDialog calendarDialog = new CalendarDialog();
		calendarDialog.dateTimePickerListener = dateTimePickerListener;
		calendarDialog.month = month;
		calendarDialog.day = day;
		calendarDialog.year = year;
		calendarDialog.monthNum = monthNum;
		return calendarDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_calendar;
	}

	@Override
	public void onViewReady() {
		mainIconIV.setOnClickListener(this);
		/*doneBTN.setOnClickListener(this);*/
		prevBTN.setOnClickListener(this);
		nextBTN.setOnClickListener(this);
		monthBTN.setOnClickListener(this);
		mainTitleTXT.setText("Birthday");
		setUpCalendar();
	}

	private void setUpCalendar(){
		calendarAdapter = new CalendarAdapter(getContext());
		if ((monthNum >=1) || (day !=null) || (month !=null) || (year != null)){
			calendarAdapter.setCurrentMonthNum(monthNum - 1);
			calendarAdapter.setCurrentYear(Integer.parseInt(year));
			calendarAdapter.setCurrentMonthName(month);
			calendarAdapter.setActualDay(Integer.parseInt(day));
			calendarAdapter.refresh();
		}
		calendarGV.setAdapter(calendarAdapter);
		calendarGV.setExpanded(true);
		displayMonth();
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.mainIconIV:
				dismiss();
				break;
			/*case R.id.doneBTN:
				if(dateTimePickerListener != null){
					dateTimePickerListener.forDisplay(calendarAdapter.getSelectedDate());
				}
				dismiss();
				break;*/
			case R.id.prevBTN:
				prevClick();
				break;
			case R.id.nextBTN:
				nextClick();
				break;
			case R.id.monthBTN:
				MonthYearDialog.newInstance(calendarAdapter.getCurrentMonthNum(), calendarAdapter.getCurrentYear(), this).show(getChildFragmentManager(), MonthYearDialog.TAG);
				break;
		}
	}

	public void prevClick(){
		calendarAdapter.prevMonth();
		displayMonth();
	}

	public void nextClick(){
		calendarAdapter.nextMonth();
		displayMonth();
	}

	private void displayMonth(){
		monthBTN.setText(calendarAdapter.getCurrentMonthName() + " " + calendarAdapter.getCurrentYear());
	}

	@Override
	public void dialogCallback(int month, int year) {
		calendarAdapter.setCurrentMonthNum(month);
		calendarAdapter.setCurrentYear(year);
		calendarAdapter.refresh();
		displayMonth();
	}

	public interface DateTimePickerListener{
		void forDisplay(String date);
	}
}
