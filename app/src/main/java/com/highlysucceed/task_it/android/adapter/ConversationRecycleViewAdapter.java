package com.highlysucceed.task_it.android.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.MessageModel;
import com.highlysucceed.task_it.data.preference.UserData;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ConversationRecycleViewAdapter extends RecyclerView.Adapter<ConversationRecycleViewAdapter.ViewHolder>  implements
        View.OnClickListener,
        View.OnLongClickListener {

	private Context context;
	private List<MessageModel> data;
    private LayoutInflater layoutInflater;
    private int myID = 0;

    private final int MESSAGE_IN = 1;
    private final int MESSAGE_OUT = 2;

	public ConversationRecycleViewAdapter(Context context) {
		this.context = context;
		this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
        myID = UserData.getUserId();
	}

    public void setNewData(List<MessageModel> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void removeItem(MessageModel item) {
        for (MessageModel messageModel : data) {
            if (item.id == messageModel.id) {
                int pos = data.indexOf(messageModel);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void addNewData(List<MessageModel> data){
        for(MessageModel item : data){
            this.data.add(item);
        }
        notifyDataSetChanged();
    }

    public void newMessage(MessageModel messageModel){
        if(data == null){
            data = new ArrayList<>();
        }

        data.add(0, messageModel);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if(viewType == MESSAGE_OUT){
            view = layoutInflater.inflate(R.layout.adapter_conversation_out, parent, false);
        }else{
            view = layoutInflater.inflate(R.layout.adapter_conversation_in, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        if(data.get(position).id  == 0){
            return MESSAGE_OUT;
        }
        return MESSAGE_IN;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.messageModel = data.get(position);

        holder.view.setTag(holder.messageModel);
        holder.view.setOnLongClickListener(this);
        holder.view.setOnClickListener(this);

        holder.messageTXT.setText(holder.messageModel.content);
        holder.dateTXT.setText(holder.messageModel.time_passed);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        MessageModel messageModel;

        @BindView(R.id.messageTXT)      TextView messageTXT;
        @BindView(R.id.dateTXT)         TextView dateTXT;
        View view;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            ButterKnife.bind(this, view);
        }
	}

    @Override
    public void onClick(View v) {
        if(clickListener != null){
            clickListener.onItemClick((MessageModel)v.getTag());
        }
    }

    @Override
    public boolean onLongClick(View v) {
        if(clickListener != null){
            clickListener.onItemLongClick((MessageModel)v.getTag());
        }
        return false;
    }

    private ClickListener clickListener;
    public void setOnItemClickListener(ClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public interface ClickListener {
        void onItemClick(MessageModel messageModel);
        void onItemLongClick(MessageModel messageModel);
    }
} 
