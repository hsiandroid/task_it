package com.highlysucceed.task_it.android.dialog;

import android.view.View;

import com.google.gson.Gson;
import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.data.model.server.MessageModel;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

import butterknife.BindView;
import icepick.State;

public class CommentOtherOptionDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = CommentOtherOptionDialog.class.getName().toString();

	private CommentCallback commentCallback;
	private MessageModel messageModel;
	@State String commentTemp;

	@BindView(R.id.openBTN)			View openBTN;
	@BindView(R.id.hideBTN)			View hideBTN;
	@BindView(R.id.cancelBTN)		View cancelBTN;

	public static CommentOtherOptionDialog newInstance(MessageModel messageModel, CommentCallback commentCallback) {
		CommentOtherOptionDialog commentOtherOptionDialog = new CommentOtherOptionDialog();
		commentOtherOptionDialog.commentCallback = commentCallback;
		commentOtherOptionDialog.commentTemp = new Gson().toJson(messageModel);
		return commentOtherOptionDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_comment_other_option;
	}

	@Override
	public void onViewReady() {
		openBTN.setOnClickListener(this);
		hideBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);

		messageModel = new Gson().fromJson(commentTemp, MessageModel.class);
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}

	@Override
	public void onClick(View v) {
		if(commentCallback != null){
			switch (v.getId()){
				case R.id.openBTN:
					dismiss();
					commentCallback.editComment(messageModel);
					break;
				case R.id.hideBTN:
					dismiss();
					commentCallback.deleteComment(messageModel);
					break;
				case R.id.cancelBTN:
					dismiss();
					break;
			}
		}
	}

	public interface CommentCallback{
		void editComment(MessageModel messageModel);
		void deleteComment(MessageModel messageModel);
	}
}
