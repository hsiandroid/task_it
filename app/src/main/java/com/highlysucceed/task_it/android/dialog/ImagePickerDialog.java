package com.highlysucceed.task_it.android.dialog;

import android.Manifest;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.system.ErrnoException;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;
import com.highlysucceed.task_it.vendor.android.java.ImageQualityManager;
import com.highlysucceed.task_it.vendor.android.java.PermissionChecker;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.BindView;
import icepick.State;

public class ImagePickerDialog extends BaseDialog implements View.OnClickListener,
		CropImageView.OnSetImageUriCompleteListener,
		CropImageView.OnCropImageCompleteListener{

	public static final String TAG = ImagePickerDialog.class.getName().toString();

	private static final int REQUEST_CAMERA = 10001;
	private static final int REQUEST_GALLERY = 10002;

	private static final int PERMISSION_CAMERA = 101;
	private static final int PERMISSION_GALLERY = 102;
	private File photoFile;

	private ImageCallback imageCallback;

	@BindView(R.id.cropImageView)		CropImageView cropImageView;
	@BindView(R.id.cameraBTN)			View cameraBTN;
	@BindView(R.id.galleryBTN) 			View galleryBTN;
	@BindView(R.id.rotateLeftBTN) 		View rotateLeftBTN;
	@BindView(R.id.rotateRightBTN) 		View rotateRightBTN;
	@BindView(R.id.pickerBTN)			View pickerBTN;
	@BindView(R.id.cancelBTN)			View cancelBTN;
	@BindView(R.id.imageCON)			View imageCON;
	@BindView(R.id.placeHolderIV)		View placeHolderIV;
	@BindView(R.id.titleTXT)			TextView titleTXT;

	@State String title;
	@State boolean isAspectRatio = false;

	private Uri uri;

	public static ImagePickerDialog newInstance(String title, boolean isAspectRatio, ImageCallback imageCallback) {
		ImagePickerDialog imagePickerDialog = new ImagePickerDialog();
		imagePickerDialog.title = title;
		imagePickerDialog.isAspectRatio = isAspectRatio;
		imagePickerDialog.imageCallback = imageCallback;
		return imagePickerDialog;
	}

	public static ImagePickerDialog newInstance(String title, boolean isAspectRatio, Uri uri, ImageCallback imageCallback) {
		ImagePickerDialog imagePickerDialog = new ImagePickerDialog();
		imagePickerDialog.title = title;
		imagePickerDialog.isAspectRatio = isAspectRatio;
		imagePickerDialog.uri = uri;
		imagePickerDialog.imageCallback = imageCallback;
		return imagePickerDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_image_picker;
	}

	@Override
	public void onViewReady() {
		pickerBTN.setOnClickListener(this);
		cameraBTN.setOnClickListener(this);
		galleryBTN.setOnClickListener(this);
		rotateLeftBTN.setOnClickListener(this);
		rotateRightBTN.setOnClickListener(this);
		cancelBTN.setOnClickListener(this);
		imageCON.setOnClickListener(this);

		if(title == null){
			titleTXT.setText(getString(R.string.app_name));
		}else{
			titleTXT.setText(title);
		}

		cropImageView.setFixedAspectRatio(isAspectRatio);
		cropImageView.setMinCropResultSize(100, 100);

		if(uri != null){
			showImage(uri);
		}

		StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
		StrictMode.setVmPolicy(builder.build());

//		openCamera();
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
		cropImageView.setOnSetImageUriCompleteListener(this);
		cropImageView.setOnCropImageCompleteListener(this);
	}

	@Override
	public void  onStop() {
		super.onStop();
		cropImageView.setOnSetImageUriCompleteListener(null);
		cropImageView.setOnCropImageCompleteListener(null);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.pickerBTN:
				cropImageView.getCroppedImageAsync();
				break;
			case R.id.cameraBTN:
			case R.id.imageCON:
				openCamera();
				break;
			case R.id.galleryBTN:
				openGallery();
				break;
			case R.id.rotateLeftBTN:
				cropImageView.rotateImage(-90);
				break;
			case R.id.rotateRightBTN:
				cropImageView.rotateImage(90);
				break;
			case R.id.cancelBTN:
				dismiss();
				break;
		}
	}

	@Override
	public void onCropImageComplete(CropImageView view, CropImageView.CropResult result) {
		if(result.isSuccessful()){
			File imageFile = getImageFile(view.getCroppedImage());
			String newPath = ImageQualityManager.compressImage(getContext(), imageFile.getAbsolutePath());
			File cropFile = new File(newPath);
			if(imageCallback != null){
				imageCallback.result(cropFile);
			}
			dismiss();
		}
	}

	@Override
	public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {

	}

	public interface ImageCallback{
		void result(File file);
	}

	private File getImageFile(Bitmap bitmap) {
		String imageFileName = "CacheImage";
		File storageDir = new File(getContext().getCacheDir(), getString(R.string.app_name));
		if ( !storageDir.exists() ) {
			storageDir.mkdirs();
		}
		File image = null;
		try {
			image = File.createTempFile(
					imageFileName,  /* prefix */
					".jpg",         /* suffix */
					storageDir      /* directory */
			);

			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100 /*ignored for PNG*/, bos);
			byte[] bitmapData = bos.toByteArray();

			FileOutputStream fos = new FileOutputStream(image);
			fos.write(bitmapData);
			fos.flush();
			fos.close();


		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}

	private void openGallery(){
		if(PermissionChecker.checkPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, PERMISSION_GALLERY)){
			Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			intent.setType("image/*");
			startActivityForResult(intent, REQUEST_GALLERY);
		}
	}

	private void openCamera(){
		if(PermissionChecker.checkPermissions(getActivity(), new String[] {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSION_CAMERA)){
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
				photoFile = createImageFile();
				if (photoFile != null) {
					Uri uri = Uri.fromFile(photoFile);
					takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
							| Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
					if (uri != null) {
						takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
					}
					startActivityForResult(takePictureIntent, REQUEST_CAMERA);
				}
			}
		}
	}

	private File createImageFile() {
		String imageFileName = "TEMP";
		File storageDir = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name) +"/data/img/temp");
		if ( !storageDir.exists() ) {
			storageDir.mkdirs();
		}
		File image = null;
		try {
			image = File.createTempFile(
					imageFileName,  /* prefix */
					".png",         /* suffix */
					storageDir      /* directory */
			);
		} catch (IOException e) {
			e.printStackTrace();
		}

		return image;
	}


	@Override
	public  void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (requestCode){
			case REQUEST_CAMERA:
			case REQUEST_GALLERY:
				if(resultCode == Activity.RESULT_OK){
					showImage(getPickImageResultUri(data));
				}
				break;
		}

	}

	public void showImage(Uri imageUri){
		boolean requirePermissions = false;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
				getActivity().checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED &&
				isUriRequiresPermissions(imageUri)) {

			requirePermissions = true;
			requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
		}

		if (!requirePermissions) {
			cropImageView.setImageUriAsync(imageUri);
		}
		cropImageView.setVisibility(View.VISIBLE);
		placeHolderIV.setVisibility(View.GONE);
	}

	public boolean isUriRequiresPermissions(Uri uri) {
		try {
			ContentResolver resolver = getActivity().getContentResolver();
			InputStream stream = resolver.openInputStream(uri);
			stream.close();
			return false;
		} catch (FileNotFoundException e) {
			if (e.getCause() instanceof ErrnoException) {
				return true;
			}
		} catch (Exception e) {
		}
		return false;
	}

	public Uri getPickImageResultUri(Intent  data) {
		boolean isCamera = true;
		if (data != null && data.getData() != null) {
			String action = data.getAction();
			isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
		}
		return isCamera ?  getCaptureImageOutputUri() : data.getData();
	}

	private Uri getCaptureImageOutputUri() {
		Uri outputFileUri = null;
		if (photoFile != null) {
			outputFileUri = Uri.fromFile(photoFile);
		}
		return outputFileUri;
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == PERMISSION_CAMERA) {
			if(getBaseActivity().isAllPermissionResultGranted(grantResults)){
				openCamera();
			}
		}
		if (requestCode == PERMISSION_GALLERY) {
			if(getBaseActivity().isAllPermissionResultGranted(grantResults)){
				openGallery();
			}
		}
	}
}
