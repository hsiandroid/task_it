package com.highlysucceed.task_it.android.dialog;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;

public class RateProviderDialog extends BaseDialog {
	public static final String TAG = RateProviderDialog.class.getName().toString();

	public static RateProviderDialog newInstance() {
		RateProviderDialog dialog = new RateProviderDialog();
		return dialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_rate_provider;
	}

	@Override
	public void onViewReady() {

	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogMatchParent();
	}
}
