package com.highlysucceed.task_it.android.dialog;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.vendor.android.base.BaseDialog;
import com.highlysucceed.task_it.vendor.android.java.CurrencyFormatter;
import com.jaygoo.widget.RangeSeekBar;

import butterknife.BindView;
import icepick.State;

public class HomePriceRangeDialog extends BaseDialog implements View.OnClickListener{
	public static final String TAG = HomePriceRangeDialog.class.getName().toString();

	public Callback  callback;

	@BindView(R.id.priceRSB)				RangeSeekBar priceRSB;
	@BindView(R.id.minPriceTXT)				TextView minPriceTXT;
	@BindView(R.id.maxPriceTXT)				TextView maxPriceTXT;
	@BindView(R.id.continueBTN)     		View continueBTN;

	@State	int defaultMin;
	@State  int defaultMax;

	public static HomePriceRangeDialog newInstance(Callback callback, int defaultMin, int defaultMax) {
		HomePriceRangeDialog greetingsDialog = new HomePriceRangeDialog();
		greetingsDialog.defaultMin = defaultMin;
		greetingsDialog.defaultMax = defaultMax;
		greetingsDialog.callback = callback;
		return greetingsDialog;
	}

	@Override
	public int onLayoutSet() {
		return R.layout.dialog_home_price_range;
	}

	@Override
	public void onViewReady() {
        continueBTN.setOnClickListener(this);

		int min = 0;
		int max = 100000;

		priceRSB.setRange(min, max);
		priceRSB.setValue(defaultMin, defaultMax);

		minPriceTXT.setText(CurrencyFormatter.format("Php", defaultMin));
		maxPriceTXT.setText(CurrencyFormatter.format("Php", defaultMax));

		priceRSB.setOnRangeChangedListener(new RangeSeekBar.OnRangeChangedListener() {
			@Override
			public void onRangeChanged(RangeSeekBar view, float min, float max, boolean isFromUser) {
				defaultMin = (int) min;
				defaultMax = (int) max;
				minPriceTXT.setText(CurrencyFormatter.format("Php", String.valueOf(min)));
				maxPriceTXT.setText(CurrencyFormatter.format("Php", String.valueOf(max)));
			}

			@Override
			public void onStartTrackingTouch(RangeSeekBar view, boolean isLeft) {

			}

			@Override
			public void onStopTrackingTouch(RangeSeekBar view, boolean isLeft) {

			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();
		setDialogLayoutParam(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()){
			case R.id.continueBTN:
                if (callback != null){
                    callback.onRangeChange(defaultMin, defaultMax);
                }
                dismiss();
				break;
		}
	}
	public interface Callback{
		void onRangeChange(int min, int max);
	}
}
