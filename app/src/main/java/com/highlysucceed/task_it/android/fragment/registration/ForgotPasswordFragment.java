package com.highlysucceed.task_it.android.fragment.registration;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.RegistrationActivity;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import butterknife.BindView;

public class ForgotPasswordFragment extends BaseFragment implements View.OnClickListener{

    public static final String TAG = ForgotPasswordFragment.class.getName().toString();

    private RegistrationActivity registrationActivity;

    @BindView(R.id.requestEmailTV)      TextView requestEmailTV;
//    @BindView(R.id.verificationCodeTV)  TextView verificationCodeTV;
    @BindView(R.id.emailET)             EditText emailET;

    public static ForgotPasswordFragment newInstance() {
        ForgotPasswordFragment forgotPasswordFragment = new ForgotPasswordFragment();
        return forgotPasswordFragment;
    }
    @Override
    public int onLayoutSet() {
        return R.layout.fragment_forgot_password;
    }

    @Override
    public void onViewReady() {
        registrationActivity = (RegistrationActivity) getContext();

        requestEmailTV.setOnClickListener(this);
//        verificationCodeTV.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.requestEmailTV:
                attemptRequestVerification();
                break;
//            case R.id.verificationCodeTV:
//                registrationActivity.openResetPasswordFragment(emailET.getText().toString());
//                break;
        }
    }

    private void attemptRequestVerification(){
//        VerificationCodeRequest verificationCodeRequest = new VerificationCodeRequest(getContext());
//        verificationCodeRequest.setProgressDialog(new ProgressDialog(getContext()).show(getContext(), "", "Checking your email...", false, false))
//                .addParameters(Variable.server.key.EMAIL, emailET.getText().toString())
//                .execute();
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//        EventBus.getDefault().register(this);
//    }
//
//    @Override
//    public void onStop() {
//        EventBus.getDefault().unregister(this);
//        super.onStop();
//    }
//
//    @Subscribe
//    public void onResponse(VerificationCodeRequest.ServerResponse responseData) {
//        if(responseData.getData().status){
//            registrationActivity.openResetPasswordFragment(emailET.getText().toString());
//        }
//    }
}
