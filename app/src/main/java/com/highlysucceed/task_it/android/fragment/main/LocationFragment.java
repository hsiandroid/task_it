package com.highlysucceed.task_it.android.fragment.main;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.highlysucceed.task_it.R;
import com.highlysucceed.task_it.android.activity.MainActivity;
import com.highlysucceed.task_it.android.adapter.LocationRecyclerViewAdapter;
import com.highlysucceed.task_it.android.dialog.defaultdialog.ConfirmationDialog;
import com.highlysucceed.task_it.data.model.server.LocationModel;
import com.highlysucceed.task_it.vendor.android.base.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;

public class LocationFragment extends BaseFragment implements
        View.OnClickListener,LocationRecyclerViewAdapter.ClickListener {
    public static final String TAG = LocationFragment.class.getName().toString();

    private MainActivity mainActivity;

    private LocationRecyclerViewAdapter locationRecyclerViewAdapter;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.locationRV)                  RecyclerView locationRV;
    @BindView(R.id.addNewLocationBTN)           TextView addNewLocationBTN;
    @BindView(R.id.locationSRL)                 SwipeRefreshLayout locationSRL;

    public static LocationFragment newInstance() {
        LocationFragment locationFragment = new LocationFragment();
        return locationFragment;
    }

    @Override
    public int onLayoutSet() {
        return R.layout.fragment_location;
    }

    @Override
    public void onViewReady() {
        mainActivity = (MainActivity) getActivity();
        mainActivity.setTitle(getString(R.string.title_main_location));
        mainActivity.setupSettingButton(0);
        addNewLocationBTN.setOnClickListener(this);

        setUpLocationListView();
    }

    private void setUpLocationListView(){
        linearLayoutManager = new LinearLayoutManager(getContext());
        locationRecyclerViewAdapter = new LocationRecyclerViewAdapter(getContext());
        locationRV.setLayoutManager(linearLayoutManager);
        locationRV.setAdapter(locationRecyclerViewAdapter);
        locationRecyclerViewAdapter.setClickListener(this);
        locationRecyclerViewAdapter.setNewData(getData());
    }

    private List<LocationModel> getData(){
        List<LocationModel> locationModels = new ArrayList<>();

        LocationModel locationModel = new LocationModel();
        locationModel.id = 1;
        locationModel.street_address = "#10 Barcelona St. Brgy. Addition";
        locationModel.city = "San Juan City,";
        locationModel.province = "NCR";
        locationModels.add(locationModel);

        return locationModels;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setSelectedItem(R.id.nav_location);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.addNewLocationBTN:
                mainActivity.startLocationActivity("create");
                break;
        }
    }

    @Override
    public void onItemView(LocationModel locationModel) {

    }

    @Override
    public void onItemEdit(LocationModel locationModel) {
        mainActivity.startLocationActivity(locationModel, "edit");
    }

    @Override
    public void onItemDelete(LocationModel locationModel) {
        deleteLocationDialog(locationModel);
    }

    private void deleteLocationDialog(final LocationModel locationModel){
        final ConfirmationDialog confirmationDialog = ConfirmationDialog.Builder();
        confirmationDialog
                .setDescription("Location")
                .setNote("Are you sure you want to remove this location?")
                .setIcon(R.drawable.icon_information)
                .setPositiveButtonText("Remove")
                .setPositiveButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        locationRecyclerViewAdapter.removeItem(locationModel);
                        confirmationDialog.dismiss();
                    }
                })
                .setNegativeButtonText("Cancel")
                .setNegativeButtonClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        confirmationDialog.dismiss();
                    }
                })
                .build(getChildFragmentManager());
    }
}
