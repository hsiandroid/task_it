package com.highlysucceed.task_it.vendor.android.base;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.highlysucceed.task_it.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;

/**
 * Created by BCTI 3 on 8/3/2017.
 */

public abstract class BaseRecylerViewAdapter<VH extends RecyclerView.ViewHolder, DI extends AndroidModel>
        extends RecyclerView.Adapter<VH>
        implements View.OnClickListener, View.OnLongClickListener {

    private Context context;
    private List<DI> data;
    private LayoutInflater layoutInflater;
    private Listener listener;

    public BaseRecylerViewAdapter(Context context) {
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        this.data = new ArrayList<>();
        layoutInflater = LayoutInflater.from(context);
    }

    public void setNewData(List<DI> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public void addNewData(List<DI> data){
        int initialCount = this.data.size();
        int newCount = 0;
        for(DI defaultItem : data){
            if(isDataUnique(defaultItem)){
                this.data.add(defaultItem);
                newCount++;
            }
        }
        notifyItemRangeChanged(initialCount, newCount);
    }

    public void removeItem(DI item) {
        for (DI defaultItem : data) {
            if (item.id == defaultItem.id) {
                int pos = data.indexOf(defaultItem);
                data.remove(pos);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    public void updateItem(DI item){
        for(DI defaultItem : data){
            if (item.id == defaultItem.id) {
                int pos = data.indexOf(defaultItem);
                data.set(pos, item);
                notifyItemRemoved(pos);
                break;
            }
        }
    }

    private boolean isDataUnique(DI item){
        for(DI defaultItem : this.data){
            if(item.id == defaultItem.id){
                return false;
            }
        }
        return true;
    }

    public Context getContext() {
        return context;
    }

    public LayoutInflater getLayoutInflater() {
        return layoutInflater;
    }

    public List<DI> getData() {
        return data;
    }

    public DI getItem(int position){
        return getData().get(position);
    }

    public View getDefaultView(ViewGroup parent, int resLayout){
        return getLayoutInflater().inflate(resLayout, parent, false);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Listener getListener() {
        return listener;
    }

    public boolean hasListener(){
        return listener != null;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {

    }

    @Override
    public int getItemCount() {
        return getData().size();
    }

    @Override
    public void onClick(View v) {
        if(hasListener()){
            switch (v.getId()){
                case R.id.adapterCON:

                    break;
            }
        }
    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()){
            case R.id.adapterCON:

                break;
        }
        return false;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Object object;
        private View view;
        private AndroidModel androidModel;

        public ViewHolder(View view) {
            super(view);
            init(view);
        }

        private void init(View view) {
            this.view = view;
            ButterKnife.bind(this, view);
        }

        public View getBaseView() {
            return view;
        }

        public void setObject(Object object) {
            this.object = object;
        }

        public Object getObject() {
            return object;
        }

        public void setItem(AndroidModel androidModel) {
            this.androidModel = androidModel;
        }

        public AndroidModel getItem() {
            return androidModel;
        }
    }

    public interface Listener {
        void onItemClick(AndroidModel androidModel);
        void onItemLongClick(AndroidModel androidModel);
    }
}
