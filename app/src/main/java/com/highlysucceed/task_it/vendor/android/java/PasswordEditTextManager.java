package com.highlysucceed.task_it.vendor.android.java;

import android.content.Context;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.highlysucceed.task_it.R;


/**
 * Created by BCTI 3 on 4/20/2017.
 */

public class PasswordEditTextManager {

    public static void addShowPassword(final Context context, final EditText editText, final ImageView imageView) {
        editText.setSelected(false);
        format(context, editText, imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setSelected(!editText.isSelected());
                format(context, editText, imageView);
            }
        });
    }

    private static void format(Context context, EditText editText, ImageView imageView) {
        int selection = editText.getSelectionStart();
        if (editText.isSelected()) {
            editText.setTransformationMethod(null);
            imageView.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.icon_password_show));
        } else {
            editText.setTransformationMethod(new AsteriskPasswordTransformationMethod());
            imageView.setImageDrawable(ActivityCompat.getDrawable(context, R.drawable.icon_hide_password));
        }

        editText.setSelection(selection);
    }
}
