package com.highlysucceed.task_it.vendor.android.java;

import java.text.DecimalFormat;

/**
 * Created by BCTI 3 on 3/28/2017.
 */

public class Formatter {
    public static String formatToPrice(double val){
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.00");
        return decimalFormat.format(val);
    }
}
