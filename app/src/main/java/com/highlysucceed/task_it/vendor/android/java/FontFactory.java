package com.highlysucceed.task_it.vendor.android.java;

import android.content.Context;

/**
 * Created by BCTI 3 on 12/2/2016.
 */

public class FontFactory extends FontManager.FontManagerTypeFace{

    public FontFactory(Context context){
        super(context,
                "fonts/Asap-Regular.otf",
                "fonts/Asap-Italic.otf",
                "fonts/Asap-Bold.otf",
                "fonts/Asap-Bold.otf");
    }
}
