package com.highlysucceed.task_it.vendor.android.java;

import android.widget.Spinner;

/**
 * Created by BCTI 3 on 9/30/2016.
 */
public class SpinnerManager {

    public static void setSpinnerText(Spinner spinner, String text){
        if(text == null){
            return;
        }
        for(int i= 0; i < spinner.getAdapter().getCount(); i++){
            if((spinner.getAdapter().getItem(i)).toString().toLowerCase().contains(text.toLowerCase())){
                spinner.setSelection(i);
            }
        }
    }
}
