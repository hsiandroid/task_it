package com.highlysucceed.task_it.vendor.android.java;

/**
 * Created by BCTI 3 on 3/13/2017.
 */

public class CalendarModel {
    public int day;
    public String month;
    public int year;
    public boolean selected;
    public String status;
}
