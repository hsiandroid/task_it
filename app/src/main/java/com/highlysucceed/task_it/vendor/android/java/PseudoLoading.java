package com.highlysucceed.task_it.vendor.android.java;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;

/**
 * Created by BCTI 3 on 12/19/2016.
 */

public class PseudoLoading {

    private Handler handler;
    private Runnable runnable;
    private String title;
    private int delay = 3000;

    private Context context;

    public static PseudoLoading newInstance(Context context) {
        PseudoLoading pseudoLoading = new PseudoLoading();
        pseudoLoading.context = context;
        return pseudoLoading;
    }

    public PseudoLoading setTitle(String title){
        this.title = title;
        return this;
    }

    public PseudoLoading setDelay(int delay){
        this.delay = delay;
        return this;
    }

    public PseudoLoading setCallback(Callback callback){
        this.callback = callback;
        return this;
    }

    public void run(){
        final ProgressDialog progressDialog = new ProgressDialog(context).show(context, "", title, false, false);
        runnable = new Runnable(){
            @Override
            public void run() {
                if(callback != null){
                    progressDialog.cancel();
                    callback.finish();
                }
            }
        };

        handler = new Handler();
        handler.postDelayed(runnable, delay);
    }

    private Callback callback;
    public interface Callback {
        void finish();
    }

    public void onPause(){
        if(handler != null){
            handler.removeCallbacks(runnable);
        }
    }
}
