package com.highlysucceed.task_it.vendor.android.java;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import com.highlysucceed.task_it.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImagePicker implements View.OnClickListener{

    private final String TAG = ImagePicker.class.getName().toString();

    private LayoutInflater layoutInflater;
    private int layout;
    private int background ;
    private View contentView;
    private View cameraBTN;
    private View galleryBTN;
    private LENGTH duration;
    private boolean swipe;

    private Snackbar snackbar;
    private Snackbar.SnackbarLayout snackbarView;
    private AppCompatActivity appCompatActivity;
    private Context context;
    private Fragment fragment;
    public String imagePath;

    public static final int RESPONSE_IMAGE_GALLERY = 1001;
    public static final int RESPONSE_IMAGE_CAMERA = 1002;

    private ImagePicker(AppCompatActivity appCompatActivity) {
        this.context = appCompatActivity;
        this.layoutInflater = (LayoutInflater) appCompatActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.duration = LENGTH.LONG;
        this.background =  R.color.colorPrimary;
        this.layout = R.layout.snackbar_image_picker;
        this.swipe = false;
        this.appCompatActivity = appCompatActivity;
    }

    private ImagePicker(Fragment fragment) {
        this.context = fragment.getContext();
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.duration = LENGTH.LONG;
        this.background =  R.color.colorPrimary;
        this.layout = R.layout.snackbar_image_picker;
        this.swipe = false;
        this.fragment = fragment;
    }

    public static ImagePicker Builder(AppCompatActivity appCompatActivity) {
        return new ImagePicker(appCompatActivity);
    }

    public static ImagePicker Builder(Fragment fragment) {
        return new ImagePicker(fragment);
    }

    public ImagePicker build(View view) {
        if (view == null) throw new FooterPopupException("view can not be null");
        if (layout == -1) throw new FooterPopupException("layout must be setted");
        switch (duration) {
            case INDEFINITE:
                snackbar = Snackbar.make(view, "", Snackbar.LENGTH_INDEFINITE);
                break;
            case SHORT:
                snackbar = Snackbar.make(view, "", Snackbar.LENGTH_SHORT);
                break;
            case LONG:
                snackbar = Snackbar.make(view, "", Snackbar.LENGTH_LONG);
                break;
        }
        snackbarView = (Snackbar.SnackbarLayout) snackbar.getView();

        if (!swipe) {
            snackbarView.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    snackbarView.getViewTreeObserver().removeOnPreDrawListener(this);
                    ((CoordinatorLayout.LayoutParams) snackbarView.getLayoutParams()).setBehavior(null);
                    return true;
                }
            });
        }

        snackbarView.setPadding(0, 0, 0, 0);
        if (background != -1) snackbarView.setBackgroundResource(background);
        TextView text = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        text.setVisibility(View.INVISIBLE);
        TextView action = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_action);
        action.setVisibility(View.INVISIBLE);
        contentView = layoutInflater.inflate(layout, null);
        contentView.setPadding(0, 40, 0, 40);
        contentView.setLayoutParams(new CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.MATCH_PARENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT));
        cameraBTN = contentView.findViewById(R.id.snackbar_camera_txt);
        cameraBTN.setOnClickListener(this);
        galleryBTN = contentView.findViewById(R.id.snackbar_gallery_txt);
        galleryBTN.setOnClickListener(this);
        snackbarView.addView(contentView, 0);
        return this;
    }

    public void show() {
        snackbar.show();
    }

    public boolean isShowing() {
        return snackbar != null && snackbar.isShown();
    }

    public void dismiss() {
        if (snackbar != null) snackbar.dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.snackbar_camera_txt:
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                if(fragment != null){
                    if (takePictureIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
                        File photoFile = createImageFile(context);

                        if (photoFile != null) {
                            imagePath = photoFile.getAbsolutePath();
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            if(fragment != null){
                                fragment.startActivityForResult(takePictureIntent, RESPONSE_IMAGE_CAMERA);
                            }else{
                                appCompatActivity.startActivityForResult(takePictureIntent, RESPONSE_IMAGE_CAMERA);
                            }
                        }
                    }
                    fragment.startActivityForResult(takePictureIntent, RESPONSE_IMAGE_CAMERA);
                }else{
                    if (takePictureIntent.resolveActivity(appCompatActivity.getPackageManager()) != null) {
                        File photoFile = createImageFile(context);

                        if (photoFile != null) {
                            imagePath = photoFile.getAbsolutePath();
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                            appCompatActivity.startActivityForResult(takePictureIntent, RESPONSE_IMAGE_CAMERA);
                        }
                    }
                }
                break;
            case R.id.snackbar_gallery_txt:
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                if(fragment != null){
                    fragment.startActivityForResult(intent, RESPONSE_IMAGE_GALLERY);
                }else{
                    appCompatActivity.startActivityForResult(intent, RESPONSE_IMAGE_GALLERY);
                }
                break;
        }
    }

    public String getImagePath(){
        return imagePath;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case RESPONSE_IMAGE_CAMERA:
                    String path = getImagePath();
                    if ( path == null ) {
                        Log.e(TAG, "Failed to get image.");
                    } else {
                        if(imageResponseListener != null){
                            imageResponseListener.imageResponse(this, new File(path));
                            Log.e("Camera", ">>>" + path);
                        }
                    }
                    break;
                case RESPONSE_IMAGE_GALLERY:

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = context.getContentResolver().query(selectedImage,filePathColumn, null, null, null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    cursor.close();

                    if ( picturePath == null ) {
                        Log.e(TAG, "Failed to get image.");
                    } else {
                        ImageManager.copyFileAppDirectory(context, new File(picturePath),
                                ImageManager.createImageFile(context, context.getString(R.string.app_name), "temp"),
                                new ImageManager.Callback() {
                                    @Override
                                    public void success(File file) {
                                        if(imageResponseListener != null){
                                            imageResponseListener.imageResponse(ImagePicker.this, file);
                                        }
                                    }
                                });
                    }
                    break;
            }
        }
    }

    private File createImageFile(Context context) {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = new File(context.getExternalCacheDir(), "image_temp");
        if ( !storageDir.exists() ) {
            storageDir.mkdirs();
        }
        File image = null;
        try {
            image = File.createTempFile(
                    "temp",  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }
        return image;
    }

    public enum LENGTH {
        INDEFINITE, SHORT, LONG
    }

    public class FooterPopupException extends RuntimeException {

        public FooterPopupException(String detailMessage) {
            super(detailMessage);
        }

    }

    public ImagePicker setImageResponseListener(ImageResponseListener imageResponseListener){
        this.imageResponseListener = imageResponseListener;
        return this;
    }

    private ImageResponseListener imageResponseListener;

    public interface ImageResponseListener{
        void imageResponse(ImagePicker picker, File file);
    }

}
