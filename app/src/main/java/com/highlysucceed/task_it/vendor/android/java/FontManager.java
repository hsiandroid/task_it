package com.highlysucceed.task_it.vendor.android.java;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by BCTI 3 on 12/2/2016.
 */

public class FontManager {

    private Typeface normalTF;
    private Typeface italicTF;
    private Typeface boldTF;
    private Typeface boldItalicTF;
    private Context context;
    private ViewGroup viewGroup;


    private static volatile FontManager singleton = null;

    public static FontManager with(Activity activity){
        singleton = new FontManager();
        singleton.context = activity;
        singleton.viewGroup = (ViewGroup) activity.findViewById(android.R.id.content);
        return singleton;
    }

    public static FontManager with(Fragment fragment, View view){
        singleton = new FontManager();
        singleton.context = fragment.getContext();
        singleton.viewGroup = (ViewGroup) view;
        return singleton;
    }

    public static FontManager with(Context context, View view){
        singleton = new FontManager();
        singleton.context = context;
        singleton.viewGroup = (ViewGroup) view;
        return singleton;
    }

    public FontManager setNormalFontPath(String fontPath){
        singleton.normalTF = getTypeFaceFromAsset(fontPath);
        return singleton;
    }

    public FontManager setItalicFontPath(String fontPath){
        singleton.italicTF = getTypeFaceFromAsset(fontPath);
        return singleton;
    }

    public FontManager setBoldFontPath(String fontPath){
        singleton.boldTF = getTypeFaceFromAsset(fontPath);
        return singleton;
    }

    public FontManager setBoldItalicFontPath(String fontPath){
        singleton.boldItalicTF = getTypeFaceFromAsset(fontPath);
        return singleton;
    }

    public FontManager setFontManagerTypeFace(FontManagerTypeFace fontManagerTypeFace){
        singleton.normalTF = fontManagerTypeFace.normalTF;
        singleton.italicTF = fontManagerTypeFace.italicTF;
        singleton.boldTF = fontManagerTypeFace.boldTF;
        singleton.boldItalicTF = fontManagerTypeFace.boldItalicTF;
        return singleton;
    }

    public void build(){
        searchForTextView(singleton.viewGroup);
    }

    public void applyFontToTextView(TextView textView){
        if (textView.getTypeface() != null){
            switch (textView.getTypeface().getStyle()){
                case Typeface.BOLD:
                    textView.setTypeface(singleton.boldTF);
                    break;
                case Typeface.BOLD_ITALIC:
                    textView.setTypeface(singleton.boldItalicTF);
                    break;
                case Typeface.NORMAL:
                    textView.setTypeface(singleton.normalTF);
                    break;
                case Typeface.ITALIC:
                    textView.setTypeface(singleton.italicTF);
                    break;
            }
        }else{
            textView.setTypeface(singleton.normalTF);
        }
    }

    private void searchForTextView(ViewGroup viewGroup){
        for (int i = 0; i < viewGroup.getChildCount(); i++)
        {
            Object child = viewGroup.getChildAt(i);
            if (child instanceof TextView)
            {
                TextView textView = (TextView) child;
                applyFontToTextView(textView);
            }
            else if(child instanceof ViewGroup)
            {
                searchForTextView((ViewGroup) child);
            }
        }
    }

    public Typeface getTypeFaceFromAsset(String fontPath){
        return Typeface.createFromAsset(singleton.context.getAssets(), fontPath);
    }

    public static class FontManagerTypeFace{
        private Typeface normalTF;
        private Typeface italicTF;
        private Typeface boldTF;
        private Typeface boldItalicTF;

        public FontManagerTypeFace(){

        }

        public FontManagerTypeFace(Typeface normalTF,
                Typeface italicTF,
                Typeface boldTF,
                Typeface boldItalicTF){

            this.normalTF = normalTF;
            this.italicTF = italicTF;
            this.boldTF = boldTF;
            this.boldItalicTF = boldItalicTF;
        }

        public FontManagerTypeFace(Context context, String normalPath,
                                   String italicPath,
                                   String boldPath,
                                   String boldItalicPath){

            this.normalTF = Typeface.createFromAsset(context.getAssets(),normalPath);
            this.italicTF = Typeface.createFromAsset(context.getAssets(),italicPath);
            this.boldTF = Typeface.createFromAsset(context.getAssets(),boldPath);
            this.boldItalicTF = Typeface.createFromAsset(context.getAssets(),boldItalicPath);
        }
    }
}
