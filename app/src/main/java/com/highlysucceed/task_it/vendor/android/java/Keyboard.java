package com.highlysucceed.task_it.vendor.android.java;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * Created by BCTI 3 on 10/26/2016.
 */

public class Keyboard {
    public static void hideKeyboard(Activity activity) {
        InputMethodManager keyboard  = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        keyboard .hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    public static void showKeyboard(Activity activity, EditText editText) {
        editText.requestFocus();
        InputMethodManager keyboard  = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        keyboard.showSoftInput(editText, 0);
    }
}
