package com.highlysucceed.task_it.vendor.android.java;

import android.text.TextUtils;

/**
 * Created by BCTI 3 on 10/25/2017.
 */

public class CurrencyFormatter {

    public static String format(double num){
        return format("", num);
    }

    public static String format(String currency, double num){
        return String.format("%s %,.2f",currency, num);
    }

    public static String formatNoDecimal(double num){
        return String.format("%,d", (int) num);
    }

    public static String formatNoComma(double num){
        return String.format("%.2f", num);
    }

    public static String format(String num){
        return format("", num);
    }

    public static String format(String currency, String num){
        if(TextUtils.isEmpty(num)){
            num = "0.00";
        }
        Double numParsed = Double.parseDouble(num);
        return String.format("%s %,.2f", currency, numParsed);
    }
}