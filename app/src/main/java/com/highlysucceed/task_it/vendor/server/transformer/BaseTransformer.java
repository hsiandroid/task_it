package com.highlysucceed.task_it.vendor.server.transformer;

/**
 * Created by BCTI 3 on 8/3/2017.
 */

public class BaseTransformer {

    public String msg = "Internal Server Error";
    public Boolean status = false;
    public String status_code = "RETROFIT_FAILED";
    public String token = "";
    public String new_token = "";
    public Boolean has_morepages = false;

    public boolean hasRequirements(){
        return false;
    }

    public boolean checkEmpty(Object obj){
        return obj !=  null;
    }
}
