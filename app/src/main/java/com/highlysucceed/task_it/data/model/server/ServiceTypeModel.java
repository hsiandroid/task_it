package com.highlysucceed.task_it.data.model.server;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class ServiceTypeModel extends AndroidModel {
    public String title;
    public String full_path;
    public String time_passed;
}
