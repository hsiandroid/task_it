package com.highlysucceed.task_it.data.model.server;

import android.os.Parcel;
import android.os.Parcelable;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class BookingProfileModel extends AndroidModel implements Parcelable{
    public String name;
    public String address;
    public String full_path;
    public String time_passed;

    public BookingProfileModel(){

    }


    protected BookingProfileModel(Parcel in) {
        name = in.readString();
        address = in.readString();
        full_path = in.readString();
        time_passed = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(full_path);
        dest.writeString(time_passed);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingProfileModel> CREATOR = new Creator<BookingProfileModel>() {
        @Override
        public BookingProfileModel createFromParcel(Parcel in) {
            return new BookingProfileModel(in);
        }

        @Override
        public BookingProfileModel[] newArray(int size) {
            return new BookingProfileModel[size];
        }
    };
}
