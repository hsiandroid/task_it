package com.highlysucceed.task_it.data.preference;


import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.highlysucceed.task_it.data.model.db.UserItem;

/**
 * Created by BCTI 3 on 1/31/2017.
 */

public class UserData extends Data{

    public static final String USER_ITEM = "user_item";
    public static final String AUTHORIZATION = "authorization";
    public static final String FIRST_LOGIN = "first_login";
    public static final String EMAIL = "email";
    public static final String PASSWORD = "password";

    public static boolean isLogin(){
        return getUserItem().id != 0;
    }

    public static boolean isMe(int id){
        return getUserItem().id == id;
    }

    public static int getUserId(){
        return getUserItem().id;
    }

    public static void insert(String key, String value){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void insert(String key, int value){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void insert(String key, boolean value){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static String getString(String key){
        return Data.getSharedPreferences().getString(key, "");
    }

    public static int getInt(String key){
        return Data.getSharedPreferences().getInt(key, 0);
    }

    public static boolean getBoolean(String key){
        return Data.getSharedPreferences().getBoolean(key, false);
    }

    public static void insert(UserItem userItem){
        SharedPreferences.Editor editor = Data.getSharedPreferences().edit();
        editor.putString(USER_ITEM, new Gson().toJson(userItem));
        editor.commit();
    }

    public static UserItem getUserItem(){
        UserItem userItem = new Gson().fromJson(Data.getSharedPreferences().getString(USER_ITEM, ""), UserItem.class);
        if(userItem == null){
            userItem = new UserItem();
        }
        return userItem;
    }

    public static void clearData(){
        Data.getSharedPreferences().edit().clear().commit();
    }

}
