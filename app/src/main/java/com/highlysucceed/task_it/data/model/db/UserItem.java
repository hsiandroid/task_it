package com.highlysucceed.task_it.data.model.db;

/**
 * Created by BCTI 3 on 12/14/2016.
 */

public class UserItem {
    public int id;
    public String username;
    public String fname;
    public String lname;
    public String email;
    public String image;
    public Info info;

    public static class Info {
        public Data data;

        public static class Data {
            public String gender;
            public String birthdate;
            public String address;
            public String street_address;
            public String city;
            public String state;
            public String contact_number;
        }
    }
}
