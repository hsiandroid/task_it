package com.highlysucceed.task_it.data.model.server;

import android.os.Parcel;
import android.os.Parcelable;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class AddLocationModel extends AndroidModel implements Parcelable{
    public String location_title;
    public String location_address;


    protected AddLocationModel(Parcel in) {
        location_title = in.readString();
        location_address = in.readString();

    }

    public AddLocationModel(){

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(location_address);
        dest.writeString(location_title);

    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AddLocationModel> CREATOR = new Creator<AddLocationModel>() {
        @Override
        public AddLocationModel createFromParcel(Parcel in) {
            return new AddLocationModel(in);
        }

        @Override
        public AddLocationModel[] newArray(int size) {
            return new AddLocationModel[size];
        }
    };
}
