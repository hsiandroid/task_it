package com.highlysucceed.task_it.data.model.server;

import android.os.Parcel;
import android.os.Parcelable;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class LocationModel extends AndroidModel implements Parcelable{
    public String street_address;
    public String city;
    public String province;
    public String time_passed;

    protected LocationModel(Parcel in) {
        street_address = in.readString();
        city = in.readString();
        province = in.readString();
        time_passed = in.readString();
    }

    public LocationModel(){

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(street_address);
        dest.writeString(city);
        dest.writeString(province);
        dest.writeString(time_passed);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LocationModel> CREATOR = new Creator<LocationModel>() {
        @Override
        public LocationModel createFromParcel(Parcel in) {
            return new LocationModel(in);
        }

        @Override
        public LocationModel[] newArray(int size) {
            return new LocationModel[size];
        }
    };
}
