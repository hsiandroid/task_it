package com.highlysucceed.task_it.data.model.server;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class BookingPreferenceModel extends AndroidModel {
    public String name;
    public String service_name;
    public String designation;
    public String full_path;
}
