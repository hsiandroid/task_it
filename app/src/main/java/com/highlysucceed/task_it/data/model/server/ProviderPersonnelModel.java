package com.highlysucceed.task_it.data.model.server;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class ProviderPersonnelModel extends AndroidModel {
    public String companyPersonnel;
    public String away;
    public String priceRange;
    public String full_path;
}
