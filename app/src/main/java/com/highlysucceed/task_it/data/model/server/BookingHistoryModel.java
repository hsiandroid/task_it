package com.highlysucceed.task_it.data.model.server;

import android.os.Parcel;
import android.os.Parcelable;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by Jomar Olaybal on 8/12/2017.
 */

public class BookingHistoryModel extends AndroidModel implements Parcelable{
    public String name;
    public String away;
    public String full_path;
    public String priceRange;
    public String time_passed;

    public BookingHistoryModel(){

    }


    protected BookingHistoryModel(Parcel in) {
        name = in.readString();
        away = in.readString();
        full_path = in.readString();
        priceRange = in.readString();
        time_passed = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(away);
        dest.writeString(full_path);
        dest.writeString(priceRange);
        dest.writeString(time_passed);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BookingHistoryModel> CREATOR = new Creator<BookingHistoryModel>() {
        @Override
        public BookingHistoryModel createFromParcel(Parcel in) {
            return new BookingHistoryModel(in);
        }

        @Override
        public BookingHistoryModel[] newArray(int size) {
            return new BookingHistoryModel[size];
        }
    };
}
