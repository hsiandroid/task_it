package com.highlysucceed.task_it.data.model.server;

import android.os.Parcel;
import android.os.Parcelable;

import com.highlysucceed.task_it.vendor.android.base.AndroidModel;

/**
 * Created by BCTI05 on 2/7/2017.
 */

public class MessageModel extends AndroidModel implements Parcelable {
    public int admin_id;
    public String content;
    public String time_passed;
    public Info info;
    public Admin admin;

    protected MessageModel(Parcel in) {
        id = in.readInt();
        admin_id = in.readInt();
        content = in.readString();
        time_passed = in.readString();
    }

    public MessageModel(){

    }

    public static final Creator<MessageModel> CREATOR = new Creator<MessageModel>() {
        @Override
        public MessageModel createFromParcel(Parcel in) {
            return new MessageModel(in);
        }

        @Override
        public MessageModel[] newArray(int size) {
            return new MessageModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(admin_id);
        dest.writeString(content);
        dest.writeString(time_passed);
    }

    public static class Info {
        public InfoData data;

        public static class InfoData {
            public CreatedAtData created_at;

            public static class CreatedAtData {
                public String date_db;
                public String month_year;
                public String time_passed;
                public TimestampData timestamp;

                public static class TimestampData {
                    public String date;
                    public int timezone_type;
                    public String timezone;
                }
            }
        }
    }

    public static class Admin {
        public AdminData data;

        public static class AdminData {
            public int id;
            public String name;
            public String username;
            public String email;
            public Object contact_number;
            public String type;
            public String role;
            public Object fb_id;
            public String image;
        }
    }
}
