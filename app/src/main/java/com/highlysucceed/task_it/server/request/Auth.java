package com.highlysucceed.task_it.server.request;

import android.content.Context;

import com.highlysucceed.task_it.config.Keys;
import com.highlysucceed.task_it.data.model.server.SampleModel;
import com.highlysucceed.task_it.vendor.server.request.APIRequest;
import com.highlysucceed.task_it.vendor.server.request.APIResponse;
import com.highlysucceed.task_it.vendor.server.transformer.BaseTransformer;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Jomar Olaybal on 8/3/2017.
 */

public class Auth {

    public static Auth getDefault(){
        return new Auth();
    }

    public void login(Context context, String username, String password) {
        APIRequest apiRequest = new APIRequest<BaseTransformer>(context) {
            @Override
            public Call<BaseTransformer> onCreateCall() {
                return getRetrofit().create(RequestService.class).requestLogin(getMultipartBody());
            }

            @Override
            public void onResponse() {
                EventBus.getDefault().post(new LoginResponse(this));
            }
        };

        apiRequest
                .addParameter(Keys.USERNAME, username)
                .addParameter(Keys.PASSWORD, password)
                .showDefaultProgressDialog("Logging in...")
                .execute();

    }

    public static void logout(int id) {

    }

    public void signup(SampleModel userModel) {

    }

    public static void facebook(SampleModel userModel) {

    }


    public interface RequestService {
        @Multipart
        @POST("{p}")
        Call<BaseTransformer> requestLogin(@Part List<MultipartBody.Part> parts);

        @POST("url")
        Call<BaseTransformer> requestLogout(@Header("Authorization") String authorization);
    }


    public class LoginResponse extends APIResponse<BaseTransformer> {
        public LoginResponse(APIRequest apiRequest) {
            super(apiRequest);
        }
    }
}
